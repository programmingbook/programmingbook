Animation
=========

Animating a sprite can be done a few different ways; you can use an
array of images, each slightly different from its precessor, or you
can use images from a sprite map. Sprite maps are fairly common but a
little complex, so for now you will just use the image array. In a
dedicated chapter, you will learn about tilemaps and how to use them
for tiling.

Walk Cycles
-----------

In order to animate your hero sprite, you need at least two sets of
four drawings. That is, you will need one *walk cycle* consisting of
at least four drawings with your hero facing left and one set with the
hero facing right. 

There are plenty of free and open source graphic programmes, but for
pixel art you might try `mtpaint <http://www.slax.org/en/modules.php?detail=mtpaint&category=graphics>`_. Set your canvas to the dimensions
of your desired tile, and then zoom in; mtpaint will draw a grid to
guide you as you paint pixels. 

So what exactly are you drawing? Well,
a *walk cycle* is a standard in animation and has been for about a
century. If you've never drawn a walk cycle before, it's mostly
(arguably) in the legs. Make four drawings with the legs in some
approximation of how legs are positions through one complete stride,
and you'll end up with a walk cycle if you loop those same four
drawings endlessly. Look online for better animation advice than would
be practical to give in this book. 

.. figure:: /images/walkcycle.png
   A walk cycle by `Ajay Karat <http://devilsgarage.com>`_, CC3.0

If you'd like, for the sake of this exercise, you can also just
download a hero sprite from the book's webpage.

Once you have a walk cycle, you can proceed to the code.

Importing Sys
-------------

Import the `sys` module for the benefit of making the exit process
more graceful. Pygame does quit without `sys` but it's considered
"cleaner" to include `sys`. 

Add this to the current import list in the code from the previous
chapter::
  import sys

Adjusting Frame Rate
--------------------

A graphic card's refresh rate and the frames per second required for
humans to perceive animation are drastically different. A high display
refresh will provide smooth graphic movement and a more realistic
world, but animation only requires 24 frames per second to appear
realistic and to synchronize with sound.

In a game as basic as this one, this doesn't really matter much, but
some day you might be working on a game where you'll need sync sound
and beautiful graphics, so it's important to understand the
concept. In order to give yourself flexibility in finding the best
animation settings for your game, add a new variable at the top of
your document::
  afps = 4

You'll see how setting it to 4 affects the animation speed later, and
by giving it a variable you can easily adjust it to find the speed
that works best for you.

Adding the Player Class
-----------------------

In the existing code from the previous chapter, you already have a
class that represents platforms. After the `Platform` class, add the
new `Player` class::
  01. class Player(pygame.sprite.Sprite):
  02. ¬ momentumX = 0
  03. ¬ momentumY = 0
  04. ¬ frame = 0

1. The first line establishes the class `Player` using the sprite
   module from pygame. You are probably getting used to invoking this by
   now, even if you are a little fuzzy on what exactly you must do once
   you've used it.
2. Next, we assign some "natural state" variables. In other words, if
   the player is freshly spawned and has had no external forces acting
   upon it, what does it do? Well, ideally it does nothing; it neither
   moves across the screen or "moves" in its animation. It just stands
   still. 

   Therefore, this line creates a variable called `momentumX` and sets
   it to 0. Once you build up the process of moving the variable
   across the screen, this will be meaningful because if no other
   button is being pressed, then the player's natural state is to have
   0 movement.
3. This line creates a variable called `momentumY` and sets it to 0.
   Again, once there is code to cause movement, this will be significant.
4. This line creates a variable called `frame` and sets it to 0. Once
   you have created code to cause animation (such as leg movement; your
   walk cycle) then the fact that the natural state of `frame` equalling
   zero will provide the "at rest" frame for the player.

The player's avatar is not really intended to do much more than that
when being completely at rest, so now you can move onto creating some
methods so that the player can do things like spawn and move::
  05. ¬ def __init__(self):
  06. ¬¬ pygame.sprite.Sprite.__init__(self)
  07. ¬¬ self.images=[]
  08. ¬¬ for i in range(1,9):
  09. ¬¬¬ img = pygame.image.load("cat"+str(i)+".png").convert()
  10. ¬¬¬ img.set_colorkey(keyer)
  11. ¬¬¬ self.images.append(img)
  12. ¬¬ self.image = self.images[0]
  13. ¬¬ self.rect = self.image.get_rect()

5. Defines a new method called `__init__` which will allow a new
   Player sprite to spawn. Note that it is indented, so it is a part of
   the `Player` class.
6. The `__init` method invokes pygames's sprite function, spawning the
   sprite in the game world..
7. This line does something you have not seen before. For the image of
   the sprite, an array is created. What does the array contain? Glad you
   asked...
8. To populate the array, a for-loop is set in motion. The for loop
   will iterate eight (1 through 9) times. What will it do? Again, I'm
   glad you asked...
9. First, a new variable, `img`, is created. The variable is set to
   the results of pygame's `img.load` function, which grabs any image in
   the current directory that is called `cat1.png` and loads it. The
   number is incremented each time the for-loop runs until it hits 9.
10. Next, a colour key is set. This may or may not be necessary,
    depending on how you drew your sprite.
11. The image, with its colour key in place, is added to the array, and
    the loop is run again until it reaches 9.
12. After the for-loop has run through its 8 iterations, the next line
    is processed. This line sets the default image of the Player sprite to
    the first image (image 0) to the first image of the array (cat1.png).
13. And finally, pygame analyzes the width and height of the avatar and
    determins the dimensions of its bounding box so that it can use this
    as a reference when determining collisions later in the game code.

That method has given the Player the potential to be animated, but
there is no animation without motion, so now we need a method to
enable movement. Actually, we need two methods; one to provide
momentum to the avatar via control keys such as wasd , and another to
enable the avatar to move across the screen.

First, create the method that will provide control::
  14. ¬ def control(self,x,y):
  15. ¬¬ self.momentumX += x
  16. ¬¬ self.momentumY += y

14. The first line creates the method `control`, which takes itself and
   `x` and `y` values as arguments. Actually, you'll be providing `x` and
   `y` *differentials*; for instance, if the user presses the right arrow
   or d key, then the avatar will move *some value* plus `x+5`.
15. This line establishes the that whatever the sprite's current
   momentum is (you set it to 0 initially, at the top of the `player`
   class, remember?), then when this method is invoked `x` should be
   added to it.
16. This line establishes the that whatever the sprite's current
   momentum is (you set it to 0 initially, at the top of the `player`
   class, remember?), then when this method is invoked `y` should be
   added to it.

That sets up movement, believe it or not. If it doesn't make sense to
you, it might make more sense when you add the keyboard controls,
which directly invokes this method.

In fact, the entire control scheme of this game is nearly the exact
same as that in the Feeding Frenzy game, so you can cut and paste that
entire code block into this game's code with only a few
modifications. 

From your current codebase, take out the main loop down to the QUIT
line. To be precise, remove these lines::

  while main == True:
  ¬ for event in pygame.event.get():
  ¬¬ if event.type == pygame.QUIT:
  ¬¬¬ pygame.quit(); sys.exit();
  
  ¬¬ if event.type == pygame.KEYUP:
  ¬¬¬ if event.key == K_ESCAPE or event.key == ord('q'):
  ¬¬¬¬ pygame.quit()
  ¬¬¬¬ sys.exit()

And replace it with the key control block from Feeding Frenzy . You
need to make some minor changes so that you are using your `control`
method. In the end, your code will look like this::

  main = True
    while main == True:
    ¬ for event in pygame.event.get():
    ¬¬ if event.type == pygame.QUIT:
    ¬¬¬ sys.exit()
    ¬¬¬ main = False
    
    ¬¬ if event.type == pygame.KEYDOWN:
    ¬¬¬ if event.key == pygame.K_LEFT:
    ¬¬¬¬ player.control(-5,0)
    ¬¬¬ if event.key == pygame.K_RIGHT:
    ¬¬¬¬ player.control(5,0)
    ¬¬¬ if event.key == pygame.K_UP:
    ¬¬¬¬ player.control(0,-5)
    ¬¬¬ if event.key == pygame.K_DOWN:
    ¬¬¬¬ player.control(0,5)
     
    ¬¬ if event.type == pygame.KEYUP:
    ¬¬¬ if event.key == pygame.K_LEFT:
    ¬¬¬¬ player.control(5,0)
    ¬¬¬ if event.key == pygame.K_RIGHT:
    ¬¬¬¬ player.control(-5,0)
    ¬¬¬ if event.key == pygame.K_UP:
    ¬¬¬¬ player.control(0,5)
    ¬¬¬ if event.key == pygame.K_DOWN:
    ¬¬¬¬ player.control(0,-5)
    ¬¬¬ if event.key == pygame.K_ESCAPE or event.key == ord('q'):
    ¬¬¬¬ pygame.quit()
    ¬¬¬¬ sys.exit()

Just as defined in your `control` method, each keypress invokes
`control` along with an `x` and `y` value, which, as we know, are
added to the current value of the sprite's momentum.

Notice that while Feeding Frenzy used variables such as `moveUp` and
`moveRight` to establish whether or not the avatar was in motion, we
do the same thing in a dedicated method. The difference here is that
Feeding Frenzy 's controls were embedded within the main loop; it was
therefore subject entirely to the ticking of the clock, so it was by
definition synchronous. Since we plan on adding gravity to this world
eventually, we need an asynchronous force that will control the pull
of gravity (or, in other words, the continual addition of momentum in
the negative `y` direction).

.. note:: In real life, you probably wouldn't have thought of that, so
	  you've have attempted to create motion in the same way as in
	  Feeding Frenzy , and then you'd hack on some gravity, and
	  you'd be left wondering why the gravity in your world was so
	  darned heavy. But that's OK, because that's how you learn,
	  so if you find yourself implementing something in the future
	  and it's not quite working, give yourself a pat on the back
	  for ingenuity. The more you practise, even when you get
	  something slightly wrong or even completely wrong, the more
	  you learn and the better programmer you become. 

Now that you have provided controls so that the player avatar can move
around the screen, add the updating method so that pygame can continue
to draw the player on the screen in the appropriate locations, and
detect collisions (and eventually calculate gravity).

This code should be placed within the `Player` class, as it is a
method of that class. It's asynchronous, so you can place it above or
below your `control` class; it won't matter::

  17. ¬ def update(self):
  18. ¬¬ currentX = self.rect.x
  19. ¬¬ nextX = currentX+self.momentumX
  20. ¬¬ self.rect.x = nextX

17. Defines a new method called `update` which will allow the player
    sprite to be accurately re-drawn on the pygame screen.
18. A new variable called `currentX` is created and the value of the
    sprite's rectangle attributes.

    .. note:: If you would like to see the rectangle's attributes for
	      yourself, you can place the lines::
		`print("Here are the rectangle x y w h positions")
		print(self.rect)`
	as the first lines of this method, and look for the results in your
	terminal when running the game later on.

19. Creates a variable called `nextX` and sets its value to the value
   of `currentX` plus the value of the current momentum (if any; if the
   player is standing still then `momentumX` would be set to 0).

20. The final line of this stanza sets the value of `self.rect.x` to
   the value of `nextX`, which you just created by taking the `currentX`
   location of the sprite and adding momentum to it.


It will probably come as no surprise to you that the next block of
code does the exact same thing the `y` position of the sprite::
  ¬¬ currentY = self.rect.y
  ¬¬ nextY = currentY+self.momentumY
  ¬¬ self.rect.y = nextY

Now the sprite can be drawn all over the screen but there's no
animation yet. For that, you need to tell pygame to cycle through each
image in the sprite's image array for as long as the sprite has
momentum (ie, the value of momentum is something other than 0). This
is slightly complex because which image pygame should use depends on
which direction the sprite it moving, so beware: there is some math
ahead::

  21. ¬¬ if self.momentumX < 0:
  22. ¬¬¬ self.frame += 1
  23. ¬¬¬ if self.frame > 3*afps:
  24. ¬¬¬¬ self.frame = 0
  25. ¬¬¬ self.image = self.images[self.frame//afps]
      
  27. ¬¬ if self.momentumX > 0:
  28. ¬¬¬ self.frame += 1
  29. ¬¬¬ if self.frame > 3*afps:
  30. ¬¬¬¬ self.frame = 0
  31. ¬¬¬ self.image = self.images[self.frame//afps+4]

21. Indented twice to remain in the `update` method, the first line
   starts an if-loop that requires `self.momentumX` to be less than 0. In
   other words, the sprite *is* in motion in a negative direction (from
   right to left).
22. If the sprite is in motion, then the value of `self.frame` is
    advanced by 1. You will remember establishing that the value of
    `frame` to 0 at the very top of the `Player` class. The frame value is
    what will control which version of the hero image pygame uses as the
    graphic for the sprite.

    .. note:: Why do we prepend the `self.` in front of `frame` now?
	      Well, because methods are like little bubbles, and so
	      the variables that occur inside of them cannot pass
	      outside without a return statement, and variables that
	      occur outside of them cannot pass *in* without a self
	      statement. When you created the `update` method, you did
	      specify that `self` was to be included; so when this
	      method is invoked, all of the outside variables
	      associated with the class are made known to it. Now you
	      know! (I would have told you earlier, but it wouldn't
	      have made sense until now, trust me)

23. If the sprite is in motion, then this line starts *another* if-
    statement. Here, *if* the sprite is in motion then is the frame
    greater than 3 (ignore the * `afps` for a moment)? If so, the next
    line will set it back to zero so that the image sequence loops
    back round to the beginning.

    If you were to leave it at just `if self.frame > 3:`, it would
    work perfectly well except that the animation frame rate would be
    remarkably fast. In fact, probably bizarrely fast; your avatar's
    legs would be cycling through strides three or four times before
    the avatar moved forward.

    Remember that the frame rate ( `fps`) in this game has been set to
    40, and with each tick of the clock, one animation `frame` is
    advancing, so the refresh rate of the game display is also the
    frame rate of animation. But LCD refresh rates and the rate at
    which the human eye requires to perceive movement in a series of
    images are not at all the same, so there is no reason to have an
    animation frame rate of 40 when the film industry standard is 24
    frames per second. Therefore, you can slow down the rate of
    animation by multiplying the number of frames by `afps` (4),
    meaning that the animation frames are moving only every fourth
    click of the pygame clock, slowing the perceived animation while
    not affecting the refresh rate of the display.

    .. note:: You might also think that you could just adjust the
	      factor by which the frames are incremented in the line
	      above this one. Currently, the frames are incremented by
	      1, so if you reduced that number to .25 then that
	      *should* affect the animation frame rate. However, it
	      also would require you to do all of the math as floating
	      point calculations, which gets unnecessarily
	      complicated. So instead of dealing with decimals, you
	      just multiply and divide by whole numbers. Yes, algebra
	      is useful after all. 

    Once this code is up and running, you can try experimenting with the
    animation speed by changing the `afps` factor.  Try 1 or
    2 or 8. Or try reducing the display `fps` to see what
    affect that has.

24. Having established that the current frame is greater than 3*
   `afps`, this line of code sets `self.frame` back to 0, going back to
   the first walk cycle image and starting the walk loop over again.
25. If the current frame is *not* greater than 3, then pygame skips the
   previous line and reaches *this* line, which sets `self.frame` to
   whatever the new frame count is (established two lines above) divided
   by `afps`. You divide by `afps` because you don't ever want to leave
   the four-image loop your walk cycle requires.
26. A blank line for code readability.
27. The next block of code does the exact same thing, but for the final
   four images of your walk cycle, when the avatar is moving from left to
   right. This first line enters an if-loop in which `momentumX` is
   greater than 0. In other words, the hero is moving in a positive
   direction (from left to right)
28. If the sprite is in motion, then increment the animation `frame` by
   1.
29. Starts a sub if-loop *if* the current frame is now greater than 3
   times the animation frame rate ( `afps`). Again, if this is true, then
   we have reached the end of the walk cycle and the next line will...
30. ...set the frame count back to zero. But...
31. ...it's important to realize that he base level of numbers we are
   dealing with is bumped up by the number of images in each walk cycle.
   There are four images of the hero walking due east (right) and four
   images of the hero walking due west (left), so if we are in this if-
   loop then the images we want to use are not `cat1.png` through
   `cat4.png` but `cat5.png` through `cat8.png`. 

   So instead of setting the `self.image` of the sprite to whatever
   number corresponds to the frame count, you set the `self.image` to
   the frame count divided by `afps` *plus 4*. Don't get confused by
   the fact that right now `afps` and 4 happen to be the same number;
   hey don't have to be. `afps` can be anything you want it to be, but
   we are adding 4 because there are 4 images in the westward walk
   cycle.
   
   If you had drawn more cells in your walk cycle, for instance, maybe
   you'd drawn 8 steps, or maybe your hero is a strange 3-legged beast
   and required 6 steps, then you would be adding 8, or 6, or whatever
   number of drawing you have for one full walk cycle loop. Make
   sense?

That's what it takes to animate a sprite.

Spawning the Player
-------------------

The player sprite is animated but it hasn't been spawned and doesn't
appear anywhere in the game world. Spawn the player along with the
platforms by locating this block of code::
  pygame.init()
  screen = pygame.display.set_mode([screenX, screenY])
  ledge_list = createWorld()
  screen.fill(white)

And adding::
  32. player = Player()
  33. player.rect.x = 50
  34. player.rect.y = 521
  35. movingsprites = pygame.sprite.Group()
  36. movingsprites.add(player)

32. This line spawns a player sprite ingame by creating a variable
   called `player` and assigning it the result of invoking the `Player`
   class. In other words, this is what makes the player sprite appear on
   the screen when the game is launched.
33. Determines where along the `x` axis of the game world the player is
   positioned.
34. Determines where along the `y` axis of the game world the player is
   positioned.
35. Creates a new sprite group called `movingsprites`, which you will
   use when refreshing the screen during the `main` loop.
36. Adds the newly spawned `player` sprite to the `movingsprites`
   group.

And now, add the player sprite into the `main` loop::

  37. player.update()
  38. movingsprites.draw(screen)

37. Invokes the `update` method from your `Player` class, which updates
   the position of the `player` sprite.
38. Updates all sprites within the `movingsprites` group.

Try running your game now, just to see what you have built so far. Go
to the Tools menu of SPE and select Run/Stop , or use the keyboard
shortcut control r . A dialogue box will appear, prompting for
arguments but since your game takes no arguments, just click the Run
button to proceed.

You should see a white background with some black
platforms. You will also have a player sprite which you can control
with the standard keyboard controls, but you you will probably notice
pretty quickly that there is a problem. The sprite leaves a trail of
pixels after each move it makes.

Quit the game and look at the code.  See if you can find the one line
of code that you can move to fix the trailing pixels. 

If you notice, the update of the `player` sprite occurs within the
`main` loop, but the update of the `screen` occurs *outside* the
`main` loop. That worked fine when nothing was moving, but now that
there are sprites that change position, you need to paint over the
previous location of the sprite.

So, if you move::

  screen.fill(white)

down into the `main` loop (you can place it just above or just after
the `player.update()` line), and re-start the game, then the trailing-
pixel issue will be fixed. 

Some things to notice so far:

* The `player` sprite's legs are successfully animated, looping
  through your walk cycle for as long as the player moves.
* There is no gravity. Even though the perspective appears to be
  perpendicular to the `x` axis, the game play is from top-down.
* There is no collision detection. The character can stroll right over
  the platform blocks.

Next, you'll add collisions, gravity, and scrolling.

animation.py
------------

The source 
code::

  #!/usr/bin/env python
  # GNU All-Permissive License
  # Copying and distribution of this file, with or without modification,
  # are permitted in any medium without royalty provided the copyright
  # notice and this notice are preserved.  This file is offered as-is,
  # without any warranty.

  import pygame
  import sys

  keyer = (255,255,255)
  black = (0,0,0)
  white = (255,255,255)
  screenX = 960
  screenY = 720
  screenU = 680
  forwardX = 590
  backwardX = 230
  tilesize = 32
  fps = 40
  afps = 4
  clock = pygame.time.Clock()
  main = True

  # generate platforms
  class Platform(pygame.sprite.Sprite):
  ¬ def createBlock(self,x,y,width,height):
  ¬ ¬ self.image = pygame.Surface([width,height])
  ¬ ¬ self.rect = self.image.get_rect()
  ¬ ¬ self.rect.y = y
  ¬ ¬ self.rect.x = x
  ¬ def __init__(self,x,y,width,height):
  ¬ ¬ pygame.sprite.Sprite.__init__(self)
  ¬ ¬ self.createBlock(x,y,width,height)


  # generate player sprite
  class Player(pygame.sprite.Sprite):
  ¬ momentumX = 0
  ¬ momentumY = 0
  ¬ frame = 0
  ¬ def __init__(self):
  ¬ ¬ pygame.sprite.Sprite.__init__(self)
  ¬ ¬ self.images = []
  ¬ ¬ for i in range(1,9):
  ¬ ¬ ¬ img = pygame.image.load("hero" + str(i) + ".png").convert()
  ¬ ¬ ¬ img.set_colorkey(keyer)
  ¬ ¬ ¬ self.images.append(img)
  ¬ ¬ ¬ self.image = self.images[0]
  ¬ ¬ ¬ self.rect = self.image.get_rect()
  ¬ def control(self,x,y):
  ¬ ¬ self.momentumX += x
  ¬ ¬ self.momentumY += y
  ¬ def update(self):
  ¬ ¬ # collision horizontal
  ¬ ¬ currentX = self.rect.x
  ¬ ¬ nextX = currentX+self.momentumX
  ¬ ¬ self.rect.x = nextX


  ¬ ¬ # collision vertical
  ¬ ¬ currentY = self.rect.y
  ¬ ¬ nextY = currentY+self.momentumY
  ¬ ¬ self.rect.y = nextY


  ¬ ¬ # moving left
  ¬ ¬ if self.momentumX < 0:
  ¬ ¬ ¬ self.frame += 1
  ¬ ¬ ¬ if self.frame > 3*afps:
  ¬ ¬ ¬ ¬ self.frame = 0
  ¬ ¬ ¬ self.image = self.images[self.frame//afps]

  ¬ ¬ # moving right
  ¬ ¬ if self.momentumX > 0:
  ¬ ¬ ¬ self.frame += 1
  ¬ ¬ ¬ if self.frame > 3*afps:
  ¬ ¬ ¬ ¬ self.frame = 0
  ¬ ¬ ¬ self.image = self.images[self.frame//afps+4]

  # calculate where all the platforms go
    def createWorld():
    ¬ ledge_list = pygame.sprite.Group()
    ¬ ledge = Platform(10,200,448,32)
    ¬ ledge_list.add(ledge)
    ¬ ledge = Platform(480,350,320,32)
    ¬ ledge_list.add(ledge)
    ¬ ledge = Platform(900,404,64,32)
    ¬ ledge_list.add(ledge)
    ¬ return ledge_list
    
    
    pygame.init()
    screen = pygame.display.set_mode([screenX,screenY])
    ledge_list = createWorld()
    player = Player()
    player.rect.x = 50
    player.rect.y = 521
    movingsprites = pygame.sprite.Group()
    movingsprites.add(player)
    
    
    while main == True:
    ¬ for event in pygame.event.get():
    ¬ ¬ if event.type == pygame.QUIT:
    ¬ ¬ ¬ pygame.quit(); sys.exit()
    ¬ ¬ ¬ main = False
    
    
    ¬ ¬ if event.type == pygame.KEYDOWN:
    ¬ ¬ ¬ if event.key == pygame.K_LEFT or event.key == ord('a'):
    ¬ ¬ ¬ ¬ player.control(-5,0)
    ¬ ¬ ¬ if event.key == pygame.K_RIGHT or event.key == ord('d'):
    ¬ ¬ ¬ ¬ player.control(5,0)
    ¬ ¬ ¬ if event.key == pygame.K_UP or event.key == ord('w'):
    ¬ ¬ ¬ ¬ player.control(0,-5)
    ¬ ¬ ¬ if event.key == pygame.K_DOWN or event.key == ord('s'):
    ¬ ¬ ¬ ¬ player.control(0,5)
    
    
    ¬ ¬ if event.type == pygame.KEYUP:
    ¬ ¬ ¬ if event.key == pygame.K_LEFT or event.key == ord('a'):
    ¬ ¬ ¬ ¬ player.control(5,0)
    ¬ ¬ ¬ if event.key == pygame.K_RIGHT or event.key == ord('d'):
    ¬ ¬ ¬ ¬ player.control(-5,0)
    ¬ ¬ ¬ if event.key == pygame.K_UP or event.key == ord('w'):
    ¬ ¬ ¬ ¬ player.control(0,5)
    ¬ ¬ ¬ if event.key == pygame.K_DOWN or event.key == ord('s'):
    ¬ ¬ ¬ ¬ player.control(0,-5)
    ¬ ¬ ¬ # quitwatch
    ¬ ¬ ¬ if event.key == ord('q'):
    ¬ ¬ ¬ ¬ pygame.quit()
    ¬ ¬ ¬ ¬ sys.exit()
    ¬ ¬ ¬ ¬ main = False
    
    
    ¬ screen.fill(white)
    ¬ ledge_list.draw(screen)
    ¬ player.update()
    ¬ movingsprites.draw(screen)
    ¬ pygame.display.flip()
    ¬ clock.tick(fps)
