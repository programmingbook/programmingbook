Application Tools
=================

Some of the applications you will need for this book come bundled
along with Slax, but others need to be installed. Additionally, you
might find that you need additional drivers to get hardware to work
correctly; Linux supports an astonishingly wide variety of computers,
so it would be impossible to ship all the drivers necessary for every
potential computer.

Installing drivers and applications on Linux is a little different
than what you may be used to. Mac OS and Windows both traditionally
encouraged users (and to some degree, still do, although they are
moving over to an app-store model) to go out onto the internet or
software stores and find software to install. The nice thing about
that is that it lets users explore and find interesting and new
software on their own. The negative side of it is that malware
proliferates and many people end up downloading applications they
should not. For decades, Linux has distributed its software to its
users via non-centralized "software repositories", or, in modern
terms, "app stores". This has the distinct advantage of keeping
software well-monitored for malware, and also makes it very easy for
users to install new applications without having to search the entire
internet for it.

To access Slax's "app store" (it's not really a store since it's
free), go to `slax.org/en/modules.php
<http://slax.org/en/modules.php>`_ and browse through the
categories. Download applications or drivers that you need, but mind
the 32-bit versus 64-bit versioning.

.. image: /images/apps.png

When downloading an application, be sure to also download the
applications it, in turn, requires to run. For instance, the Pygame
module requires Python, SDL, and smpeg to run, so download those
modules as well since they have not been included by default on the
216mb Slax CD, which is how Slax can stay at 216mb. 

If you're comfortable working in the terminal, you can download and
activate all of the applications with just the `slax activate`
command. Type `slax` for more details. 

For this book, there are modules you will need consistently, so you
may as well download them all. If you are running Slax off of a
persistent thumbdrive, then the modules only need to be downloaded
once; they will persist throughout all of your sessions since they are
able to save themselves to your thumbdrive. 

If you are running Slax from a CD, then your session data is never
saved, so save the modules to a spare thumbdrive, and you can load
them in each time you reboot.

Download these modules:

* svgalib
* sdl
* pygobject
* glib
* expat
* mtpaint
* gtk*
* pygtk
* perl
* m4
* gc
* gettext-tools
* wxPython
* spe
* python
* smpeg
* pygame
* automake
* autoconf

Some other optional but recommended applications that you might find
useful:

* gimp-legacy
* git
* w3m
* emacs

After you download each application, save the `.sb` file to a
thumbdrive for safe-keeping (if you're running Slax off of a
thumbdrive, saving it to that same thumbdrive will do). To activate
these modules, (that is, load them into the Slax environment so you
can use the applications), open a terminal and change into the
directory containing the applications files. The quickest way to do
that is to type `cd` and then drag the folder containing your
applications into your terminal window. Paste the directory in as
text, and then press RETURN and you should find yourself in a
directory of `.sb` files. 

Once there, type this simple command::

  # for i in *.sb ; do yes no | slax activate $i ; done

This loops through each `.sb` file, tells slax to answer "no" to the
inevitable question of whether you'd like to download extra support
packages from the internet (if you have downloaded all the
applications recommended in my list then you have all the extra
support libraries and applications you need), and then to activate the
application. When it finishes the loop, it pronounces itself done and
ends the programme.

Remember, if you are running Slax from a CD, you will have to run this
command every time you freshly boot, because obviously you cannot
permanently merge the applications with an environment running off of
a read-only optical disc. However, you will only need to do it once
upon boot, so it's not too inconvenient.

If you're running Slax from a thumbdrive, then the initial activation
of an application copies the `.sb` file to a system folder, which will
be automatically loaded from then on.

Homework
--------

Don't stop at just the applications I'm recommending; try them all!
Browse the Slax website to see all of the modules available to you,
and activate a few to see what they do.

Keep in mind that if you're running off of CD, everything is temporary
and will vanish upon reboot. If you're running off of a thumbdrive,
any application you activate will be re-activated upon reboot. 

The important thing to realize is that there are hundreds of free
software applications to help you get everyday work done as well as
the specialized programmer work you're learning now. Don't hesitate to
give new applications a try; you just might find something you
love.

.. image:: /images/straightedge.png

For an overview on running Slax on a semi-regular or regular
basis, see `http://straightedgelinux.com/slax <http://straightedgelinux.com/slax>`_
