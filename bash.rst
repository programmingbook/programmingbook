Intro to BASH
=============

Your first programming language is going to be BASH. Why? because it's
an easy language to grasp and yet has all of the most common
programming constructs, such as for- and while-loops, functions,
variables, includes, parsing, and much more. There is no other
language that is quite so practical and yet so accessible.

It's also a uniquely powerful language, since it speaks directly to
the UNIX system that is driving your computer (and, coincidentally,
most of the Internet and big data centers and network infrastructures
of the world).

The first program you're going to write is going to have a text-only
interface. Textual interfaces evokes all kinds of retro- computing
nostalgia, but in fact most of what any computer does is done in plain
text. Some operating systems bend over backward to conceal this fact
from their users, but behind all those pretty interfaces, it's all
just plain text.

.. figure:: /images/xterm.png
   :scale: 50 %

   *Pictured: Two BASH terminal emulators.*

A basic but obvious example of this is the internet itself. Launch
Firefox from the `K-menu` in the bottom left of the Slax desktop. In
Firefox, navigate to the fanciest, `prettiest website
<http://straightedgelinux.com/switch/>`_ you can think of. Press
control-u to View Source and behold: plain text. 

How can so much in the modern world, with all of the fancy user
interfaces and gooey, glossy touchscreens and 3d graphic technology be
drive by plain text?  The answer is twofold.

#. First of all, it isn't really *all* driven by plain text, at least
   not by the time we users see it. The most advanced programming
   languages (C, C++, and so on) are compiled languages, meaning that
   even though the programmer types in plain text when writing the
   program, the code is run through a code compiler like gcc (GNU C
   Compiler) so that it all ends up as binary data which directly
   instructs the CPU of the user's computer. But only computers read and
   understand binary instructionsets; the programmers themselves start
   out with plain text.

#. Secondly, some programming languages are interpreted languages.
   This means that the programmer writes the code in plain text and then
   a runtime environment application runs the code, translating it from
   plain text to binary instructions *in realtime*. Many of the most
   popular programming languages are interpreted languages; examples
   include HTML, CSS, javascript, Ruby, BASH, Python, and even the
   megalithic Java.

Linux and BASH make no secret that they thrive on plain text, and
neither should you. We like plain text because both humans and
computers can easily read and process plain text. So your first
program will only have a textual inteface. This doesn't mean you can't
and won't put nice and pretty graphics on top of plain text code in
later lessons, but the stuff under the hood, in the veins, the stuff
making everything go, will always be plain text. Learning how to read,
parse, and process it effectively is a skill every programmer needs.

Getting BASH
------------

You can get BASH in a few different ways. On Linux (which is what you
should be using for this book) you most likely already have it (you
*certainly* do if you are running Slax). Whenever you open a Terminal
(Konsole on Slax, Terminal or Xterm on some others), you are opening a
BASH prompt.

.. tip:: The first character (# on Slax, but a dollar sign on most
	 others) represents your terminal's prompt; don't type that
	 in. Not all terminals use the same character for a prompt, so
	 it's not unheard of that you might see a percent sign or
	 dollar sign, but the idea is the same: your terminal, when
	 idle, will always give you a prompt and patiently await your
	 command. 

To ensure you're using BASH, you can always run the program `echo
$SHELL` and this will reveal exactly what shell you are
currently running. In fact, try that in whatever terminal
you've managed to open::
  # echo $SHELL

And just like that, I've secretly introduced you to the concept of
variables.

.. note:: Recent Mac OS releases have shipped with BASH as the default
	  shell, so if you're insisting upon using Mac OS, you'll find
	  BASH in `/Applications/Utilities` as `Terminal.app` . If
	  you're using Windows, then there is something called `cygwin
	  <http://www.cygwin.com/>`_ which installs a UNIX-like
	  environment into your existing Windows system. BASH is its
	  default terminal.

          Having BASH on other platforms is a great way to get
          yourself to use it everyday, but do not rely upon it for
          this book. If you are going to work through this book, use
          Slax Linux, or at least *some* Linux.

Variables
---------

Imagine that you are at the top of a cliff, and your computer is in
the ravine. The only way for you to communicate is via a system of
pullies which you have rigged elevator-style between the two of you.
Therefore, you can place items into the basket and lower it down to
your computer, and the computer can place items in the basket and send
it back up to you.

In that story, the basket would be a `variable`, and the items you
place into the variable would most likely be user input. The items
that the computer places into the variable would be computer output;
it might be an echo statement, or the results of some mathematical
problem, or the output of a command.

The point is that the only reason you have access to the computer's
information is because that information was placed into the
basket. And the only reason the computer has access to information
generated by you is because you put the information into the
basket.

Without variables to bridge the gap between the programmer
(and ultimately, the user) and the computer, you would be sending
instructions to the computer without it having any context about what
data it should perform those instructions upon, or else the computer
would be processing data and never sending it back up to you. 

So, variables are like a container into which you can place data. In
most any programming language, some variables are built in or
reserved, as with the `$SHELL` variable in BASH , but you can also
create your own.

In BASH , creating a variable is done simply by inventing a
variable name (this creates the basket into which you'll place the
data) and then providing the data you want to store in that
variable. Try it::
  # DESSERT='cupcake'

Just like that, you've instantiated a variable and placed the word
"cupcake" into it. Once data has been put into a variable, you can
take it out and look at it by prefacing the variable name with a
dollar sign::
  # echo $DESSERT
  cupcakes

Variables are designed to be malleable. For instance, you can change
your BASH prompt. Try this for fun::
  # PS1=":-) "

Now your prompt is a smiley face. You can make your prompt be anything
you want it to be, but a hash or dollar sign or percent sign are the
most common conventions. 

`PS1` is a built-in variable that BASH uses to
decide what string of characters to use for its prompt. 

In addition to built-in variables and variables that you yourself
create, there are also variables that you can let your users define.

User Input
----------

If you want your user to be able to define what data is contained
inside of a variable, you can use `read`::
  # read DESSERT

This moves the cursor to a blank line and waits patiently for user
input. Try entering something, and then press return. To view the data
you've just placed into that variable, use::
  # echo $DESSERT

Of course, just dumping a user to a blank line is not really very
user-friendly, so we might improve our small programme with an `echo`
statement::
  # echo "Enter a dessert and then press return." && read DESSERT

Of course this all feels silly, since we're typing the programme in
and then immediately running it, so let's review a few more principles
of BASH and then write a programme we can actually run as an actual
application.

Processing Input
----------------

Getting data from users is a pretty important aspect of programming,
but equally important is what you do with that data. Right now we're
just going to process text that the user gives us back, but the basic
principles will be the same when you need to process key presses
needed to move a video game character, or numbers for calculations,
and so on.

Most programming languages have built-in functions to
process data, and many of the libraries you add to those languages
provide a lot more. In BASH , you can string commands together with
"pipes"; that is, you send the output of one command as the input for
another command so that the original data is filtered and affected in
whatever way you choose.

#. For instance, if you place some string of characters into a
   variable::
     # ANIMAL=drummer

#. And then call that data back out::
     # echo $ANIMAL

   Then you get the data just as it was typed in. No surprise there.

#. Using a pipe, however, you can pass that data through another
   command, changing the appearance of the called data (although it does
   not change the data contained in the variable itself)::
     # echo $ANIMAL | tr [:lower:] [:upper:]
     DRUMMER
     # echo $ANIMAL
     drummer

There are a vast number of programs available to BASH for the
processing of data; tr to translate characters to other characters,
cut to break up letters and words and fields, cat to concatenate, tac
to concatenate in reverse, rev to reverse, basename to split apart
file extensions from filenames, grep to search, date to provide the
time and date, sed to find and replace, yes to repeat a string back to
the shell infinitely, and many many more.

Test
----

In addition to processing the contents of variables, you can also test
them. The `test` command provides means for you to compare the
contents of variables with what you might have expected to get back
from the user, or to other variables, or even just whether or not a
variable or file exists.

`Test` takes a few different forms, but when programming it's most
useful as a conditional statement; "if something is true, then do
*whatever*". For instance:

#. In this code block::
     # ANIMAL=drummer
     # if [ X"$ANIMAL" == "Xdrummer" ] ; then echo
     "yes they are equal" ; fi
     yes they are equal

   We test to see if the value of X$ANIMAL is equal to Xdrummer. (We
   add the "X" to normalize our data; it's mostly a best-practise that
   helps avoid unexpected errors.)

#. If the conditiion turns out to be false, then we get nothing back::
     # ANIMAL=drummer
     # if [ X$ANIMAL == Xpianist ] ; then
     echo "yes they are equal" ; fi

There are many other ways to test conditions in BASH but now you have
the basic concept, and we'll put it to work soon so you can see an
example of why it's so useful.

Redirection
-----------

When calling data from a variable, the computer's output normally just
gets dumped on your screen, as you have seen. To be more precise, the
output is actually sent to something called "standard out", which by
default is your screen. When you process a variable with a pipe, you
are intercepting standard out and filtering the data, and *then*
letting the user see the output. The fact that you, as the programmer,
controls where the data is in the flow from variable to output is very
important. Just as important is the fact that you can take something
destined for output and turn it into input for the next filter or
application. Sometimes you don't want the user to get the output at
all. For instance, if the user is creating a file that needs to be
saved on their hard drive, you wouldn't want the output to be dumped
out to a terminal, but into a file that the user could then open and
use later. BASH makes it very easy to redirect input, output, and even
error messages. Try this:

#. As you know, the default resting place for output is to the
   programmer's console::
     # echo "foo"
     foo

   In this example, "foo" is echoed to the standard output.

#. Now try redirecting the standard output to create a new file::
     # echo "foo" >> file.txt

#. Notice how you do not see the output. Did the command not work? Of
   course it did; in this case, "foo" was echoed not to the default
   output location of your screen, but into a file called `file.txt`::
     # ls
     file.txt
     # cat file.txt
     foo

   Now you see that "foo" was placed into a new file on your harddrive.
   Exciting, no?

#. Now let's combine some text input, text filtering, and redirection::
     # read DESSERT
     cupcakes
     # echo $DESSERT | rev | tr [:lower:] [:upper:] >> backwards.txt
     # cat backwards.txt
     SEKACPUC

Nonsense, obviously (although there actually are many use-cases for
reversing strings), but you see how the flow of input and output, and
the data within the variable, is manipulated. This will be important
very soon!

Homework
--------

This has been a mere overview of BASH but as a result you have already
learned some of the most important principles in programming. Next,
you will program an actual usable and useful application entirely in
BASH , but before you proceed try exploring BASH a little more on your
own.

* Experiment with the tools reviewed in this chapter, and explore new
  commands in BASH , so that you get used to getting and manipulating
  text.
* Play around with variables. Change them, manipulate them, see what
  works and what doesn't work.
* Practise redirection. See what the difference is when you use one >
  instead of >> to redirect. Try redirecting the output of common
  commands like `ls` and `pwd` and even `date`.
