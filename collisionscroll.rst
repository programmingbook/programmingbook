Collision and Scrolling
=======================

The previous game Feeding Frenzy used, essentially, top-down gameplay;
you could imagine that the avatars were pieces on a board game as in
checkers or chess. The principles of using platforms as obstacles or
blockers or walls is perfectly valid with or without gravity, so even
though our game world is primed for gravity, before we add that into
the mix we will add collision detection and, in a similar vein,
scrolling.

Collision Detection
-------------------

First thing to do is to read up on the `collision` function of Pygame.
From a quick overview of its documentation on `pygame.org/docs
<http://pygame.org/docs>`_, you see that its purpose is to find
sprites within groups that collide, and to return a list containing
those sprites.

So now ask yourself when the `player` sprite would be able to detect a
collision.  Obviously it should not occur only at the point when the
`player` is spawned, so it must be included within a `method` in the
`Player` class.

It would not happen only upon the initialization of the `player`
sprite, so the `__init__` method is out.

Something during the movement of the player would make sense, so you
might look at the `control` method. However, looking at the `control`
method makes it pretty obvious that this method's sole purpose is to
provide keyboard access to the sprite. It has no notion of where the
sprite is within the game world; it only provides a hook between the
sprite and the keyboard. So it's also out.

That leaves you with one method, and it is indeed the correct method
to use for collision detection: the `update` method. This method's job
is ascertain where in the game world the sprite is located, and in
what direction it is moving. Therefore, if you want to intercept the
natural movement of the sprite, it would be here that you would do
so. 

We know from the Pygame documentation that in order for the
`player` sprite to detect a collision, it must have a sprite group to
look out for. Do we have a sprite group in the code yet?

Well, yes, actually we have two groups. The first is very obvious
because we only made it recently; it is the `movingsprites` group and
it contains the `player` sprite. Above the `main` loop, look for this
block of code for reference::
  player = Player()
  player.rect.x = 50
  player.rect.y = 521
  movingsprites = pygame.sprite.Group()
  movingsprites.add(player)

Obviously detecting for collisions with itself would be useless to us,
so there must be another group off of which `update` should base its
collision detection. It was created in the very first iteration of
this game, in a method called `createWorld`. It is the `ledge_list`
sprite group.

OK, so now we know the sprite group we need to pass *into* the
`player.update` method, but do we know how to do that?

To pass data into a method, you must force that method to first
*request* the data. This is a little bit of a chicken-or-the-egg
situation; how does a method know to ask for data if the data may not
exist yet?  Well, the way a method knows to ask is because you are the
programmer and you tell it to ask.

To prompt a method to ask for data, you create an argument for the
dataset. Find the `update` method in the `player` class and change it
from::
  def update(self):

to this line, which includes a new argument called "blocker", although
you're free to call it whatever you like, as long as you're consistent
later::
  def update(self, blocker):

If you attempted to run the code now, an error will stop it from
running. Why? because whereas previously `player.update` required only
its `self` to execute, now you have required `player.update` to ask
for some new data, and so it does. However, when you invoke
`player.update` in your code, you do not feed it any data.

What data should you feed it? You should feed it the sprite group
`ledge_list`.  To do this, look in your `main` loop and find this
block in your code::

  player.update():
  movingsprites.draw(screen)

And simply pass `ledge_list` to `player.update` when you invoke it::

  player.update(ledge_list):
  movingsprites.draw(screen)

With that, `ledge_list` gets tunneled into `player.update` as the
answer to the "blocker" argument.

Now that `update` has the data contained in `ledge_list` available to
it, you can detect collisions with just four lines of code. Place this
block of code at the *bottom* of the `update` method of the `Player`
class::

  01. ¬¬ block_hit_list = pygame.sprite.spritecollide(self, blocker, False) 
  02. ¬¬ for True in block_hit_list:
  03. ¬¬¬ self.rect.y = currentY
  04. ¬¬¬ self.rect.x = currentX

1. The first line creates a new variable called `block_hit_list`. It
contains the results of the Pygame function `sprite.spritecollide`
(which imports the `blocker` data).

.. note:: Notice that `sprite.spritecollide` takes three arguments:
	  self, blocker, and False. The Pygame documentation notes
	  that as a part of the `sprite.spritecollide` function, there
	  is the builtin option to remove any collided sprite from its
	  group. This would be great if we were colliding with enemies
	  or collectable treasure, but unless you *want* all the
	  platforms that your player touches to vanish, it's best to
	  set that option to `False`.

2. This line starts a for-loop which searches for any True instance
   (as opposed to a False instance, or in other words, a non-instance) of
   collision recorded in the `block_hit_list`.
3. If there *is* a collision, then `self.rect.x` is set to the
   `currentX` value. In essence, the sprite is told to stay put. That is
   the expected result if you walk into a wall.
4. If there *is* a collision, then `self.rect.y` is set to the
   `currentY` value. In essence, the sprite is told to stay put. That is
   the expected result if you are on the ground or, conversely, hit the
   ceiling.

Try running your game now, just to see what you have built so far. Go
to the Tools menu of SPE and select Run/Stop , or use the keyboard
shortcut control r . A dialogue box will appear, prompting for
arguments but since your game takes no arguments, just click the Run
button to proceed.

You should find that with just a few lines of code, you have
implemented collision detection successfully.

Side Scrolling
--------------

You have probably come to expect lengthy explanations of very complex
topics. I'm afraid that scrolling around your world just isn't really
that complicated. Place this block of code in your `main` loop::

  01. ¬ if player.rect.x >= forwardX:
  02. ¬¬ scroll = player.rect.x - forwardX
  03. ¬¬ player.rect.x = forwardX
  04. ¬¬ for ledge in ledge_list:
  05. ¬¬¬ ledge.rect.x -= scroll
    
  06. ¬ if player.rect.x <= backwardX:
  07. ¬¬ scroll = backwardX - player.rect.x
  08. ¬¬ player.rect.x= backwardX
  09. ¬¬ for ledge in ledge_list:
  10. ¬¬¬ ledge.rect.x += scroll

1. Starts an if-loop that is used *if* the player's current location
   is greater than or equal to the value of `forwardX`. What is
   `forwardX`? It's a value that you, in the first iteration of this
   game, created; look at the variables up in the very top of your
   document and you'll find that `forwardX` was set to 590. Therefore, if
   the `player` sprite hits pixel 590 on the `x` axis, this if-loop gets
   triggered.
2. Creates a new variable called `scroll` and sets its value to the
   results of the player's current location minus the value of
   `forwardX`.
3. Sets the value of `player.rect.x` to the value of `forwardX`. In
   other words, it places the player sprite at 590 over and over and over
   again, for as long as the user holds down the right-arrow or d key.
   The player sprite, although technically moving, will appear to be
   standing in the same place on the screen unless...
4. ...the game world itself moves! This is exactly what happens in
   this for-loop; for any `ledge` in `ledge_list`...
5. ...set `ledge.rect.x` to `ledge.rect.x` minus the value of `scroll` The
   entire if-statement, then, is dedicated to detecting when the player
   sprite hits the 590 ( `forwardX`) mark, and then holding the sprite at
   that location while moving everything else on screen by the same
   factor that the player sprite *technically* is moving. You might think
   of it as moving-by-proxy.
6. The same must be done when a player is moving back (or left, or
   west). You could omit the second block of code if you don't want to
   permit the user to backtrack, but otherwise the code is the same,
   except that it judges whether the player is less than or equal to the
   `backwardX` value (currently set to 230 at the top of your document).
7. Creates the `scroll` variable and sets its value to `backwardX`
   minus `player.rect.x`.
8. Sets the value of `player.rect.X` to `backwardX`, making it
   *appear* that the sprite is staying at the `backwardX` mark.
9. Opens a for-loop that looks at the `ledge_list` for any objects it
   contains...
10. ...and shifts those objects along the `x` axis by whatever number
   is currently contained in the `scroll` variable.

Try the game again and notice how your world is now an endlessly
scrollable environment.

collision.py
------------

The source
code::

  #!/usr/bin/env python
  # GNU All-Permissive License
  # Copying and distribution of this file, with or without modification,
  # are permitted in any medium without royalty provided the copyright
  # notice and this notice are preserved.  This file is offered as-is,
  # without any warranty.
  
  
  import pygame
  import sys

  
  keyer = (255,255,255)
  black = (0,0,0)
  white = (255,255,255)
  screenX = 960
  screenY = 720
  screenU = 680
  
  
  forwardX = 590
  backwardX = 230
  tilesize = 32
  fps = 40
  afps = 4
  clock = pygame.time.Clock()
  main = True
    
    
  # generate platforms
  class Platform(pygame.sprite.Sprite):
  ¬ def createBlock(self,x,y,width,height):
  ¬ ¬ self.image = pygame.Surface([width,height])
  ¬ ¬ self.rect = self.image.get_rect()
  ¬ ¬ self.rect.y = y
  ¬ ¬ self.rect.x = x
  ¬ def __init__(self,x,y,width,height):
  ¬ ¬ pygame.sprite.Sprite.__init__(self)
  ¬ ¬ self.createBlock(x,y,width,height)
  
  
  # generate player sprite
  class Player(pygame.sprite.Sprite):
  ¬ momentumX = 0
  ¬ momentumY = 0
  ¬ frame = 0
  ¬ def __init__(self):
  ¬ ¬ pygame.sprite.Sprite.__init__(self)
  ¬ ¬ self.images = []
  ¬ ¬ for i in range(1,9):
  ¬ ¬ ¬ img = pygame.image.load("hero" + str(i) + ".png").convert()
  ¬ ¬ ¬ img.set_colorkey(keyer)
  ¬ ¬ ¬ self.images.append(img)
  ¬ ¬ ¬ self.image = self.images[0]
  ¬ ¬ ¬ self.rect = self.image.get_rect()
  ¬ def control(self,x,y):
  ¬ ¬ self.momentumX += x
  ¬ ¬ self.momentumY += y
  
  
  ¬ # update method + blocker argument 
  ¬ # to bring in ledge_list data
  ¬ def update(self, blocker):
  ¬ ¬ # collision horizontal
  ¬ ¬ currentX = self.rect.x
  ¬ ¬ nextX = currentX+self.momentumX
  ¬ ¬ self.rect.x = nextX
  
  
  ¬ ¬ # collision vertical
  ¬ ¬ currentY = self.rect.y
  ¬ ¬ nextY = currentY+self.momentumY
  ¬ ¬ self.rect.y = nextY
  
  
  ¬ ¬ # moving left
  ¬ ¬ if self.momentumX < 0:
  ¬ ¬ ¬ self.frame += 1
  ¬ ¬ ¬ if self.frame > 3*afps:
  ¬ ¬ ¬ ¬ self.frame = 0
  ¬ ¬ ¬ self.image = self.images[self.frame//afps]
  
  
  ¬ ¬ # moving right
  ¬ ¬ if self.momentumX > 0:
  ¬ ¬ ¬ self.frame += 1
  ¬ ¬ ¬ if self.frame > 3*afps:
  ¬ ¬ ¬ ¬ self.frame = 0
  ¬ ¬ ¬ self.image = self.images[self.frame//afps+4]
  
  
  ¬ ¬ # detect collisions with ledges via 'blocker' argument
  ¬ ¬ block_hit_list = pygame.sprite.spritecollide(self, blocker, False)
  ¬ ¬ for True in block_hit_list:
  ¬ ¬ ¬ self.rect.y = currentY
  ¬ ¬ ¬ self.rect.x = currentX
  
  
  # calculate where all the platforms go
  def createWorld():
  ¬ ledge_list = pygame.sprite.Group()
  ¬ ledge = Platform(10,200,448,32)
  ¬ ledge_list.add(ledge)
  ¬ ledge = Platform(480,350,320,32)
  ¬ ledge_list.add(ledge)
  ¬ ledge = Platform(900,404,64,32)
  ¬ ledge_list.add(ledge)
  ¬ return ledge_list
  
  
  pygame.init()
  screen = pygame.display.set_mode([screenX,screenY])
  ledge_list = createWorld()
  player = Player()
  player.rect.x = 50
  player.rect.y = 521
  movingsprites = pygame.sprite.Group()
  movingsprites.add(player)
  
  
  while main == True:
  ¬ for event in pygame.event.get():
  ¬ ¬ if event.type == pygame.QUIT:
  ¬ ¬ ¬ pygame.quit(); sys.exit()
  ¬ ¬ ¬ main = False
  
  
  ¬ ¬ if event.type == pygame.KEYDOWN:
  ¬ ¬ ¬ if event.key == pygame.K_LEFT or event.key == ord('a'):
  ¬ ¬ ¬ ¬ player.control(-5,0)
  ¬ ¬ ¬ if event.key == pygame.K_RIGHT or event.key == ord('d'):
  ¬ ¬ ¬ ¬ player.control(5,0)
  ¬ ¬ ¬ if event.key == pygame.K_UP or event.key == ord('w'):
  ¬ ¬ ¬ ¬ player.control(0,-5)
  ¬ ¬ ¬ if event.key == pygame.K_DOWN or event.key == ord('s'):
  ¬ ¬ ¬ ¬ player.control(0,5)
  
  
  ¬ ¬ if event.type == pygame.KEYUP:
  ¬ ¬ ¬ if event.key == pygame.K_LEFT or event.key == ord('a'):
  ¬ ¬ ¬ ¬ player.control(5,0)
  ¬ ¬ ¬ if event.key == pygame.K_RIGHT or event.key == ord('d'):
  ¬ ¬ ¬ ¬ player.control(-5,0)
  ¬ ¬ ¬ if event.key == pygame.K_UP or event.key == ord('w'):
  ¬ ¬ ¬ ¬ player.control(0,5)
  ¬ ¬ ¬ if event.key == pygame.K_DOWN or event.key == ord('s'):
  ¬ ¬ ¬ ¬ player.control(0,-5)
  ¬ ¬ ¬ # quitwatch
  ¬ ¬ ¬ if event.key == ord('q'):
  ¬ ¬ ¬ ¬ pygame.quit()
  ¬ ¬ ¬ ¬ sys.exit()
  ¬ ¬ ¬ ¬ main = False
  
  
  ¬ screen.fill(white)
  ¬ ledge_list.draw(screen)
  ¬ player.update(ledge_list)
  ¬ movingsprites.draw(screen)
  ¬ pygame.display.flip()
  ¬ clock.tick(fps)

scroll.py
---------

The source code with 
scrolling::

  #!/usr/bin/env python
  # GNU All-Permissive License
  # Copying and distribution of this file, with or without modification,
  # are permitted in any medium without royalty provided the copyright
  # notice and this notice are preserved.  This file is offered as-is,
  # without any warranty.
  
  
  import pygame
  import sys
  
  
  keyer = (255,255,255)
  black = (0,0,0)
  white = (255,255,255)
  screenX = 960
  screenY = 720
  screenU = 680
  
  
  forwardX = 590
  backwardX = 230
  tilesize = 32
  fps = 40
  afps = 4
  clock = pygame.time.Clock()
  main = True
  
  
  # generate platforms
  class Platform(pygame.sprite.Sprite):
  ¬ def createBlock(self,x,y,width,height):
  ¬ ¬ self.image = pygame.Surface([width,height])
  ¬ ¬ self.rect = self.image.get_rect()
  ¬ ¬ self.rect.y = y
  ¬ ¬ self.rect.x = x
  ¬ def __init__(self,x,y,width,height):
  ¬ ¬ pygame.sprite.Sprite.__init__(self)
  ¬ ¬ self.createBlock(x,y,width,height)
  
  
  # generate player sprite
  class Player(pygame.sprite.Sprite):
  ¬ momentumX = 0
  ¬ momentumY = 0
  ¬ frame = 0
  ¬ def __init__(self):
  ¬ ¬ pygame.sprite.Sprite.__init__(self)
  ¬ ¬ self.images = []
  ¬ ¬ for i in range(1,9):
  ¬ ¬ ¬ img = pygame.image.load("hero" + str(i) + ".png").convert()
  ¬ ¬ ¬ img.set_colorkey(keyer)
  ¬ ¬ ¬ self.images.append(img)
  ¬ ¬ ¬ self.image = self.images[0]
  ¬ ¬ ¬ self.rect = self.image.get_rect()
  ¬ def control(self,x,y):
  ¬ ¬ self.momentumX += x
  ¬ ¬ self.momentumY += y
  
  
  ¬ # update method + blocker argument 
  ¬ # to bring in ledge_list data
  ¬ def update(self, blocker):
  ¬ ¬ # collision horizontal
  ¬ ¬ currentX = self.rect.x
  ¬ ¬ nextX = currentX+self.momentumX
  ¬ ¬ self.rect.x = nextX
  
  
  ¬ ¬ # collision vertical
  ¬ ¬ currentY = self.rect.y
  ¬ ¬ nextY = currentY+self.momentumY
  ¬ ¬ self.rect.y = nextY
  
  
  ¬ ¬ # moving left
  ¬ ¬ if self.momentumX < 0:
  ¬ ¬ ¬ self.frame += 1
  ¬ ¬ ¬ if self.frame > 3*afps:
  ¬ ¬ ¬ ¬ self.frame = 0
  ¬ ¬ ¬ self.image = self.images[self.frame//afps]
  
  
  ¬ ¬ # moving right
  ¬ ¬ if self.momentumX > 0:
  ¬ ¬ ¬ self.frame += 1
  ¬ ¬ ¬ if self.frame > 3*afps:
  ¬ ¬ ¬ ¬ self.frame = 0
  ¬ ¬ ¬ self.image = self.images[self.frame//afps+4]
  
  
  ¬ ¬ # detect collisions with ledges via 'blocker' argument
  ¬ ¬ block_hit_list = pygame.sprite.spritecollide(self, blocker, False)
  ¬ ¬ for True in block_hit_list:
  ¬ ¬ ¬ self.rect.y = currentY
  ¬ ¬ ¬ self.rect.x = currentX
  
  
  # calculate where all the platforms go
  def createWorld():
  ¬ ledge_list = pygame.sprite.Group()
  ¬ ledge = Platform(10,200,448,32)
  ¬ ledge_list.add(ledge)
  ¬ ledge = Platform(480,350,320,32)
  ¬ ledge_list.add(ledge)
  ¬ ledge = Platform(900,404,64,32)
  ¬ ledge_list.add(ledge)
  ¬ return ledge_list
  
  
  pygame.init()
  screen = pygame.display.set_mode([screenX,screenY])
  ledge_list = createWorld()
  player = Player()
  player.rect.x = 50
  player.rect.y = 521
  movingsprites = pygame.sprite.Group()
  movingsprites.add(player)
  
  
  while main == True:
  ¬ for event in pygame.event.get():
  ¬ ¬ if event.type == pygame.QUIT:
  ¬ ¬ ¬ pygame.quit(); sys.exit()
  ¬ ¬ ¬ main = False
  
  
  ¬ ¬ if event.type == pygame.KEYDOWN:
  ¬ ¬ ¬ if event.key == pygame.K_LEFT or event.key == ord('a'):
  ¬ ¬ ¬ ¬ player.control(-5,0)
  ¬ ¬ ¬ if event.key == pygame.K_RIGHT or event.key == ord('d'):
  ¬ ¬ ¬ ¬ player.control(5,0)
  ¬ ¬ ¬ if event.key == pygame.K_UP or event.key == ord('w'):
  ¬ ¬ ¬ ¬ player.control(0,-5)
  ¬ ¬ ¬ if event.key == pygame.K_DOWN or event.key == ord('s'):
  ¬ ¬ ¬ ¬ player.control(0,5)
  
  
  ¬ ¬ if event.type == pygame.KEYUP:
  ¬ ¬ ¬ if event.key == pygame.K_LEFT or event.key == ord('a'):
  ¬ ¬ ¬ ¬ player.control(5,0)
  ¬ ¬ ¬ if event.key == pygame.K_RIGHT or event.key == ord('d'):
  ¬ ¬ ¬ ¬ player.control(-5,0)
  ¬ ¬ ¬ if event.key == pygame.K_UP or event.key == ord('w'):
  ¬ ¬ ¬ ¬ player.control(0,5)
  ¬ ¬ ¬ if event.key == pygame.K_DOWN or event.key == ord('s'):
  ¬ ¬ ¬ ¬ player.control(0,-5)
  ¬ ¬ ¬ # quitwatch
  ¬ ¬ ¬ if event.key == ord('q'):
  ¬ ¬ ¬ ¬ pygame.quit()
  ¬ ¬ ¬ ¬ sys.exit()
  ¬ ¬ ¬ ¬ main = False
  
  
  ¬ # if player is at the forwardX mark,
  ¬ # then move the world around him
  ¬ ¬ if player.rect.x >= forwardX:
  ¬ ¬ scroll = player.rect.x - forwardX
  ¬ ¬ player.rect.x = forwardX
  ¬ ¬ for platform in ledge_list:
  ¬ ¬ ¬ platform.rect.x -= scroll
  
  
  ¬ # if player is at the backwardX mark,
  ¬ # then move the world around her
  ¬ if player.rect.x <= backwardX:
  ¬ ¬ scroll = backwardX - player.rect.x
  ¬ ¬ player.rect.x = backwardX
  ¬ ¬ for platform in ledge_list:
  ¬ ¬ ¬ platform.rect.x += scroll
  
  
  ¬ screen.fill(white)
  ¬ ledge_list.draw(screen)
  ¬ player.update(ledge_list)
  ¬ movingsprites.draw(screen)
  ¬ pygame.display.flip()
  ¬ clock.tick(fps)
