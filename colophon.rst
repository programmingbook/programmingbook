Colophon
========

Being a bit of a geek, I'm always interested in hearing about how
something was made. So for you production nerds out there, here are
some details about the production of this book.

It was originally written in Docbook using `GNU Emacs
<https://gnu.org/software/emacs/>`_ in `nxml-mode
<http://www.emacswiki.org/emacs/NxmlMode>`_.

Later, I ported the whole thing to `Sphinx <http://sphinx-doc.org/>`_,
a Python- and reStructuredText- based documentation system. I migrated
it to Sphinx partly because I wanted to learn Sphinx, but also because
when translating Docbook to HTML, the continuity of broken numbered
lists was lost. I could have manually numbered the lists in Docbook
but that felt very un-Docbook-like and I just couldn't bring myself to
do that. (Except that I *did* do just that with Sphinx, but it felt
more acceptable there.)

For the Sphinx port, I edited in Gnu Emacs with its presumably builtin
ReST mode.

The book was written in `Slackware <http://slackware.com>`_ and `Slax
<http://slax.org>`_, both being freely available and freely modifiable
Linux distributions particularly, in my humble opinion, well-suited
for programming, writing, multimedia, and, well, everything else.

Feedback and suggestions popped up, quite unexpectedly, from the Linux
community at large. Particularly helpful have been Ken Fallon,
Jonathan Rogers, and Garjola.

The cat drawing for _The Floor is Lava_ example was done by Jess
Weichler. It was drawn by hand; I scanned it using XSane and traced
over it in either `GIMP <http://gimp.org>`_ or `Inkscape
<http://inkscape.org>`_.

Coffee was provided (usually for a fee) by Orbis Cafe and Tazza d'Oro.
