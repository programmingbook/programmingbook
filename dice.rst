Dice Rolling in BASH
====================

Your first programme in BASH will be a dice rolling game that pits the
user against the computer. The user will virtually roll some dice, and
the computer will roll some dice, and if the numbers match then the
user wins.

Pseudo Code
-----------

Knowing that that is the structure of your game, try to imagine how
you would implement such a game in code. Since you don't know any code
yet, this might sound like an impossible task, but think of it in
broad and generic terms, even if you only imagine it in the real
world.

For instance:

#. First, you would need dice.
#. After the die are rolled, you have two numbers: one for the first
   player and one for the second.
#. Once you have two numbers, you must compare them.
#. If the numbers are the same, then you can pronounce a winner. If
   they differ, you can pronounce a loser.

Generating Numbers
------------------

Starting out very simplistically, the first task, then, is to get two
semi-random numbers from BASH. There are at least two commands that
would provide such a result; one you might be able to think of one
just by thinking of normal things that computers do, like telling time
for instance. Yes, the `date` command is a great tool for generating
pseudo-random numbers::
  # date
  Fri Jul 26 21:29:41 EDT 2013

That might not seem very useful, but if you were to read the
documentation of `date`, you would see that `date` can provide the
current time in a variety of ways. Perhaps seeing the time nanoseconds
would be more useful::
  # date +%N
  624942771
  # date +%N
  539130670

Much better. In fact, it's overkill in a way, because the game only
requires one digit per roll. To filter the long nanosecond string down
to just one digit, you can redirect the output of date such that it
passes through the `cut` command first. `Cut` allows you to lift out a
single character (or more, if you please) from any string of
characters::
  # date +%N | cut -b1
  4
  # date +%N | cut -b7
  2

The `-b` switch in the `cut` command defines which character
(actually, which byte) to pull from the input. You can take the first
character, the second, third, or whatever you want. Whatever you
choose, you now have a fairly unpredictable method for generating
numbers on demand.

.. tip:: A second way to generate numbers in BASH is to use the
	 built-in random function::
	   # echo $RANDOM | cut -b1
	   9

	 Both work equally as well, and both are only pseudo-random,
	 since computers cannot generate truly random
	 numbers. However, it will be a re-occuring theme in this book
	 and in your future programming efforts that there is rarely
	 only *one* way to solve a problem when coding.

Creating Variables
------------------

Now that you have the means of obtaining a random number from BASH,
you need to assign the number to each participant of the game; in this
case, that's the user and the computer. 

When you want to store information in a programme, you use a
variable. Variables are the building blocks of code and you will use
them in any language you use on any kind of project you embark upon,
so get used to defaulting to them::
  # PLAYER=$(date +%N | cut -b1)
  # COMPUTER=$(date +%N | cut -b2)

Now there are two variables ( `PLAYER` and `COMPUTER`), each of which
contain the results of the date/cut command we invented for our random
number generator. You can see the results using an echo statement::
  # echo $PLAYER
  2
  # echo $COMPUTER
  3

You could leave it up to the user to read the results of the game and
decide whether the game was won or lost, but obviously in real
computer games this is determined for the user, so we should have the
computer analyze the results and announce the outcome.

.. note:: The importance of variables cannot be overstated. At least
	  third of all problems encountered by beginning programmers
	  can be solved simply by remembering to use variables. When
	  in doubt, use a variable.

Comparing Variables
-------------------

Comparing the contents of two variables is a very common task in
programming. It can help the user control the application, it can help
the computer determine whether or not an option has been turned on or
off, and in games it can help detect collisions and health points, and
much more.

In this case, there are two possible outcomes; the user has
either won or the user has lost. Any time there are forks in a road
(so to speak) in a programme, you can bet that an `if` statement is
involved.

In BASH, an if-statement is structured like 
this::
  # if [ $PLAYER == $COMPUTER ]; then echo "You won!" ; else echo "You lost."; fi
  You lost.

And now the programme is done. You have successfully created random
numbers for two players, compared those numbers, and announced the
results. 

The question is, how can we make this game playable without
the user having to type all the code into a terminal? The answer is to
write the commands down as source code and run the application from
within a terminal. You'll do that soon, but first some further
practise with data flow. Onward to the next chapter, and to your next
BASH project.

Dice.sh
-------

The source 
code::
    #!/bin/bash
    # GNU All-Permissive License
    # Copying and distribution of this file, with or without modification,
    # are permitted in any medium without royalty provided the copyright
    # notice and this notice are preserved.  This file is offered as-is,
    # without any warranty.

    PLAYER=$(date +%N | cut -b1)
    COMPUTER=$(date +%N | cut -b3)

    if [ $PLAYER == $COMPUTER ]; then
    echo "You won"
    else
    echo "You lost"
    fi

    # EOF
   
