Appendix: Distribution
======================

Now that you're a real live software developer, the idea of installing
Python modules and using a terminal to launch applications seems all
but normal to you. But think back to before you started reading this
book and you may remember that most people have no idea how to do any
of that, and probably don't really want to, either.

That's one good reason to learn a little about packaging.

Another good reason is that even fearless programmers like you and I
sometimes just want to go and grab the latest stable release of an
application and run it like a normal person.

Packaging an application for distribution can be tricky because:

* Most operating systems have several ways for applications to be
  installed and run. For instance, Windows generally uses `.exe` or
  `.msi` files, Mac OS uses `.pkg` or `.mpkg` and `.app` or plain old
  binary executables, and Linux generally uses `.txz` or `.rpm` or
  `.deb` archives.

* It requires *you* to keep your code well-organized so that
  everything required to run your application is where your programme
  expects it to be.

In fact, there are two classic mistakes that new programmers make that
you should know about *now* so that you may avoid them:

#. **Do not** assume that the users of your application have computers
   that are set up with the exact same software, libraries, and operating
   system versions as the one you developed the application on.

   If you had to install something in order to write your code, then
   quite probably your target audience will need to install something
   in order to run it!

#. **Do** keep your source code, and all of the necessary assets that
   need to be distributed with your application (such as sound files,
   images, documentation, and so on), organized.

   When creating your programme, make sure that you consistently copy
   all of the assets you depend upon into a central location. In this
   book, everything got copied into the same directory as the
   application you were writing to keep things simple. It's even
   better to create sub-directories in your code project, such as
   `/images`, `/sounds`, and so on. Either way, make sure that
   everything your application needs is all within one parent
   directory. The more self-contained your project is, the easier it
   is to re-distribute.

Keeping those principles in mind, even as you begin your journey as a
programmer, will make the transition to having "real" programmes that
you want other people to be able to obtain and install and use much
easier.

.. note:: This chapter covers distribution of your application, rather
          than installation. In other words, we are distributing a
          mostly self-contained executable version of your programme,
          intended to be run from one directory. We are not taking the
          files involved by your programme and placing them in the
          system locations defined by the user's operating system.

          It is arguably better to install an application, because
          presumably it becomes fully integrated into the rest of the
          system. However, it's also a lot of work understanding how
          each OS needs its applications to be installed. So for now,
          we'll stick to simply packaging our code up and distributing
          it that way.

Pyinstaller
-----------

There are many ways to package up applications and you should favour
whatever native packaging tool exists for your target OS. If your
target OS is all OS's, then the simplest and most universal packaging
tool for Python applications is `pyinstaller
<http://www.slax.org/modules.php?detail=pyinstaller>`_.

Pyinstaller can be installed from the Slax module page, and it is also
available for other operating systems and Linux distributions.

Once installed, using it is generally as simple as one command.

#. First, navigate in your terminal to the folder containing your
   Python application::
     # cd ~/code/platformer
     # ls -F
     COPYING
     README
     TODO     
     images/
     platformer.py
     sounds/

#. Finally, execute the `pyinstaller` command::
     # pyinstaller platformer.py

#. After `pyinstaller` has run, you will find some new files in your
   code folder::
     # ls -F
     COPYING
     README
     TODO     
     build/
     dist/
     images/
     platformer.py
     platformer.spec
     sounds/

   In the `dist` folder, you will find a `dice` directory containing
   a self-contained Linux build of your application. It will run on
   a Linux system even if the user does not have, for instance, pygame
   installed.

#. Try launching the application::
     # ./platformer

   If the application does not launch, read the error and use your
   newfound programming skills to solve the problem. Probably, it is
   missing an asset file; for instance, if you are creating a
   self-contained version of a game, then you must copy your `images`
   and `sounds` directories (and any other non-code assets required
   for the application to run) into the distribution directory.

   An alternate way of managing external assets is to install them
   into some standard system location (such as /usr/share/ on Linux)
   but this is something that is done by the installer, and since it
   is OS-specific it must be done differently by each version of the
   installer. For that reason, it's easier at this stage to just place
   everything into the same folder as the executable and distribute it
   all in one directory.
