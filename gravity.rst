Gravity
=======

Right now the obstacles you have created in your world are bird's-eye-
views of walls, but what if you turned the game world on its side and
turned them into platforms? In order to do this, you'll need to
conjure the mysterious forces of gravity. 

To implement gravity, there are a few new variables that must be
created within the "natural state" of the `Player` class. Currently
the `Player` class begins with::

  class Player(pygame.sprite.Sprite):
  ¬ momentumX = 0
  ¬ momentumY = 0
  ¬ frame = 0

Those are the values that the `player` sprite starts out with. To this
list, add::

  class Player(pygame.sprite.Sprite):
  ¬ momentumX = 0
  ¬ momentumY = 0
  ¬ frame = 0
  ¬ # GRAVITY ADDITIONS
  ¬ jumping = False
  ¬ frame_since_collision = 0
  ¬ frame_since_jump = 0

You'll soon see how these get used.

Gravity and Collisions
----------------------

Gravity is affected by collision detection. Specifically, when an
object hits a solid object, at least in simplified physics, its
momentum is nullified. Therefore, in your code you should locate the
routine that checks for collisions and force a few changes to the
`player` sprite's condition.

Locate the `block_hit_list` code block and add two gravity-related
lines to it::

  ¬¬ for True in block_hit_list:
  ¬¬¬ self.rect.y = currentY
  ¬¬¬ self.rect.x = currentX

  ¬¬¬ ## GRAVITY ADDITIONS
  ¬¬¬ self.momentumY = 0
  ¬¬¬ self.frame_since_collision = 0

Below the gravity comment, the first line sets `self.momentumY` to 0
for fairly obvious reasons: if there is a collision, then stop the
player sprite's momentum. 

The last line sets `self.frame_since_collision` to 0. Resetting this
value is a little meaningless to you now because you have not actually
used the value yet, but in the next block of code you will, and its
significance should become clearer then. For now, just know that
setting the `frame_since_collision` variable to 0 acts as a counter
for the ingame physics so that Pygame knows that the player has
recently landed on some object, and so it safe to once again start
listening for a signal to jump.

Jumping
-------

Gravity is no fun without jumping and currently there is no system to
enable the `player` sprite to jump. There are two components to
implementing jumping; one is a control component, so that the user has
a way on the keyboard to trigger a jump, and the other is adjusting
the game physics to allow the `player` sprite to become airborn
temporarily.

First, you need a method to handle jumping. You implement this within
the `Player` class::
  01. ¬ def jump(self,blocker):
  02. ¬¬ self.jumping = True
  03. ¬¬ self.frame_since_jump = 0

1. The first line creates the method and calls it `jump`, requiring
   its `self` and `blocker` as arguments. This means that all collision
   data will be available to it, which is certainly important when your
   player is flying around through the air.
2. Next, the variable `self.jumping` is set to True. It was False by
   default.
3. And finally, `self.frame_since_jump` is set to 0 because if the
   player has just jumped, then the counter that tracks whether the
   player is airborn or not should be reset, because the player has
   *just* jumped.

You will notice that the method allowing the player to jump does not
really contain code having anything to do with motion or movement at
all. That might surprise you but you must understand that "jumping" is
not a real action in a game world that, in reality, has no gravity. So
all this method really does is set a few variables that will alert
other portions of the code that a jump signal has been triggered. And
then *those* lines of code will manipulate the player's onscreen
position, momentum, and downward "fall".

The `Player` class gets your code handling the motion of jumping. So,
within the `Player` class's `update` method, add this block of code::
  04. ¬¬ if self.frame_since_collision < 6 and self.frame_since_jump < 6:
  05. ¬¬¬ self.frame_since_jump = 100
  06. ¬¬¬ self.momentumY -= 8
    
  07. ¬¬ self.frame_since_collision += 6
  08. ¬¬ self.frame_since_jump += 6

4. Indented twice so that it is a part of the `update` method of the
   `Player` class, this line starts an if-statement dependent upon the
   current value of `self.frame_since_collision` being less than 6 *and*
   (not "or") the value of `self.frame_since_jump` being less than 6. In
   other words, the player must have just collided into some object and
   must not have jumped within the past 6 frames.
5. If those conditions are met, then this next line sets
   `self.frame_since_jump` to 100. Why would you do this? Well, in so
   doing you prevent the player from jumping again while, more than
   likely, the player is already in mid-air.

   .. note:: You can leverage this knowledge later, however, when
	     designing power- ups or just changing the mechanics of
	     your gameplay. Maybe you'd like to grant your player the
	     ability to jump even when already flying through the air,
	     or maybe your game level is underwater and you want to
	     force your player to "swim" in order to keep afloat, or
	     maybe you just enjoy messing around with the way gravity
	     works. Whatever the reason, this is the line you'll want
	     to come back to should you want to bend the rules of
	     physics.

6. Sets the variables `self.momentumY` to its current value minus 8.
   The number 8 here is mostly arbitrary and you are free to experiment
   around with other values. The important thing to note here is that
   this line *directly controls* the height that a single jump carries
   the player sprite.

7. The next two lines increment frames; the first line increments the
   `self.frame_since_collision` by 6. This is a significant number
   because if all the for-loops and if-statements have been passed over,
   then with each tick of the Pygame clock, the frames counting the time
   since the player's last collision should be incremented so that you
   can track whether or not the player should be falling, or jumping, or
   just moving, at any given moment.
   
   Setting the increment number to 6 automatically exceeds the
   permitted range for a new jump, thus preventing mid-air
   jumping. Try lowering the number to get in-air jumps, wall-jumps,
   and other neat mechanics; setting it to 1 nearly guarantees the
   opportunity to re-jump in mid-air and off of walls, while 3, for
   instance, only affords the possibility for a small window of time
   after the initial jump.

8. The second line increments `self.frame_since_jump`. Again, this is
   useful because you want to be able to control when the player sprite
   is permitted to jump.

You've taken care of the physics side of jumping, now you must modify
the keyboard controls.

The keyboard control scheme as it is now was basically copied directly
over from the previous game project and is modelled for top-down
gameplay. Now that you are introducing gravity into your world, the up
and down controls should behave differently; pressing and holding the
up key should not move the player at a steady, constant rate, but
instead only trigger the `jump` method that you added to the `Player`
class.

Changing the behaviour of the up and down controls is as simple as
locating the KEYDOWN section in the `main` loop and altering::
  ¬¬¬ if event.key == pygame.K_UP:
  ¬¬¬¬ player.control(0,-5)

to this::

  ¬¬¬ if event.key == pygame.K_UP:
  ¬¬¬¬ player.jump(ledge_list)

Such that an up-arrow or w triggers the `jump` method, bringing in all
the `ledge_list` data along with it so that collisions can be
detected.

You can *optionally* remove the downward control from the KEYDOWN
section, or you can leave it in, depending on whether or not you want
the user to be able to force the player avatar down from mid-
air. It's your choice of mechanics. 

You should remove both the up and down controls from the KEYUP section
because you do not want the release of either the up or down control
keys to trigger any event or have any affect on your player
sprite.

That said, this is another area that is ripe for experimentation. Many
different mechanics and bonus moves can be discovered by changing the
behaviours of the up and down controls, so feel free to return to this
later and change the settings to see what happens.

Calculating Gravity
-------------------

Now that the player can jump, you must finally create a method to
calculate the affects of gravity with each tick of the Pygame clock.
This is yet another method within the `Player` class, the job of which
is to contantly diminish the value of `momentumY`, just as gravity
does in the real world (in our simplified physics model, anyway;
certainly our ingame gravity is not adding mass to our objects, but
the general effect is the same)::
  09. ¬ def gravity(self):
  10. ¬¬ self.momentumY += .17
  11. ¬¬ if self.rect.y >= screenU and self.momentumY >= 0:
  12. ¬¬¬ self.momentumY = 0
  13. ¬¬¬ self.rect.y = screenU
  14. ¬¬¬ self.frame_since_collision = 0

9. The first line creates the method and calls it `gravity`, requiring
   nothing but the data of the player sprite as its argument.
10. The variable `self.momentumY` is set to its current value plus 0.17.
   This number is entirely arbitrary and you should feel free to change
   it to see how it affects your world's gravitational pull.

   If you look back at the code determining whether or not the player
   is in mid-air or not, you see that if the player *is* determined to
   be airborn, then the `momentumY` is set to itself minus 8. This
   method is constantly, without condition, adding `y` momentum,
   meaning that the player sprite is being "pulled" by gravity to
   lower pixel numbers. Since the top of the Pygame screen is 0 and
   the bottom is some higher number, the player sprite is constantly
   being pulled to the bottom of the screen.

   Increase the value of this number, and the heavier your
   gravitational pull will be. Ingame, not in real life.

11. Of course, the problem is that if the player sprite is constantly
   being tugged downward, it would naturally disappear right off the
   screen. Adding some parameters to the gravitational pull is important;
   in this code block, you start an if-statement which checks whether the
   location of the player sprite is greater than or equal to the value of
   `screenU` (the "ground" of your game world, which may or may not also
   be the bottom of the Pygame window, depending on how you are
   structuring your game) *and* if the value of `momentumY` is greater
   than or equal to 0.
12. If both of those conditions are met (if the player sprite is
   greater than or equal to ground level and it has `y` momentum or no
   `y` momentum, then this line sets `self.momentumY` to 0 so that the
   player sprite does *not* continue to descend past `screenU`.
13. This line sets the location of the sprite to the same value as
   `screenU` just to ensure that the sprite "lands" on the ground.
   Without this, there can sometimes be glitches so that the player
   sprite appears to be one or two pixels just above the ground, which
   can get annoying.
14. And finally it re-sets the counter tracking when the player sprite
   last came into contact with a solid object. With this counter reset to
   0, the rest of your code will know that it is safe to permit the
   player sprite to jump again.

Death by Gravity
----------------

If you game consists exclusively of platforms, you might want your
player sprite to die when it falls off of a platform. You can probably
figure out how to implement that code; instead of the above block,
which defines the ground, insert this simple if-statement into the
`gravity` method::

  ¬ if self.rect.y > screenU:
  ¬¬ sys.exit()

If the sprite's location exceeds the number contained in `screenU`,
then the game quits.

In a real game, you would want to implement some indication that the
player had died, and probably just re-spawn the avatar at some
previous checkpoint, but for now perma-death is good enough.

Gravity in the Main Loop
------------------------

The last line of code you need to insert for gravity to become a
reality is within the `main` loop. Of course, you already update the
player sprite and the platforms, but now that you have a gravity
method you must also update it. Find this block of code in your `main`
loop::
  ¬ player.update(ledge_list)
  ¬ movingsprites.draw(screen)

And add a gravity update::

    ¬ player.gravity()
    ¬ player.update(ledge_list)
    ¬ movingsprites.draw(screen)

Try the game and witness the physics engine you have created. Also, go
back through your code and try changing some of the values in the jump
and gravity calculations to see the affect they have on your game.

gravity.py
----------

The source 
code::
  #!/usr/bin/env python
  # GNU All-Permissive License
  # Copying and distribution of this file, with or without modification,
  # are permitted in any medium without royalty provided the copyright
  # notice and this notice are preserved.  This file is offered as-is,
  # without any warranty.
  
  
  import pygame
  import sys
  
  
  keyer = (255,255,255)
  black = (0,0,0)
  white = (255,255,255)
  screenX = 960
  screenY = 720
  screenU = 680
  
  
  forwardX = 590
  backwardX = 230
  tilesize = 32
  fps = 40
  afps = 4
  clock = pygame.time.Clock()
  main = True
  
  
  # generate platforms
  class Platform(pygame.sprite.Sprite):
  ¬ def createBlock(self,x,y,width,height):
  ¬ ¬ self.image = pygame.Surface([width,height])
  ¬ ¬ self.rect = self.image.get_rect()
  ¬ ¬ self.rect.y = y
  ¬ ¬ self.rect.x = x
  ¬ def __init__(self,x,y,width,height):
  ¬ ¬ pygame.sprite.Sprite.__init__(self)
  ¬ ¬ self.createBlock(x,y,width,height)
  
  
  # generate player sprite
  class Player(pygame.sprite.Sprite):
  ¬ momentumX = 0
  ¬ momentumY = 0
  ¬ frame = 0
  ¬ # gravity variables
  ¬ jumping = False
  ¬ frame_since_collision = 0
  ¬ frame_since_jump = 0
  ¬ def __init__(self):
  ¬ ¬ pygame.sprite.Sprite.__init__(self)
  ¬ ¬ self.images = []
  ¬ ¬ for i in range(1,9):
  ¬ ¬ ¬ img = pygame.image.load("hero" + str(i) + ".png").convert()
  ¬ ¬ ¬ img.set_colorkey(keyer)
  ¬ ¬ ¬ self.images.append(img)
  ¬ ¬ ¬ self.image = self.images[0]
  ¬ ¬ ¬ self.rect = self.image.get_rect()
  ¬ def control(self,x,y):
  ¬ ¬ self.momentumX += x
  ¬ ¬ self.momentumY += y
  
  
  ¬ # update method + blocker argument 
  ¬ # to bring in ledge_list data
  ¬ def update(self, blocker):
  ¬ ¬ # collision horizontal
  ¬ ¬ currentX = self.rect.x
  ¬ ¬ nextX = currentX+self.momentumX
  ¬ ¬ self.rect.x = nextX
  
  
  ¬ ¬ # collision vertical
  ¬ ¬ currentY = self.rect.y
  ¬ ¬ nextY = currentY+self.momentumY
  ¬ ¬ self.rect.y = nextY
  
  
  ¬ ¬ # moving left
  ¬ ¬ if self.momentumX < 0:
  ¬ ¬ ¬ self.frame += 1
  ¬ ¬ ¬ if self.frame > 3*afps:
  ¬ ¬ ¬ ¬ self.frame = 0
  ¬ ¬ ¬ self.image = self.images[self.frame//afps]
  
  
  ¬ ¬ # moving right
  ¬ ¬ if self.momentumX > 0:
  ¬ ¬ ¬ self.frame += 1
  ¬ ¬ ¬ if self.frame > 3*afps:
  ¬ ¬ ¬ ¬ self.frame = 0
  ¬ ¬ ¬ self.image = self.images[self.frame//afps+4]
  
  
  ¬ ¬ # detect collisions with ledges via 'blocker' argument
  ¬ ¬ block_hit_list = pygame.sprite.spritecollide(self, blocker, False)
  ¬ ¬ ¬ for True in block_hit_list:
  ¬ ¬ ¬ ¬ self.rect.y = currentY
  ¬ ¬ ¬ ¬ self.rect.x = currentX
  ¬ ¬ ¬ ¬ # gravity
  ¬ ¬ ¬ ¬ self.momentumY = 0
  ¬ ¬ ¬ ¬ self.frame_since_collision = 0
  
  
  ¬ ¬ # gravity
  ¬ ¬ if self.frame_since_collision < 6 and self.frame_since_jump < 6:
  ¬ ¬ ¬ self.frame_since_jump = 100
  ¬ ¬ ¬ self.momentumY -= 8
  ¬ ¬ ¬ # increment frames since collision and jump
  ¬ ¬ ¬ # set to 6 for no in-air jumps
  ¬ ¬ ¬ # set to less for possible in-air and wall- jumps
  ¬ ¬ ¬ self.frame_since_collision += 3
  ¬ ¬ ¬ self.frame_since_jump += 3
  
  
  ¬ # anti gravity
  ¬ def jump(self,blocker):
  ¬ ¬ self.jumping = True
  ¬ ¬ self.frame_since_jump = 0
  
  
  ¬ def gravity(self):
  ¬ ¬ # greater number here = heavier gravity
  ¬ ¬ self.momentumY += .27
  ¬ ¬ # use screenU as "the ground" so we don't die
  ¬ ¬ if self.rect.y > screenU and self.momentumY >= 0:
  ¬ ¬ ¬ self.momentumY = 0
  ¬ ¬ ¬ # or we could use it to create death
  ¬ ¬ ¬ # pygame.QUIT; sys.exit()
  ¬ ¬ ¬ self.rect.y = screenU
  ¬ ¬ ¬ self.frame_since_collision = 0
  
  
  # calculate where all the platforms go
  def createWorld():
  ¬ ledge_list = pygame.sprite.Group()
  ¬ ledge = Platform(10,200,448,32)
  ¬ ledge_list.add(ledge)
  ¬ ledge = Platform(480,350,320,32)
  ¬ ledge_list.add(ledge)
  ¬ ledge = Platform(900,404,64,32)
  ¬ ledge_list.add(ledge)
  ¬ return ledge_list
  
  
  pygame.init()
  screen = pygame.display.set_mode([screenX,screenY])
  ledge_list = createWorld()
  player = Player()
  player.rect.x = 50
  player.rect.y = 521
  movingsprites = pygame.sprite.Group()
  movingsprites.add(player)
  
  
  while main == True:
  ¬ for event in pygame.event.get():
  ¬ ¬ if event.type == pygame.QUIT:
  ¬ ¬ ¬ pygame.quit(); sys.exit()
  ¬ ¬ ¬ main = False
  
  
  ¬ ¬ if event.type == pygame.KEYDOWN:
  ¬ ¬ ¬ if event.key == pygame.K_LEFT or event.key == ord('a'):
  ¬ ¬ ¬ ¬ player.control(-5,0)
  ¬ ¬ ¬ if event.key == pygame.K_RIGHT or event.key == ord('d'):
  ¬ ¬ ¬ ¬ player.control(5,0)
  ¬ ¬ ¬ if event.key == pygame.K_UP or event.key == ord('w'):
  ¬ ¬ ¬ ¬ player.jump(ledge_list)
  ¬ ¬ ¬ #if event.key == pygame.K_DOWN or event.key == ord('s'):
  ¬ ¬ ¬ ¬ #player.control(0,5)
  
  
  ¬ ¬ if event.type == pygame.KEYUP:
  ¬ ¬ ¬ if event.key == pygame.K_LEFT or event.key == ord('a'):
  ¬ ¬ ¬ ¬ player.control(5,0)
  ¬ ¬ ¬ if event.key == pygame.K_RIGHT or event.key == ord('d'):
  ¬ ¬ ¬ ¬ player.control(-5,0)
  ¬ ¬ ¬ #if event.key == pygame.K_UP or event.key == ord('w'):
  ¬ ¬ ¬ ¬ #player.control(0,5)
  ¬ ¬ ¬ #if event.key == pygame.K_DOWN or event.key == ord('s'):
  ¬ ¬ ¬ ¬ #player.control(0,-5)
  ¬ ¬ ¬ # quitwatch
  ¬ ¬ ¬ ¬ if event.key == ord('q'):
  ¬ ¬ ¬ ¬ pygame.quit()
  ¬ ¬ ¬ ¬ sys.exit()
  ¬ ¬ ¬ ¬ main = False
  
  
  ¬ # if player is at the forwardX mark,
  ¬ # then move the world around him
  ¬ if player.rect.x >= forwardX:
  ¬ ¬ scroll = player.rect.x - forwardX
  ¬ ¬ player.rect.x = forwardX
  ¬ ¬ for platform in ledge_list:
  ¬ ¬ ¬ platform.rect.x -= scroll
  
  
  ¬ # if player is at the backwardX mark,
  ¬ # then move the world around her
  ¬ if player.rect.x <= backwardX:
  ¬ ¬ scroll = backwardX - player.rect.x
  ¬ ¬ player.rect.x = backwardX
  ¬ ¬ for platform in ledge_list:
  ¬ ¬ ¬ platform.rect.x += scroll
  
  
  ¬ screen.fill(white)
  ¬ ledge_list.draw(screen)
  ¬ player.gravity()
  ¬ player.update(ledge_list)
  ¬ movingsprites.draw(screen)
  ¬ pygame.display.flip()
  ¬ clock.tick(fps)
