Programming Book
================

Learn to Programme in Just 3,873 Easy Steps
-------------------------------------------

Edition 1.5

Seth Kenlon
~~~~~~~~~~~
Slackermedia Documentation seth@straightedgelinux.com

Klaatu la Terible
~~~~~~~~~~~~~~~~~
Slackermedia Documentation klaatu@member.fsf.org

Legal Notice
============
Copyright © 2012 Seth Kenlon. This material may only be distributed
subject to the terms and conditions set forth in the GNU Free
Documentation License (GFDL), V1.2 or later (the latest version is
presently available at `http://www.gnu.org/licenses/fdl.txt <http://www.gnu.org/licenses/fdl.txt>`_).


Abstract
--------
For people who don't think like programmers, by a person who doesn't
think like a programmer, this book teaches the art of hacking code
with Linux, the scripting language BASH, the object oriented language
Python, and assorted Python modules including Pygame. This book's goal
is not to turn the reader into a professional programmer over night.
Instead, it teaches the reader how to start thinking like a
programmer, how to read code, and how to write code. Most importantly,
this book arms the reader with all the knowledge needed to start
writing simple but helpful programmes, and with the ability to
intelligently seek out new programming techniques useful in writing
more ambitious applications.

Contents
--------
.. toctree::
   :maxdepth: 2
   :numbered:

   intro
   linux
   apps
   bash
   dice
   rolodex
   rolodim
   rolodux
   procedural
   python
   pygame
   platformer
   pythondice
   animation
   collisionscroll
   gravity
   tilemapping
   pyqt
   pyqtxt
   reflection
   next
   mtpaint
   kolourpaint
   distribution
   colophon
