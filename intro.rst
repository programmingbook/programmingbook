Who Should Read This Book
=========================

Programmers and people who write books on programming (present company
excluded) tend to believe that the rest of the world's population have
brains that work the way *their* brains work. They assume that we
understand the basic concepts behind logic and math and programming
and hard stuff like that. They imagine we might even spend an afternoon
thinking about code and how to solve problems with code. They therefore 
write books that explain programming under those assumptions.

Fact of the matter is, most people (myself included) have no clue how
programming works. To be honest, even programmers don't really know
how programming works because they don't build CPUs in their spare
time. So they know what they know, but when those fancy programming
terms get converted into binary sequences, it becomes a mystery all
over again.

My point is that in computing, there are layers upon layers of
information that we all must choose to either understand, or to accept
on faith. Now, if your goal in life is to become an electrical
engineer or a CPU designer, or you want to write a code compiler or
decompiler, then you might need to have a deep understanding of binary
instruction sets and how signals are processed and so on. But if your
intent is to learn more about how programming works, and how you might
be able to wrangle those quirky home appliances we call "computers",
then this book is for you.

I cannot honestly say that there is an effective and easy way to
programme. If programming was easy, then everyone would do it, because
everyone has their own unique set of problems that they would like to
solve with computers. And who wouldn't want a completely customized
solution that works exactly how they want it to work? But all the
attempts at making simple, no-brainer programming applications have
rendered clunky, bloated, slow scripting engines that produce
"applications" that are clunky, slow, buggy, inflexible, and difficult
to re-distribute.

That's not to say they *never* work, but if you're looking to
understand the art of programming so that you can make applications
that you and other people can happily use, and so that you will know
how to continue to grow your knowledge in the future, then you just
have to sit down and study.

If you're willing to put a little work into it, this book will enable
you to do just those things.

But be warned, dear reader, that programming is difficult, confusing,
backwards, logical to a fault, and supremely unattainable. It's hard
work.

By the end of this book, you will *not* be a programmer. You will have
learnt how to learn a programming language, how to follow its
syntactical rules, and how to weild all the common programming
constructs like variables, functions, classes, and so on. In other
words, you will be fully prepared to continue learning.

That is, after all, what programming really boils down to; continuous
learning. You can ask any self-respecting programmer and they will
tell you that they are always pushing themselves to learn new things
about the languages they love, and about new languages that they find
themselves needing to use. This isn't a warning to you so much as it
is a feature of being a programmer. If you love to learn, and you do
not want to stop learning, then you just might love programming,
because it's full of innovation and new features and new theories and
structures, and you get to keep up with it and emmerse yourself in it
as much and as often as you want.

Besides, as I've found, programming is sometimes something you
actually really *need* to do, and once you start doing it, you start
seeing how it really is solving your problems. And then you start,
incredibly, to understand how it works. At that point, you won't be
able to stop programming. You'll want to keep doing it because, as it
turns out, you enjoy it after all.

Still with me? OK, well let's get started. Don't expect college-level
exams and fancy lingo; we're just gonna whip up some code and be done
with it. And we're going to keep doing that until we actually start
understanding why it works.

How to Use This Book
--------------------

This book is not an academic study of programming, but neither is it a recipe book. It is a gentle introduction to the world of active and productive computing. Each chapter should be read in one sitting, and you should be prepared to do a little homework to add to what you have learnt. I encourage you to attempt the homework assignments even if you utterly fail at them. Believe it or not, the more you try, the better you become, even though some of your early attempts may feel frustrating to you. The important thing to remember is, in order to programme, you must, well, programme.

Therefore, read each chapter carefully, studying both the sample code and the explanations of what the sample code is doing, and *why* it works. If you don't fully understand, go back and read it again. Each chapter *can* be fully understood, as long as you really think about what's going on. You don't necessarily have to retain all of that knowledge; repitition will take care of that. However, you should, at least whilst reading, understand the logic and syntax of the code.

Sample Code
-----------

All code used in this book can be found at the end of each chapter of
this book. The files themselves and many of the non-code assets
can be downloaded from
`straightedgelinux.com/blog/python/codesamples.tar.gz <http://straightedgelinux.com/blog/python/codesamples.tar.gz>`_

.. note:: It doesn't apply much to coding, but incidentally, the
	  source code *of* the book itself is also available
	  online at `gitorious.org/programmingbook <http://gitorious.org/programmingbook/>`_.

The code in this book was mostly written specifically for this book,
although in typical open source fashion, we've borrowed a lot from
examples found in documentation and from other programmers online. The
code is licensed under the GNU All-Permissive license, which means,
basically, that you can do anything you want with the code (including
claiming it as your own; no attribution required).

While the code download is helpful in avoiding typing errors and other
silly mistakes that you might make by manually entering the code,
there is a lot to be said for typing in the code yourself. It helps
you internalise what the book is saying, and repetition will help you
pick up on patterns in code. Eventually, advanced concepts such as
classes and methods and instantiating variables will not seem at all
foreign, because you yourself will have written them a dozen times
already. So as much as possible, type in the code from the book, and
try to run it. If you run into problems, then download and try the
code samples from the website; if it runs, you know that the problem
was yours (probably just a typo), so look for differences between the
files to find out what went wrong.

The book's website, `straightedgelinux.com/blog/python <http://straightedgelinux.com/blog/python/>`_, has a
copy of this text available in html and will continue to be updated as
needed should there be major changes or error corrections required.
