Appendix: KolourPaint
=====================

If you are doing your own art for your game, or you need an
application to make changes to existing art, try KolourPaint , a basic
paint application that is included with Slax's KDE desktop.

.. note:: Pixel-painting applications produce bitmap images as opposed
	  to vector images. It's common for 2d games to use bitmap
	  graphics, although vector images are considered more
	  flexible. If you are experienced with vector art, you can
	  use Inkscape , available from `inkscape.org
	  <http://inkscape.org>`_, for your artwork, converting to
	  bitmap exclusively for the game. Save your vector sources in
	  case you ever need to re- visit your game to up-res it. 

If you have ever used a bitmap paint programme then KolourPaint will
be mostly intuitive. For creating game assets, its basic workflow
would probably go something like this:

#. Launch KolourPaint from the K-menu in the lower left corner of your
   Slax desktop.
#. Set your canvas size to the tilesize dimension you want to use in
   your game. Your character sprites do not need to be constrained to the
   tilesize dimensions unless you intend to tile them for some reason.
   Either way, assume you are designing something that is 128 pixels by
   128 pixels. Go to the Image menu and select Resize/Scale , or just use
   control-e.
#. In the Resize/Scale window, set the dimensions of your canvas to
   128 by 128 and click OK.
#. Magnify the canvas until it at least fills your screen by using the
   Zoom buttons in the top menu bar, or by using the control-+ or
   control-- keyboard shortcuts. When working on small documents on a
   pixel-level, it might be useful to have a separate window displaying a
   100% sized version of your work. For this, go to the View menu and
   select the Show Thumbnail option, or just use control-h shortcut.
#. Activate the grid view, if you like, with Show Grid in the View
   menu, or by using the control-g keyboard shortcut. This displays an
   on-screen grid revealing the bounds of each pixel in your canvas.
#. Draw! er, Paint!

Tools
-----

Unlike many fancy "graphic design" applications, the tools in
KolourPaint are pretty straight-forward. The tool palette is found on
the far left of the KolourPaint screen.

Tools
;;;;;

:Selection (Free-Form): A "lasso" or free-hand selection tool that
  will select every pixel you surround within its bounds. After
  selecting, you can copy, cut, paste, affect, delete, move, or anything
  else you want to do to the pixels you have selected. To de-select,
  simply click somewhere outside the selection area.
:Selection (Elliptical): A selection tool that constrains itself to a
  circle or ellipse.
:Line: Draws single lines in any direction of any length. Drawing a
  line with Shift pressed constrains the line's angle by 45-degree
  increments; Control constrains them by 15-degrees.
:Eraser: Paints pixels to match the background colour. It does not
  actually erase pixels in the sense of producing an alpha channel as an
  eraser often does in some other graphic applications.
:Flood Fill: Fills in contiguous areas of uniform pixels with the
  foreground colour, including alpha (transparent)
:Colour Eraser: Erases any pixel that is the foreground colour and
  converts them to the background colour, and doesn't touch any pixel of
  any other colour. A very useful tool that is essentially "just" a
  replace-colour function, but it feels more interactive.
:Rounded Rectangle: Draws a rectangle with stylishly rounded corners.
  There are three modes for this tool, which can be set in the
  contextual options below the toolbox: No Fill , Fill with Background
  Colour , and Fill with Foreground Colour . You may also set the outer
  border size.
:Polygon: Draws polygons with the same options as the Rounded
  Rectangle tool. The polygon tool will remain active until you right-
  click to finalize the shape, adding new sides to your shape for as
  long as you keep moving and clicking the mouse.
:Connected Lines: Similar to the Polygon tool, this tool will connect
  progressive dots with a line for as long as you move your mouse and
  click. To finalize a shape, just right-click.
:Zoom: Selects an area to zoom into.
:Selection (Rectangular): A selection tool like Free-Form and
  Elliptical, except rectangular.
:Text: A basic text tool. A text toolbar will appear as you use it so
  that you can choose different fonts and font weights, but there is no
  internal selection of words or letters, so don't expect a typography
  tool. There are two modes in the contextual options below the toolbox:
  text with an opaque background or with a transparent background.
:Pen: A one-pixel free form line tool. There is no adjustment of line
  thickness with this tool.
:Brush: A free form line tool with adjustable line thickness.
:Colour Picker: Allows you to pick a colour by clicking on any colour
  in the current document. For a system-wide version of this, use
  KColorChooser.
:Spraycan: Paints pixels in a scattered pattern.
:Rectangle: Draws a rectangle with the same options as the Rounded
  Rectangle tool.
:Ellipse: Draws a circle or ellipse with the same options as the
  Rounded Rectangle tool.
:Curve: Draws a single line, and then allows two clicks to control the
  curve of the line. The thickness of the lines can be selected in the
  contextual options below the toolbox. If you draw a line while holding
  down the mouse button, a straight line is drawn and then curved with
  the following clicks. If you draw a line by clicking at two separate
  points on the canvas, the line wraps around on itself to form an
  ellipse which you can skew with the following click.

Colour
------

In addition to this toolset, there is a colour pallete at the bottom
of the screen. It houses 22 colour swatches by default, which you can
swap out for other colour sets via the Colours menu. Of course, you
can always choose any colour value by double-clicking the current
colour swatch and choosing from the standard KDE colour chooser.

The most unique mechanic of the colour box is the Colour Similarity
tool.  This sets how different two similar colours must be in order to
be considered two different colours; in other words, it determines the
colour threshold for tools like the Flood Fill, Colour Eraser, and
functions like Autocrop. For example, if you have a red pixels and
green pixels next to one another and flood fill the red pixel, then of
course the green pixel would not be filled because it's very different
from red. But what if you have a red pixel next to a pink pixel?
Should a flood fill see these two shades of red as the same or
different? 

With a high colour similarity requirement (such as 30%), a flood fill
would see the two shades as "red enough", and fill them both in. With
a low colour similarity requirement, a flood fill would see them as
two distinct colours and it would only fill in the colour you clicked
on with your flood fill tool.

There are plenty of other features to KolourPaint , many of which can
be found in the More Effects option of the Image menu (or control-m
for quick access). This features colour balancers, colour invert, and
other manipulations.

Saving and Exporting
--------------------

When you save your image, use either Save or Export in the File menu.
For Pygame, it is best to save as PNG, since PNG guarantees a
reasonable file size plus an alpha channel. KolourPaint has the
ability to save in 24-bit colour but that might be overkill for most
Pygame projects. To save space, you might consider saving your work as
a 256-colour image. 

There is no special KolourPaint format, so whatever you save or export
is your master source.
