Firing Up Linux
===============

GNU Linux (usually just called "Linux" for brevity) is an operating
system, just like Windows and Mac OS X, except different. Fortunately,
what makes it different is that it lets you do whatever you want to do
with it. This is exactly what a new programmer needs, incidentally, so
it's no wonder that Linux is so popular among the world's best
programmers. In fact, most of the security software the world uses
(usually without knowing we're using it), nearly all the internet
protocols in existence, and indeed the internet itself (including the
trendy "cloud" computers), many popular video game consoles, most
movie special effects companies, and much more, are Unix or Linux
technologies.

What makes it even better is that you can write programs within a
Linux development environment that will run on Mac and Windows, too.

Why Aren't You Already Using Linux?
-----------------------------------

The reason you're not already using Linux is because a huge number of
computers are sold with Windows already installed on them, and most of
the other computers are sold with OS X on them, while there are only a
few computers sold with Linux on them. Most of us just fall into using
Windows or Mac for that reason alone.

.. note:: Computers with Linux pre-installed are available from 
	  Dell, `System76 <http://system76.com>`_, `ZaReason <http://zareason.com>`_, and many other online
	  retailers. 

Most Linux users install their operating system on whatever computer
they happen to purchase or find in a dumpster so to them it's
completely irrelevant as to whether a computer is an Apple or a
"Windows PC". But it's not often something that you stumble onto; if
you're using Linux then you probably are doing so intentionally, which
is not the case with the *de facto* OEM operating systems.

Why Use Linux?
--------------

Linux is different, but in an exciting and good way. Will there be a
learning curve? yes, but isn't there always, when you're learning
something new? The real question is, why should you use Linux instead
of just getting by with OS X or Windows? Obviously lots of people
program on Macs and Windows, and quite probably you will want your
programs to run on OS X and Windows in the end. So why start
programming on Linux? There are lots of reasons. And here,
conveniently, is a list of many of them:

.. image:: /images/desktop.png
   :scale: 50%
   :alt: An image of a Linux desktop.

* Linux traditionally values cross-platform programming. Many of the
  most famous Linux-based applications also happen to run quite well on
  Mac OS X and Microsoft Windows. Many of the most famous Linux-oriented
  applications also happen to have become famous on other platforms;
  Firefox, VLC, Audacity, Open Office, Apache, ffmpeg, and many many
  more. This is not accidental, and it segues nicely into the next
  point...
* Linux also values *smart* code. It might not come as a surprise that
  programmes that are written to work only on one or two platforms are
  limited mainly because shortcuts were taken during the coding process.
  Well-written code runs on multiple platforms, or at worst is easily
  adapted so that it can run on multiple platforms. Of course, not all
  code *needs* to run on multiple platforms because the application
  might be very specific in what it does. However, the fact that you
  learn to write code smartly is an invaluable lesson that will manifest
  itself in all aspects of your initial software design and then
  throughout the development process. In the common tongue: strive to
  build a Firefox , not an IE .
* Linux is open source. This means that you can look at and learn from
  all the code that makes your computer run. So if you see something
  that you like in one application, all you have to do to learn *how*
  it's done is to download and read the source code.
* Linux code isn't just open source, it's also free to re-use and re-
  publish. With most applications written for Linux, you inherit the
  liberty to re-use any code you want in your own programmes. This is
  huge for any programmer; if you want to see that for yourself, just
  take a look at all of the open source technology being used by major
  software companies (kerberos, SSH, KHTML, Webkit, IPtables, ffmpeg,
  and so much more). No one wants to reinvent the wheel, and Linux makes
  sure you don't have to.
* Understanding Linux means understanding the Big Picture. I don't
  want to suggest that you can't learn to programme on Mac or Windows,
  because very many good programmers do just that. However, there are a
  different ways to learn things in life; you can learn specific tricks
  so you are able to work within one environment, or you can learn the
  principles and techniques of code that are universal across all
  programming languages and frameworks so that you can use your
  knowledge in any computing environment.
* Linux is malleable and efficient. If you don't find Linux efficient,
  then you can just change the way it works until you're most
  comfortable with it. Admittedly, you might not know how to just change
  around everything at first, but the more you use it, the more you will
  start to understand how you can optimize your own work environment so
  that you can get more done, more effiently.
* Linux allows you to interact directly with the computer you are
  using. While it's true that there are layers of abstraction in the
  Linux OS, these layers are exposed to you so that you can bypass them
  when you want. So if you're determined to learn how to interact with
  your technical gear on a low level, you almost stumble into it on
  Linux because the ability to do so is just so accessible.

Using Linux
-----------

So how do you start using Linux? Books and articles have been written
on this subject, so I'm not going to repeat it all here. Go to your
nearest book store and buy a magazine like `Linux Identity <http://linuxidentity.com/us/>`_ about
getting started on Linux, or buy a book on it. They're everywhere and
most of them are quite good and many include a DVD that contains one
or two flavours of Linux. The website `straightedgelinux.com/slax
<http://straightedgelinux.com/slax/>`_ provides a thorough yet quick
introduction to the basics of Slax Linux, the distribution of Linux
that this book uses for its standardized framework.

One of the biggest stumbling blocks for people just starting out on
Linux is the fact that they don't know how to use it. Learning a new
OS can be frustrating, and it can slow you down during tasks that
should be routine and mundane for you. Lucky for you, you're embarking
on something completely new, so there'll be no such thing as "routine"
or "mundane" because it's *all* new to you. You may as well throw
learning Linux into the mix.

You can, of course, just jump right in.  And for this book, you
should! The easiest way to jump right in is to use `Slax
<http://slax.org/>`_, a distribution of the Linux OS that specializes
in running from a USB thumbdrive or CD, and for which I have created
some modular software packages so that you and this book will be on
the same page, so to speak. It's only a few hundred megabytes in size
and yet still has a complete desktop and host of useful applications
for every day tasks. If you're lazy, then you can even buy a
thumbdrive with Slax pre-installed for you.

Since it's free to obtain Linux, easy to use, and is simply the best
platform for application development, this book will assume that you
are using Linux. For standardization purposes, it will specifically
assume you are using Slax Linux.

.. note:: Strictly speaking, nearly any Linux or BSD distribution will
	  do.  However, if you choose to go your own route on this, or
	  use Mac or Windows instead, there could be a lot of
	  additional work you'll have to do to set up your dev
	  environment, which this book cannot predict and therefore
	  will not cover. That being said, if you're already a
	  seasoned Linux or BSD user and feel like you know how to
	  install applications, then feel free to use any distro of
	  your choice.

Booting into Slax
-----------------

The interesting thing about Slax Linux is that it was designed to run
off of a thumbdrive or CD. It doesn't expect to be installed on your
computer, so trying Slax is a no-risk investment.

.. note:: If you'd rather use a Linux distribution that does install
	  to your computer (thereby wiping out your existing operating
	  system and any data that you don't have backed up), then
	  feel free to do so. A good start that is roughly akin to
	  Slax would be `Zenwalk <http://zenwalk.org/>`_. Both are
	  derivatives of `Slackware <http://slackware.com/>`_, which
	  is a fairly hands-on distribution, so if you're looking for
	  a gentle introduction to everyday Linux, the "easy" Linux
	  distribution is `Ubuntu <http://ubuntu.com/>`_. This book
	  uses Slax, however, because it is so portable.

To start using Slax, download either the USB image (preferred, unless
you have a computer, like a pre-2013 Apple, that will not boot via
USB) or the CD `.iso` from `slax.org/en/download.php
<http://slax.org/en/download.php>`_. Only choose to download the 64bit
version if your computer is 64bit capable. If you are unsure, choose
the 32bit version. 

.. image:: /images/slax.png
   :scale: 50%
   :alt: An image of the Slax Linux in action.

Once you've downloaded the image, you're ready to
either burn it to disc or put it on a thumbdrive.

Slax to CD
----------

Creating a bootable CD from the Slax `iso` is easy. It may be a matter
of just a few clicks, in fact. There are some advantages to running
Slax from CD instead of a thumbdrive:

* Creating a bootable CD is generally easier than creating a bootable
  thumbdrive
* Not all computers can boot from a USB device. However, nearly all
  computers can boot from CD, even Macs

There are also disadvantages:

* Any major changes you make to Slax will be lost upon reboot, since
  CDs are read-only media once burned. Obviously, you'll need an
  external thumbdrive (as little as 2gb should be sufficient) so you can
  save your exercises from this book as well as software modules you'll
  need to do the lessons.

To create a Slax live CD:
-------------------------

#. Download the `iso` from the website.
#. Use a disc burning software to burn the image to disc. If you don't
   often burn operating systems to disc, then then have a look around
   `sourceforge.net <http://sourceforge.net>`_ and you'll find that
   there are several free disc burning utilities for both Mac and
   Windows.

   * For Windows, try `cdrtfe
     <http://cdrtfe.sourceforge.net/cdrtfe/index_en.html>`_
     .. image:: /images/cdrtfe.png
   * For Mac, try `SimplyBurns <http://simplyburns.sourceforge.net/>`_
   .. image:: /images/simplyburns.png

   It's important to burn the *image* to disc, which is distinctly
   different from dragging the `iso` file into the empty disc icon and
   burning it as a file (as you would do if you were making a backup of
   your personal data).

   .. warning:: After you've burnt the disc, mount it and look at the
	        files on it. If you see a single file called
	        `slax-7.0.8.iso` (or similar) then you have *not*
	        burned the image! Try again, and make sure that you
	        are burning an image to disc, rather than files or
	        data to disc.

#. Once the disc is finished burning, reboot your computer.

   * On a Mac, you can go to System Preferences and choose the Startup
     Disk option, setting the boot device to your Linux disc instead
     of to the computer's hard drive. Alternately on a Mac, hold down
     the Option key immediately after the computer chimes at reboot or
     power-on and choose to boot from the CD (for some reason, Macs
     usually mis-identify non-Apple boot drives, so it may be
     mislabelled as Windows; this is just a minor bug in OS X that can
     be safely ignored)
   * If you're on any other computer, you will need to press some key
     for either your BIOS, UEFI, or Boot control. Which key this is
     will vary from manufacturer to manufacturer; sometimes it's `F11`
     , or `F12` , or `F2` , or `F9` , or even `Del` . Look in your
     computer or motherboard's documentation to find out for sure, or
     just try the common keys and see what happens.


     .. warning:: If your computer shipped with Microsoft Windows 8,
	          then your motherboard may have "secure boot"
	          enabled. Disable it in the UEFI controls. Don't
	          worry; secure boot is not secure anyway, and having
	          it disabled will not interfere with either your
	          Windows install or Linux.  In the end, you'll
	          eventually find yourself in either a BIOS/UEFI setup
	          menu or a boot picker. If you get a boot picker,
	          simply select your CD drive as the boot device. If
	          you get a BIOS/UEFI menu, find the Boot Sequence
	          menu and position the CD drive in sequence *before*
	          your harddrive.

#. Once you've got a Slax splash screen, you can use the up or down
   arrow keys to set boot options. If you have 2gb of RAM, you can
   safely set Slax to copy itself into the computer's RAM such that
   instead of running off of a slow and cumbersome CD, you will be
   running straight out of RAM. It will feel, more or less, like Linux
   is installed on the computer natively (but it isn't; Slax will
   never install itself to your computer even if you want it
   to). Since Slax is running off a CD or your RAM, it will not save
   any change you make to it. Have a thumbdrive around so you can save
   data.

Slax to USB
-----------

Creating a bootable USB thumbdrive from the Slax USB image is fairly
simple and very rewarding. There are some advantages to running Slax
from a thumbdrive instead of a CD:

* Not all computers have optical drives, so a USB thumbdrive may be
  your only practical option
* USB thumbdrive read/write speeds are generally faster than the read
  time of an optical drive, so the OS will respond faster than when
  being run off of CD
* Unlike read-only CDs, all of your changes and customizations can be
  saved back to your thumbdrive so that when you reboot, all of your
  hard work and modifications to your desktop will persist

  .. warning:: While Slax on a thumbdrive does save your data, it does
	       better with personal data, like files and installed
	       applications, than with preference changes, such as
	       wallpaper changes and other customizations to the
	       environment. This is only a limitation of running a
	       live environment, so if you ever install Linux
	       permanently, you can, of course, make all the changes
	       to your environment that you want.

There are also disadvantages:

* Not all computers can boot from thumbdrive. For example, all Macs
  prior to 2012 were designed to disallow booting from USB devices
* Even on Macs that can boot to USB, creating a bootable USB
  thumbdrive is difficult since Apple doesn't enable open source
  filesystems. If you're on a Mac, you might literally find it easier to
  make a Slax boot CD so that you can then use Slax to create a Slax
  bootable thumbdrive

To create a Slax live USB thumbdrive:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#. Format your thumbdrive. On Windows, format it as a FAT volume. On
Linux, format it as an ext2 volume.

   .. note:: If you're on a Mac, your options are limited; while you
	     have the MS-DOS FAT filesystem, you don't have the
	     Windows tools to make it useful as a boot device, and the
	     only modern filesystem you have access to is HFS but it's
	     not open source so what Slax can do with it is limited.
	     If you have an optical drive, burn a Slax `iso` to disc
	     and boot it. Then create the USB thumbdrive from within
	     Linux. If you have a newer Mac with no optical drive, you
	     may not be able to create a bootable thumbdrive on that
	     machine, although it may be worth .

#. Download the USB image from the website. It is delivered as a
   zipped archive, not an `iso`.
#. Unzip the archive into your thumbdrive.  On Windows, move the zip
   archive to your thumbdrive and then use `7zip <http://7-zip.org/>`_
   to unzip. On Linux, use this command (assume that "thumbdrive" is
   the name of your thumbdrive)::
     # unzip ~/Downloads/Slax*zip -d /media/thumbdrive

#. After unzipping, you are left with a directory called `slax` and a
   text file. Follow the instructions contained in the `slax.txt` file.
   On Linux, you run the `bootinst.sh` and on Windows `bootinst.bat`. On
   Linux, open the file manager ( dolphin on Slax) and navigate to your
   thumbdrive > slax > boot Right-click `bootinst.sh` and select
   Properties from the contextual menu. In the Permissions tab, place a
   checkmark in the Is executable checkbox. 

   .. image:: /images/executable.png
      :scale: 50%
      :alt: An image demonstrating setting permissions in the GUI. If
            you are not using a GUI, chmod 777 bootinst.sh

   Once set, close the Permissions window and double-click
   `bootinst.sh` to run it.

#. Once the USB thumbdrive is ready, reboot your computer.

Rebooting can be trickier than you might expect.

If you're on a Mac that allows booting from USB, hold down the Option
key immediately after the computer chimes at reboot or power-on and
choose to boot from the thumbdrive (for some reason, Macs usually
mis-identify non- Apple bootable drives, so it may be mis-labelled as
Windows; this is just a cosmetic bug that you can safely ignore or
report to Apple if you prefer).

.. image:: /images/efi.png
   :alt: An image approximating the Mac EFI bootloader.

If you're on any other computer, you will need to press some key for
either your BIOS, UEFI, or Boot control. Which key this is will vary
from manufacturer to manufacturer; sometimes it's `F11` , or `F12` , or `F2`
, or `F9` , or even `Del` . Look in your computer or motherboard's
documentation to find out for sure, or just try the common keys and
see what happens.

.. image:: /images/bios.png
   :alt: An image approximating a BIOS or UEFI interface.

After some trial and error, you'll eventually find yourself in either
a BIOS/UEFI setup menu or a boot picker. If you get a boot picker,
simply select your USB drive as the boot device. If you get a
BIOS/UEFI menu, find the Boot Sequence menu and position the USB drive
in sequence *before* your harddrive.

If all has gone well, you'll be staring at a Linux desktop in no time.

.. note:: If you have a very new computer, or a computer that has been
	  built using especially closed-source firmware, you might
	  find that wireless networking doesn't work. If this is the
	  case, you need to install a driver, which can probably be
	  found as a module on the Slax website.  Don't get too bogged
	  down trying to fix little issues like this (unless you want
	  to); connect to a wired network for now and just use Slax as
	  a development environment as you read this book and then
	  learn more about installing drivers and exploring fancier
	  distributions later.

Homework
--------

Spend some time looking around the Linux desktop and getting to know
the environment. Try a few simple tasks, like viewing your home
directory, creating a new text file, moving the file from one folder
to another, and so on.

Don't just use the environment, though; as you teach yourself how to
perform these simple tasks, look critically at the style of computing
that the Slax desktop provides. Take note of its difference from other
desktops you've used, and its similarities.  Most importantly, look at
it from a programmer's perspective. Without knowing anything about
code yet, try to imagine some of the logic behind programming an
entire desktop with file windows, an application launch menu, clock,
files, and so on.

Think about these things *now* because, believe it or not, by the end
of this book you'll be programming some basic common desktop tools,
like a calculator and a text editor.

Remember, this was only a few hundred megabytes to download, so
there's not much to look at right now, but it's something you can
build upon and a great place to start for the rest of this book.
