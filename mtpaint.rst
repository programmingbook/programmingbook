Appendix: Mtpaint
=================

If you are making custom or customized art for your game, you might
try mtpaint , a cross-platform basic paint application that is
available as a Slax module from `slax.org/en/modules.php <http://slax.org/en/modules.php>`_.

.. note:: Pixel-painting applications produce bitmap images as opposed
	  to vector images. It's common for 2d games to use bitmap
	  graphics, although vector images are considered more
	  flexible. If you are experienced with vector art, you can
	  use Inkscape , available from `inkscape.org
	  <http://inkscape.org>`_, for your artwork, converting to
	  bitmap exclusively for the game. Save your vector sources in
	  case you ever need to re- visit your game to up-res it.

To install mtpaint , navigate to
`http://slax.org/en/modules.php?detail=mtpaint&category=graphics
<http://slax.org/en/modules.php?detail=mtpaint&category=graphics>`_
and download the module (if you're running off of a CD, be sure to
save the module to a thumbdrive for posterity). Or activate fit
directly::

  # slax activate mtpaint

If you have ever used a bitmap paint programme then mtpaint will be
mostly intuitive. For creating game assets, its basic workflow would
probably go something like this:

#. Launch mtpaint from the K-menu in the lower left corner of your
   Slax desktop.
#. Set your canvas size to the tilesize dimension you want to use in
   your game. Your character sprites do not need to be constrained to the
   tilesize dimensions unless you intend to tile them for some reason.
   Either way, assume you are designing something that is 128 pixels by
   128 pixels. Go to the Image menu and select Resize Canvas , or just
   use Page Down .
#. In the Resize/Scale window, set the dimensions of your canvas to
   128 by 128 and click OK .
#. Magnify the canvas until it at least fills your screen by using the
   Zoom drop-down menu in the top menu bar, or by using the + or -
   keyboard shortcuts. When working on small documents on a pixel-level,
   it might be useful to have a separate window displaying a 100% sized
   version of your work. For this, go to the View menu and select the
   View Window option, or just press the v key on your keyboard.
#. A grid overlay showing the bounds of each pixel in your canvas if
   you zoom in close enough.
#. Mtpaint features layers. It's usually best to keep additional
   palettes in view, if you have the screen real estate for it. Activate
   the Dock view by selecting Show Dock from the View menu.
#. Paint!

Settings
--------

Most of the tools in mtpaint are modified by the Settings Toolbar.
Show the Settings Toolbar by going to the View and selecting the Show
Settings Toolbar . The lower two thirds of the Settings Toolbar houses
controls for the luma level of the current colour, the size of the
brush, the flow of paint, and the opacity level of the paint. Along
the top of the Settings Toolbar are seven non-exclusive modes:

Modes
;;;;;

:Continuous: Normal paint mode at full opacity.
:Opacity: Subject to the opacity setting; if the opacity is set to
  full (255) then this will look exactly like normal mode.
:Tint: Never fully opaque; tints the colours beneath it in the same
  fashion that the screen function in other graphic editors.
:Tint +/-: Similar to tint, but multiplies colours instead. Similar to
  the Multiply function in other graphic applications.
:Colour Selective: Right-click on the Colour Selective Mode button to
  select the colour that will be protected from being painted.
:Blend: Right-click on the Blend Mode button to select the type of
  blending you want. Try them all!
:Disable All Masks: Disables masks currently in use. Masking is a
  somewhat advanced painting technique, so you probably won't use this
  mode much at first.

Tools
-----

The tools in mtpaint are pretty straight-forward, once you figure out
where they are. The tool palette is found at the top of the mtpaint
screen.

Tools
;;;;;

:Paint: A free form line tool with adjustable line thickness.
:Shuffle: The shuffle tool swaps random pixels in the brush area.
:Flood Fill: Fills contiguous areas of uniform pixels with a new
  colour.
:Straight Line: Draws single lines in any direction of any length. It
  will continue to draw lines until a right-click is received. Drawing a
  line with Shift key pressed constrains cursor movement along the
  vertical axis.
:Smudge: Smudges pixels by brush size.
:Clone: Clones pixels. To set the clone source, hold down the control
  key and move the white brush area (the white square) in relation to
  the red source area (the red square). Release the control key and
  start cloning.
:Make Selection: Selects every pixel you surround within its bounds.
  After selecting, you can copy, cut, paste, affect, delete, move, or
  anything else you want to do to the pixels you have selected. To de-
  select, right-click.
:Polygon Selection: Selects an area defined by as many lines as you
  draw. Draw with the mouse button down for free-hand style selection.
  Right-click to close the shape.
:Gradient: Creates gradients which can be overlaid upon images or
  selections.
:Lasso Selection: Free-hand selection.
:Text: Generates text on an image. The text is not editable after it's
  been generated.

Layers
------

The dock on the right side of the mtpaint window displays the layers
and layer options. In mtpaint , the topmost layer is not transparent
by default. For a layer to be transparent:

#. Select Edit Alpha from the Channels menu.
#. In the Create Channel window, select the Cleared and click the OK
   button.
#. A new layer is created, with an "alpha" checkerboard as its
   background. The lower layers are not visible. If you want them to be,
   place a tick in the Show all layers in main window checkbox in the
   dock on the right.

Colour Selection
----------------

On the left side of the mtpaint window are 256 preset colours. These
can be customized via the Palette menu, but new colours can be picked
dynamically by clicking on the colour swatch in the upper left corner
of the mtpaint window. This window also contains an eye-dropper tool
so that you can select any colour currently on screen.

