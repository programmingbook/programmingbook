Where to Go Next
================

You have reached the end, more or less, of this book.

In case you are wondering what to do next, just remember what the
preface told you: go find a problem that needs fixing, and use your
programming skills to solve it.

Start out small. Start with simple problems. The first problem I ever
solved with BASH scripting was the desire to sign onto a network in
one command instead of three.

Later, I learned other tricks, and was able to convert folders of
images to self-contained PDFs, or big audio files to small compressed
ones, and so on.

Finally, I took on larger problems, like parsing input and output in
order to compile and install applications, or to take 100 sets of 20
sounds into re-distributable drumkits for a popular Linux drum machine
called Hydrogen, and much more.

The point is, build up your skills at your own pace. But *build*
them. There is no faster way to fail at programming than to *not*
programme. Trust me, if you programme, you will eventually look up
from your keyboard and realise that it has happened; somehow, you have
transformed from a confused, frustrated child banging away on the
keyboard to a refined, brilliant, sometimes grumpy and sometimes
fervently passionate, programmer.

Happy hacking.
