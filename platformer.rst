Side-Scrolling Platformer
=========================

Learning to programme is not quite a linear process, but it is,
fortunately, cumulative. Everything you have learned up to this point
is directly applicable to writing an actual, playable game, and even
to other programming tasks. However, the things you've learnt will not
always render blocks of code you can just copy and paste into new
programmes without adaptation; every programme is unique, and so are
the ways you'll solve the problems and puzzles that your end goal
presents to you. 

Starting with this chapter, you will take all the
principles you have used so far and apply them in new ways to create a
proper platform game complete with collision detection, gravity, and a
scrolling screen. 

.. image:: /images/lava.png

In this particular chapter, we will cover very
specifically the process of creating "solid" obstacles such as
platforms or walls.

Imports
-------

The only thing you need to import for this example is the pygame
module::

  1. #!/usr/bin/env python
  2. import pygame

1. Declares the type of document.
2. Imports pygame modules.

Variables
---------

Some important variables can be set straight away. First, a chroma key
colour to take care of transparency, and then the dimensions of the
game window::
  03. keyer = (255,255,255)
  04. black = (0,0,0)
  05. white = (255,255,255)
  06. screenX = 960
  07. screenY = 720
  08. screenU = 680
  09. forwardX = 590
  10. backwardX = 230
  11. tilesize = 32
  12. fps = 40
  13. clock = pygame.time.Clock()
  14. main = True

3. Creates a variable called `keyer` and sets it to the RGB value for
   white. If you're using your own sprites, white might not be the best
   colour key for you, so feel free to change it as needed.
4. Creates a variable called `black` and sets its value to the RGB
   value of 000.
5. Creates a variable called `white` and sets its value to the RGB
   value of 255,255,255.
6. Creates a variable called `screenX` and sets its value to the
   number 960. This will be the width of the game window that opens when
   the game is launched. It will *not*, however, be the end of game
   world, since your game world will scroll.
7. Creates a variable called `screenY` and sets its value to the
   number 720. This will be the height of the game window that opens when
   the game is launched. Think of 0 as the ozone layer and 720 as solid
   ground.
8. Creates a variable called `screenU` and sets its value to the
   number 680. Strictly speaking, this is not necessary to have but I
   like having a variable specifically representing the pixel point at
   which death will occur. Having this set a little above the actual
   bottom edge of the window either makes your game feel more responsive
   (quicker death!) or just makes it less forgiving, depending on your
   point of view.
9. Creates a variable called `forwardX` and sets its value to the
   number 590. This means that as the hero sprite walks forward across
   the screen, it is at the 590 pixel position that the screen will start
   to scroll forward. Without this value at all, the screen would not
   scroll until the player is touching the right edge of the screen, so
   the user would never be able to see what is coming. 590, in a 960-wide
   viewport, is fair, but feel free to move it back to 500 or 450 if you
   feel even more space is justified.
10. Likewise, the `backwardX` will serve as the trigger for scrolling
    back. You could omit this value entirely if you prefer the older
    tradition of not permitting players to backtrack.
11. Creates the variable `tilesize` and sets its value to 32. This
    basically just establishes the size of each rectangular chunk of
    screen space you will be using. For this particular game the number 32
    is arbitrary but in later programmes it will be important for tiling
    graphics.
12. Creates the variable `fps` and sets its value to 40. We are using a
    faster frame rate for this game because in this game there is
    animation and gravity. At slower frame rates, the motion looks less
    smooth.
13. Creates the variable `clock` and sets it to the pygame function
   `time.Clock`, which you may remember from the previous game as the
    driving force behind keeping the game moving at a reasonable speed.
14. Creates a variable called `main` and sets it to `True`. Of course
    this will be used to base our main loop upon later in the code. You
    could wait to instantiate many of these variables, but sometimes it's
    nice to establish them early and in one place so that they're easy to
    find should you want to change them.

Platforms
---------

Python is an object-oriented programming language, so creating
platforms isn't something you will do each time a platform appears in
the game. You will instead leverage teh ability of object-oriented
programming to create a class that *represents* a platform, and then
make a one-line call to it when you are building your levels.

To keep this game simple, you will not use a graphic overlay on your
platforms. We will add this on later; for now, we'll just draw
directly onto the screen with pygame's built-in shapes. It won't be
pretty, but it will let us focus on just the physics for now::
  15. class Platform(pygame.sprite.Sprite):
  16. ¬ def createBlock(self,x,y,width,height):
  17. ¬¬ self.image = pygame.Surface([width, height])
  18. ¬¬ self.rect = self.image.get_rect()
  19. ¬¬ self.rect.y = y
  20. ¬¬ self.rect.x = x
  21. ¬ def __init__(self,x,y,width,height):
  22. ¬¬ pygame.sprite.Sprite.__init__(self)
  23. ¬¬ self.createBlock(x,y,width,height)

15. Establishes a new class called `Platform` using the pygame function
   `sprite.Sprite`. Yes, platforms are sprites too.
16. Creates a method within the `Platform` class called `createBlock`,
   which takes as arguments itself (in other words, when you create it
   later by calling upon this class, it spawns a new instance of itself),
   an `x` and `y` value representing its location within the gameworld
   and `width` and `height` values representing how wide and high the
   platform itself will be.
   
   You can think of the `x` and `y` values as where on the canvas you
   want to paint, and the `width` and `height` values as what size
   paint brush you intend to use.

17. Paints the object being created with the pygame function `Surface`
    to the width and height defined by the `width` and `height`
    variables.

    This can be a little confusing, because, as you may or may not
    have noticed, `width` and `height` don't actually *exist*
    yet. This is where the non-linearity of object-oriented
    programming can get a little upsetting, but it helps if you just
    have faith and know that at some point in your programme, you
    *will* provide `width` and `height` values.
18. Next, pygame looks at the attributes of the rectangle it has just
   drawn in order to assign the proper world co-ordinates to what it has
   just drawn. Without this small block of code, the painted rectangles
   would appear in the game but they would default to the top most border
   of the window far out of reach of the player.
19. Assigns the `y` co-ordinates of the rectangle to the `y` value
   which, even though it doesn't exist now, will exist in the future,
   when this class is invoked.
20. Assigns the `x` co-ordinates of the rectangle to the `x` value.
21. At this point in the code, the first method has established all the
    values needed to create a platform sprite. The second method in the
    `Platform` class uses this data and carries out the task of actually
    instantiating the platform in the game world.
22. Initializes an instance of the sprite described in the first
    method...
23. ...and gives it the appropriate values.

You now have the abililty to create platforms anywhere in your game
world that you like. Without a player, the game won't be terribly
interesting, but so that we stay focused on the task of creating
platforms, we will follow this logic through and then add in the
player later.

Creating the World
------------------

Proceeding boldly without a `player` class, you can create a method
within the game itself which will draw platforms where you want them.

There are many different ways to perform this task. This is the most
manual way, which is not necessarily good or bad, but it is very
direct and easy to understand. For more complex environments, you may
want to look into creating map files of your world, or try out
external libraries that use other technologies like XML to establish
advanced layouts.

For now, you will use the most direct method of simply defining where
in your game world you want platforms::

  24. def createWorld():
  25. ¬ ledge_list=pygame.sprite.Group()
  26. ¬ ledge=Platform(0,400,448,32)
  27. ¬ ledge_list.add(ledge)

24. Notice that you are *not* within the `Platform` class; this is a
   method all its own. The method, `createWorld`, has three tasks:
25. Create a `ledge_list` which will contain all the Platform sprites
   being drawn.
26. Create an instance of `Platform` called `ledge`, assigning it the
   values of 0 for `x`, 400 for `y` (I told you they would exist in the
   future; welcome to the future!), 448 for `width` and 32 for `height`.
27. Add the newly spawned ledge to the `ledge_list`.

This method can do this for as long as you can type in new platform
locations. In fact, you can try a few now::
  28. ¬ ledge=Platform(300,600,96,32)
  29. ¬ ledge_list.add(ledge)
  30. ¬ ledge=Platform(970,600, 96, 32)
  31. ¬ ledge_list.add(ledge)
  32. ¬ return ledge_list

28. Creates a new ledge at 300 `x` and 600 `y`. It will be the same 32
   pixels high but only 96 wide.
29. Adds the newly spawned ledge to the `ledge_list`.
30. Creates yet another ledge, but this time notice that it is create
   *offscreen*. You won't be able to see it within a 960x720 window
   because it's drawn at 970 pixels from the left; that's 10 pixels
   outside of view.

   Fortunately, when we get a player sprite, you will add a scrolling
   mechanic so that your viewable world *does* encompass 970 pixels,
   and as many more pixels as you please.
31. Adds the new ledge, out of view though it may be, to the
   `ledge_list`.
32. The final line of the method returns the `ledge_list`, making it
   accessible to the rest of the programme. Without a return statement at
   the end of this method, there would be no `ledge_list` as far as any
   other of the game code would know. That's not a bug; it makes
   processing data cleaner in some cases, because sometimes you don're
   really need the rest of your application to be bothered with every
   single process running in the background. In this case, however, we do
   want that data out of the method, so you use a return statement.

The world now exists, but we still aren't able to see it since we have
no main loop in our code yet, much less have we bothered invoking a
pygame window. Let's throw in some quick and dirty code, mostly copied
from the previous game, just so we can finally see these platforms
that you've created::

  33. pygame.init()
  34. screen = pygame.display.set_mode([screenX, screenY])
  35. screen.fill(white)
  36. ledge_list = createWorld()

33. Initializes pygame.
34. Creates a variable called `screen` and sets it to a pygame window
   with the values of `screenX` and `screenY`, which you defined at the
   top of your document.
35. Fills `screen` with the RGB values contained in the `white`
   variable that you set at the top of your document.
36. Invokes `ledge_list`, causing the `createWorld` method to populate
   the world.

For good measure, we will literally copy and paste blocks of code from
the previous game so that we can press q to quit the game. The only
difference here is that you must prefix KEYUP with pygame, since in
this code you did not import the pygame locals::
  while main == True:
  ¬ for event in pygame.event.get():
  ¬¬ if event.type == pygame.QUIT:
  ¬¬¬ pygame.quit(); sys.exit();
  
  ¬¬ if event.type == pygame.KEYUP:
  ¬¬¬ if event.key == K_ESCAPE or event.key == ord('q'):
  ¬¬¬¬ pygame.quit()
  ¬¬¬¬ sys.exit()
  
And finally, add in the code to refresh the screen and start the
clock::

  ¬ ledge_list.draw(screen)
  ¬ pygame.display.flip()
  ¬ clock.tick(fps)

Try running your game now, just to see what you have built so far. Go
to the Tools menu of SPE and select Run/Stop , or use the keyboard
shortcut control r . A dialogue box will appear, prompting for
arguments but since your game takes no arguments, just click the Run
button to proceed.

You should see a white background with some black platforms. That's
all so far, but now you know the process of drawing platforms and,
whether you realize it or not, making them into solid objects.

In the next chapter, we will re-visit this platformer and add an
animated sprite to the world, making use of collision detection, and
introducing gravity and scrolling.

platforms.py
------------

The source 
code::
   #!/usr/bin/env python
   # GNU All-Permissive License
   # Copying and distribution of this file, with or without modification,
   # are permitted in any medium without royalty provided the copyright
   # notice and this notice are preserved.  This file is offered as-is,
   # without any warranty.

   import pygame

   keyer = (255,255,255)
   black = (0,0,0)
   white = (255,255,255)
   screenX = 960
   screenY = 720
   screenU = 680

   forwardX = 590
   backwardX = 230
   tilesize = 32
   fps = 40
   clock = pygame.time.Clock()
   main = True

   # generate platforms
   class Platform(pygame.sprite.Sprite):
   ¬ def createBlock(self,x,y,width,height):
   ¬ ¬ self.image = pygame.Surface([width,height])
   ¬ ¬ self.rect = self.image.get_rect()
   ¬ ¬ self.rect.y = y
   ¬ ¬ self.rect.x = x
   ¬ def __init__(self,x,y,width,height):
   ¬ ¬ pygame.sprite.Sprite.__init__(self)
   ¬ ¬ self.createBlock(x,y,width,height)

   # calculate where all the platforms go
   def createWorld():
   ¬ ledge_list = pygame.sprite.Group()
   ¬ ledge = Platform(10,200,448,32)
   ¬ ledge_list.add(ledge)
   ¬ ledge = Platform(480,350,320,32)
   ¬ ledge_list.add(ledge)
   ¬ ledge = Platform(900,404,64,32)
   ¬ ledge_list.add(ledge)
   ¬ return ledge_list

   pygame.init()
   screen = pygame.display.set_mode([screenX,screenY])
   screen.fill(white)
   ledge_list = createWorld()

   while main == True:
   ¬ for event in pygame.event.get():
   ¬ ¬ if event.type == pygame.QUIT:
   ¬ ¬ ¬ pygame.quit(); sys.exit()
   ¬ ¬ ¬ main = False

   ¬ ¬ if event.type == pygame.KEYUP:
   ¬ ¬ ¬ if event.key == ord('q'):
   ¬ ¬ ¬ pygame.quit()
   ¬ ¬ ¬ sys.exit()
   ¬ ¬ ¬ main = False

   ¬ ledge_list.draw(screen)
   ¬ pygame.display.flip()
   ¬ print("here")
   ¬ clock.tick(fps)
