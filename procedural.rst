Procedural Programming
======================

So far all the programming that you have done has been shell
scripting. That is, your programmes have consisted of a sequential
list of actions that you want the computer to execute, one after
another. There has been nothing asynchronous (ie, out of order) and
the programmes run once and then close.

That model works well for terminal-based applications but for
graphical applications it's more common that a computer opens a window
onscreen and sits and waits for the user to interact with it. The
window does *not* simply open, run a job, and then close. And there are
often many different ways that the application can be used; it's
entirely unpredictable as to whether a user will click one button or
another at any given time; all actions within the application are
asynchronous.

Such a diverse operating environment simply cannot be created from a
purely sequential script, and so the idea of procedural, or
functional, programming was born.

Procedural programming refers to the practise of writing chunks of
code that can be re-used later within the code itself. In this way,
you can group tasks together in "functions", and then re-use those
functions throughout your programme as needed.

.. note:: If that notion seems incomplete to you, it might help to
	  think of how a car, for instance, works. You turn a car on
	  and it doesn't really do anything aside from sit there and
	  idle. But you could imagine that the car was "listening" for
	  prompts from the driver. In other words, if the driver
	  presses the gas pedal then the car calls upon the `drive`
	  object (a complex system consisting of the engine, wheels,
	  gears, and so on), and it moves. The driver can drive either
          in 1st gear, or 2nd gear, or even reverse; so the `drive`
          function is flexible and can be defined more or less
          on-the-fly.

          Similarly, if the driver turns on the headlights, then the
	  car calls the `headlight` object (a system consisting of
	  electrical wiring, lightbulbs, and so on) and the headlights
	  light up. Again, there is flexibility in whether the
	  headlights are high-beams, normal, and so on. 

          If the driver activates the windshield wipers, the car calls
	  upon the `wiper` object, and rain is swept away from the
	  windshield. And so on.

For a simple introduction to procedural programming, you can revise
the dice game you created earlier in the book. First, let's review the
original programme::
  #!/bin/bash
      
      PLAYER=$(date +%N | cut -b1)
      COMPUTER=$(date +%N | cut -b1)
    
      if [ $PLAYER == $COMPUTER ]
          then echo "You won"
          else echo "You lost"
      fi

It's a simple application that runs once upon execution and then
exits. A user could play the game over and over and over again but
perhaps it would be nicer to ask the player if he or she would like to
roll the dice again, until a winning roll has been reached.

Functional Dice
---------------

Your dice rolling programme can be a bit more flexible by creating one
simple function that represents the action of rolling dice. To create
an a function in BASH, you simple declare one. Let's make one called
"dice"::

  #!/bin/bash
    dice() {
    
    PLAYER=$(date +%N | cut -b1)
    COMPUTER=$(date +%N | cut -b3)
    echo $PLAYER
    echo $COMPUTER
    
    if [ $PLAYER == $COMPUTER ]
        then echo "You won"
        else echo "You lost"
    fi
    
    }

As you can see, a `dice()` function now exists. It is simply the entire
programme that was `dice.sh` previously pasted in between the braces
of the dice() function. It's as simple as that, but in itself it
affords no advantage. In fact, if you run the programme as it is,
nothing will happen because the dice() routine is never used. BASH
skips over the dice() function because it's a function that is never
called, and then BASH finds no further code to execute, and the
programme ends.

But watch what happens if you add an if statement asking whether the
user would like to play the game::

  #!/bin/bash
    dice() {
    
    PLAYER=$(date +%N | cut -b1)
    COMPUTER=$(date +%N | cut -b3)
    
    if [ $PLAYER == $COMPUTER ]
        then echo "You won"
        else echo "You lost"
    fi

    }

    dice

    echo "Roll dice?"
    read INPUT

    if [ X$INPUT == X"y" ]
        then dice
        else exit
    fi

Run the programme now and you'll see that `dice()` is executed once, and
then the user is offered another try. If yes, then dice() is executed
again. This repeats until the player decides not to roll the die.

.. note:: Notice that when you call upon the dice() function, you
	  simply issue the command `dice` as if it were a normal,
	  everyday BASH command like `date` or `cut`.

Nice as this is, it doesn't really provide anything we couldn't have
implemented simply by copy and pasting the same code in twice into the
source code twice in a row. From the user's perspective, it would
still be a game that runs once, offers a re-try, and then exits.

But what if you wanted the user to be able to play until a winning
roll? This would require, essentially, an infinite loop contigent upon
the player's desire to continue playing and whether or not the player
has won or lost the round.

Dice Loop
---------

To make the dice programme infinitely loopable, all you have to do is
move all of the code into a function::
    
    #!/bin/bash
    dice() {
    
    PLAYER=$(date +%N | cut -b1)
    COMPUTER=$(date +%N | cut -b3)
    
    echo $PLAYER
    echo $COMPUTER
    
    if [ $PLAYER == $COMPUTER ]
        then echo "You won" && exit
        else echo "You lost"
    fi
    
    # dice
    
    echo "Roll dice?"
    read INPUT
    
    if [ X$INPUT == X"y" ]
        then dice
        else exit
    fi
    
    }

If you run the above code, however, you would find again that nothing
happens because now all the executable code has been placed into a
function that, unless specifically called, is skipped over by BASH.
Therefore, one external line is necessary: at the very bottom of your
code, execute the function with the single command `dice`::

    #!/bin/bash
    dice() {
    
    PLAYER=$(date +%N | cut -b1)
    COMPUTER=$(date +%N | cut -b3)
    
    echo "Player rolls " $PLAYER
    echo "Computer rolls " $COMPUTER
    
    if [ $PLAYER == $COMPUTER ]
        then echo "You won" && exit
        else echo "You lost"
    fi
    
    # dice
    
    echo "Roll dice?"
    read INPUT
    
    if [ X$INPUT == X"y" ]
        then dice
        else exit
    fi
    
    }
    
    dice

The `dice` command at the bottom of the source code invokes the
function dice() and once the computer is inside of that function, the
flow of logic simply forces the computer to continually execute dice()
unless the player wins (at which point the computer is told explicitly
to `exit`) or the player responds with anything but "y".

Try it out now.

Normalizing Input
-----------------

Depending on how prone you are to breaking things, you may have
noticed while playing your dice game that if you respond with anything
other than a lower-case "y" then the computer interprets your response
as a signal to exit. This is because to a computer, responses such as
"yes" or "yeah" or even an upper-case "Y" simply are not equal to "y".

Since any user in their right mind would expect a programme to
understand that "Y" or "yes" is basically the same to answering "y",
you should make your application flexible enough to understand a
variety of similar and logical responses.

This process is called "data
normalization" and it's done nearly any time that a user provides
input into forms or prompts. In this case, it's probably easiest to
accept any possible response beginning with a "y" as positive and
anything else as a negative.

You already know how to look at just the first character in a
string. And you have already used the `tr` command to convert
characters from upper to lower case. Therefore, normalizing the user's
response down to a lowercase "y" or any other character is simple and
only requires one additional variable::

  #!/bin/bash
    dice() {
    
    PLAYER=$(date +%N | cut -b1)
    COMPUTER=$(date +%N | cut -b3)
    
    echo $PLAYER
    echo $COMPUTER
    
    if [ $PLAYER == $COMPUTER ]
        then echo "You won" && exit
        else echo "You lost"
    fi
    
    # dice
    
    echo "Roll dice? [y/n]"
    read INPUT
    
    # normalize
    ANSWER=$(echo $INPUT | cut -b1 | tr [:upper:] [:lower:] )
    
    # compare the normalized data instead of INPUT variable
    if [ X$ANSWER == X"y" ]
        then dice
        else exit
    fi
    
    }
    
    # main
    dice

Notice that some guidance in the prompt also helps; in this version of
the application, the user is encouraged to enter either y or n,
reducing the chance of errant responses such as 1/0 or t/f or silly
things like "uh huh" and so on. And yet our data normalization allows
for a wide variety of responses and will catch anything beginning with
a "y" or "Y" as a positive response.

Main Loop
---------

The structure of your latest dice iteration is very similar to the
classic object oriented programme model. The code that the computer
sees and executes straight away is the "main" part of the code, which
is funny since it often is quite short (because all the really complex code
is contained in functions and classes functions).

You will use the concept of a main loop all the time in other
programming languages, Python included, so get used to the idea
now. The main section of your code will be relatively short and
possibly even cryptic since it will be executing far more complex and
verbose code that is stored outside of the main section. But it's the
main section of the code that throws the computer into an endless
loop, which in turn provides your user with an application that sits
patiently and awaits a command.

dicey.sh
--------

And the complete source
code::
   #!/bin/bash
   # programmatic implementation of dice.sh

   # GNU All-Permissive License
   # Copying and distribution of this file, with or without modification,
   # are permitted in any medium without royalty provided the copyright
   # notice and this notice are preserved.  This file is offered as-is,
   # without any warranty.

   dice() {

   PLAYER=$(date +%N | cut -b1)
   COMPUTER=$(date +%N | cut -b3)
   echo $PLAYER
   echo $COMPUTER

   if [ $PLAYER == $COMPUTER ]
   then echo "You won"
   else echo "You lost"
   fi

   echo "Roll dice?"
   read INPUT
   ANSWER=$(echo $INPUT | cut -b1 | tr [:upper:] [:lower:] )

   if [ X$ANSWER == X"y" ]
   then dice
   else exit
   fi

   }

   dice

   # EOF
