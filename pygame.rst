PyGame
======

In the world of programming, there are often existing structures that
you will want to tap into when creating new code. That is, unless you
are writing everything from scratch, you are probably going to be
creating software that works within some other existing software, such
as a desktop environment, or with an online service, or on a mobile
device, and so on. Learning to work with "frameworks" or an API or an
SDK is important. 

Everyone loves games, so let's spend some time creating a game with
Python and a Python framework. Not only will you learn Python, but
you'll learn how to hook into external libraries as well.

.. figure:: /images/ardentryst.png
   :scale: 100 %

   *Pictured: Ardentryst, a fine side-scrolling RPG made in
   Pygame. We will not be making a game quite this complex, but it is
   a great example of what is possible with Python and Pygame.*

You may have heard of game engines like the famously open- sourced
Quake� and Doom� engines from Id Software, or the Unity� engine, or
the Unreal� series, and so on. You may have wondered what exactly an
engine is, especially since in the gaming world there is so much talk
about the different engines available.

Well, first of all you should know that when most normal-people talk
about game engines, it's usually just buzzwordspeak. Few people who
get excited about a game engine actually has any idea what a game
engine does on a programmatic level. To a programmer, there is more to
game programming than just engines; there are engines, frameworks, and
libraries, and all of these are basically collections of code that
alleviate the need for the developer (that's you) from constantly
re-inventing the wheel.  Can you imagine having to code from scratch
every little animation that happens in a game? it would basically be
impossible if you are writing a modern game.

There is sometimes some overlap, but generally speaking a game engine
makes it simple for game programmers to do everything from character
design to animation to game logic, game physics, and game
mechanics. An engine has lots of pre-defined objects and methods that
game developers can just drop into their code for quick and easy
results. A framework is not as complete, but it does help with the
animation, mechanics, and common game logic. Libraries are far more
modular and usually only try to cover one thing; you might find a
physics library, or a map library, or an effects library, or one that
helps parse data, and another to help write game saves, and so on.

We won't be using a proper game engine, although Python can
utilize Blender , Panda3d , and even your own custom game engines.
Starting out with a game framework is good enough for now, and the one
you will use is called Pygame . Using Pygame will help you understand
the important principle of tapping into additional tools when writing
code. The additional libraries you will use include the SDL graphic
layer and the openAL sound system.

.. note:: Using existing frameworks and libraries is a level of
	  abstraction; you are not writing the code that literally
	  draws pixels on the screen to create your avatars or
	  designing the system calls that will decide how your game
	  controls are mapped to the keyboard and mouse. As a result,
	  you might sometimes feel like you don't fully understand
	  some of the code you are using. But that's actually a good
	  thing, and very normal.  As a programmer, you'll make all
	  kinds of calls to libraries that you aren't familiar
	  with. It's just a side effect of there being so much code
	  out there and so many devices to control. Trust me, you
	  don't really want to understand it all, you just want to
	  code. As long as it's not getting in your way, blindly use
	  the libraries available to you and let them work for you.

SPE
---

In the real word of professional programming, one of the tools of the
trade is an IDE: an Integrated Development Environment. Popular ones
include `Eclipse`, mostly used for Java development although it has a
rich plugin structure so that it can be used for many languages,
`Netbeans` for Java, `Qt-Creator` for C++ and PyQt, and in the non-open
source world there is Visual Studio by Microsoft and XCode by Apple.
They are all basically similar in concept; they provide a consistent
environment in which a developer can create programming projects, type
and test code, and build and debug their finished products. While all
of these things can be done perfectly well without an IDE, given their
popularity, it is important to be familiar with how they work. 

`Stani's Python Editor`, or `SPE`, is an IDE specifically designed for
Python, so if you continue to use Python, you'll have SPE in your
toolkit. Even more specifically, it is designed for Python and
Blender, so if you decide to explore game engines then SPE can help.

Install SPE in Slax using the `slax` command as usual. On other Linux
distributions, you will find SPE (sometimes called by its long name of
Stani's Python Environment ) in your software installer. 

.. figure:: /images/stani.jpg
   :scale: 50 %

   *Pictured: The Stani Python Editor follows most common IDE conventions but
   remains intuitive for the new user.*

To start using SPE, create a `src` or `code` directory in your home
directory, and then create a project folder (such as `pygameIntro` or
similar) within that. This keeps all of your coding projects in one
place. Although SPE does not, some IDE's enforce this practise, and
either way it's a good organizational habit to get into. 

Start `SPE` from the K-Menu in the lower left corner of the screen.

The SPE UI
----------

There are three parts to the SPE user interface.

* In the top right quadrant is a tet editor, where you can type in and
  edit your code.

* Along the bottom half of the window is a Python shell (IDLE) so that
  you can test your Python commands and programming logic before you
  committing them to your actual code that you are entering into the
  text editor and saving to files.

* In the top left quadrant is a file system browser, code explorer, a
  place for taking notes, and more. Obviously, it's a very flexible
  panel, so you can use it for many different things while coding.

Feeding Frenzy Game
-------------------

For your first graphical game, you will create a simple game in which
the player collects objects scattered across the playing field. When
the player's avatar moves over an object, the object disappears from
screen and the player gains a point.

Of course your first step should be to determine the underlying logic
of such a game. Up to this point, your programmes have been linear,
while this game must be asynchronous. It's not just a script for the
user to step through, but a collection of events that could happen at
any time as the programme runs. This requires a slightly different
thought process, some of which will not make much sense to you until
you see it implemented many times. The only way to see the sometimes
backward-seeming logic of programming in action is to programme *a
lot*. For now, accept anything you do not feel is intuitive as
something you will learn the more you programme, because in fact
that's exactly what will happen.

#. First, think of the objects on screen that your user will see.
   Certainly your game needs a player avatar and, according to the
   description of the game, also some objects that the player must
   collect in order to win the game. These are usually called "sprites"
   in the jargon of game developers, and often "avatars" or "characters"
   in the jargon of the people playing the games. Pygame has the ability
   to handle most of the basic sprite functions for you, so creating
   sprites will be fairly trivial.

#. Obviously, you will also need a world in which all of the action
   will take place. Pygame provides a window in which the game will
   occur, so that will do for now.

#. You will need some mechanism for detecting when the player moves
   over an object, and a way to make the object disappear as if though
   the player has picked it up. Pygame has many ways of dealing with
   these kinds of sprite interactions, and you'll learn one of those
   methods in this exercise.

#. Finally, you will need some mechanism of keeping score. Standard
   Python variables can be used for this.

Now that you have some idea of what you need to implement, let's go
about implementing it.

The First Line
~~~~~~~~~~~~~~

The first line of code you should write in the text entry field of SPE
is a declaration of what kind of document your source code is. This
tells your computer how to execute the code within the saved
document::
  #!/usr/bin/env python

That's not Python code, of course, that's just a path to the programme
on a posix system that will enable the computer to execute the source
code appropriately. In this case, you reference the application env
(in the `/usr/bin` folder) which in turn looks for the default version
of python installed on the system.

.. tip:: You may never care about what version of Python is used when
	 running things on your own computer, but to some users it
	 really does matter, so use env in your declarations.

This is considered best practise when defining what application should
interpret the source code, since some people might have a preference
as to which version or install of Python they want your application to
use. Env is the polite way of asking your user's computer what it was
configured to use, rather than insisting on a specific executable
application located in some specific path.

Importing Modules
~~~~~~~~~~~~~~~~~

The first set of commands you will write are the import statements.
These define the Python modules your code needs in order to run. 

In real life, you would probably not know each and every one of the
modules you'll need. You'll have some idea, since you'll have worked
out some kind of logic before you start writing the code, but it's
very common to return frequently to the top of the document in order
to import some new module as you realize what you need.

.. warning:: Do not type in line numbers when programming.

The first is obvious; pygame is an essential module for a programme
using...pygame.  Another module you'll need is called sys, which
allows Python to access files on the computer it's running on::
  1. import pygame
  2. from pygame.locals import *
  3. import sys

1. The first import line brings pygame into your programme.

2. The second line brings certain commonly used functions and modules
   into your programme's namespace, meaning that you won't have to
   constantly preface pygame-only modules with "pygame.". It's an
   optional convenience.

3. The third line imports the sys module so that Python can access
   files and folders on the computer it's running on.

You will probably need another module or two, but you can always go
back and add it later, so we'll leave the import section for now::
  04. pygame.init()

4. Pygame needs to be initialized in order to use it. It makes sense
   to initialize it up front (although strictly speaking you don't
   actually have to intialize it until you actually start using Pygame
   modules in the code). Initializing it right away is as good a choice
   as any, so let's get it over with.

It's time to set up some initial variables. In so doing, you will also
invoke your first Pygame function::
  5. clock = pygame.time.Clock()
  6. fps = 15

5. This should look somewhat familiar to you; this line creates a
   standard Python variable called `clock` and sets its value to the
   pygame function `time.Clock`.

6. As you probably know, computers tend to process data very fast.
   Generally, they process data as fast as possible. A game that lasts
   three milliseconds is no fun, so you create another variable `fps` and
   set its value to 15 or thereabouts. 15 will render reasonably smooth
   motion for simple games (which this definitely will be), while higher
   frame rates can provide even smoother motion but cost more in terms of
   graphics card performance.

More variables are needed, but this time to help create a world for
the game. This game is simple, so you will not need much::
  7. black = (26,   26,  26)
  8. blue  = (57,  103, 189)
  9. key = (0,     0, 255)
  10. score = 0

7. Defining colours in Pygame is done with RGB values. The first
   number in the array represents red, the second green, and the third
   blue. A colour's range may be anywhere from 0 (darkest shade
   possible) to 255 (lightest shade possible). You can muddle through
   RGB values through experimentation or you can use a colour
   chooser. I use Kcolor Chooser but you could just as easily use the
   one found in GIMP or Inkscape , or install and use the Firefox
   add-on `colorzilla
   <https://addons.mozilla.org/en-US/firefox/addon/colorzilla/>`_.
   Technically speaking, a value of 000 would be black, but in the
   code I have set black to a dark gray, just for variety.


8. This line sets a value for blue. It's important to keep in mind
   that the variable names and the actual colours contained within them
   are completely unrelated. You could just as easily call this variable
   `sky` or `yellow`; as long as the RGB values are blue, then it is the
   colour blue that pygame will draw.

10. This final variable is obviously not a colour, but a value for the
    game score. It is set to zero so that each game will always start
    the player out with zero points.


Creating Sprites
~~~~~~~~~~~~~~~~

Think about your favourite video game. Usually, there are hoardes of
bad guys, but only about five different kinds of bad guys. For
instance, in Super Mario Brothers, you might have a dozen of bad guys
on screen at any moment and yet they are all the exact same mushroom
graphic. Even in modern 3d games there are identifiable models used in
large groups of baddies. These are all sprites, but more importantly,
they are classes.

A class is a programming construct that is a lynchpin of the somewhat
abstract idea of "object-oriented programming". When a programming
language is said to be object-oriented, it means that you can create
classes to represent tasks that your application will perform, and
then call upon (and usually modify on-the-fly) these classes as if
though they were objects in your toolbox so that those functions can
be performed over and over again, asynchronously to whatever else is
happening at any given moment. It's a rich style of programming and
one that is vitally important to how modern applications work.

In order to create sprites for your game, you must create a class that
will encompass the process of drawing an avatar on screen. You will
write this code only once, and yet you'll be able to call upon it
multiple times later in the programme when you want to create items
for your player to gather.

This is also the first time you will use the TAB key in Python. Python
is strict about tabs. Most languages ignore tabs, athough programmers
tend to use them to format code so that it's easier to read. However,
Python actually sees tabs as part of the source code, using them to
define parents and child statements. 

If you type in Python code that is perfectly logical and syntactically
correct but forget a tab, then it will not run. SPE , and most any IDE
designed for Python or with a Python module, knows that tabs are a
vital part of the Python language, so they will assist in making sure
you are tabbing properly. It's another great reason to use an
IDE.

Take note of the tabs (¬) in the following code block::
  11. class avatar(pygame.sprite.Sprite):
  12. ¬  def __init__(self, filename):
  13. ¬ ¬  pygame.sprite.Sprite.__init__(self) 
  14. ¬ ¬  self.image = pygame.image.load(filename).convert()
  15. ¬ ¬  self.image.set_colorkey(key)
  16. ¬ ¬  self.rect = self.image.get_rect()

11. The first line of the class code block establishes what we are
   doing: creating a class. We call the class "avatar" because that's a
   term that makes sense. In the parentheses is the pygame module we are
   invoking. In this case, we are using the `sprite.Sprite` function of
   pygame, which was designed to provide sprites in a game.

12. The second line of this block defines what attributes the class can
   take. This will enable you to re-use the class itself as many times as
   you want, and yet still provide it some variation. This makes classes
   very flexible. It might help to imagine that you are creating an
   electronic gadget when you make a class, and that you are adding plugs
   and dials and switches in the def statement.

   The def statement is a little cryptic, but it does all have a
   meaning. First of all, notice that it is given one tabbed
   indentation, meaning that it is a part of the avatar class.

   In the statement, you are defining what must happen for an instance
   of this class to be initialized (__init__). In parentheses, self
   and filename are specified, meaning that in order for your
   programme to successfully use your new avatar class, it must be
   provided with itself and a filename of *something*. In this case
   that *something* should be an image, since it is a graphical avatar
   that you are creating.

   Having access to itself is pretty much a given, but should you try
   to use the avatar class in your programme without providing Python
   a filename for it to use as the graphic for the character it is
   creating, then it will fail.

13. The next line looks more cryptic than it really is. In this line,
    you call upon the pygame function `pygame.sprite.Sprite` (the
    pygame function that creates sprites) to initialise (__init__) an
    instance of some sprite. Which sprite, you don't know yet, and
    cannot know yet, because you want the flexibility to define which
    avatar you create each time you use the class. So it's necessarily
    vague and self- referential; all we know at this point is that
    when we call upon this class, we want to create a sprite that will
    become an individual being within the game.

    .. tip:: Note that this line got indented twice, because it is
	     part of the def function of your avatar class.

14. The next line defines the builtin pygame variable `self.image` and
    sets its value to the pygame function `image.load`, with the
    promise of a filename of an image in parentheses, which in turn is
    embedded in parentheses including the pygame function
    `convert`. Convert tells pygame to create a copy of the file in
    memory, so that it can load the images quicke than constantly
    reading it off of your drive.

    .. tip:: Of course you wouldn't probably think to do all that
	     yourself unless you were very familiar with pygame. But
	     many tricks like this are standard code snippets that
	     you'll find in Pygame documentation, other people's
	     games, online examples, and so on. Use these tricks;
	     that's why we create free software!

15. The next line sets the chroma key of your image so that some
    colour is translated to invisibility. In other words, it performs
    a Green Screen effect so that whatever image you give pygame, you
    have the ability to key out a colour. If you didn't key out a
    colour, all of your sprites would be rectangles, which would
    probably look a little too old school even for the most nostalgic
    of us.

    .. note:: Notice that the key colour is set to your variable
	      `key`. You should adjust the RGB value of your `key`
	      variable to match whatever graphic you intend to use for
	      your sprites, once you find graphics to use. But more on
	      that later.

16. The final line of this block carves out a bounding box so that
    pygame knows where the avatar's edges are. This is important for
    game mechanics like collisions, and of course pygame takes care of
    most of it for you. The builtin variable `self.rect` gets set to
    the value of the builtin function `self.image.get_rect`, such that
    any avatar's self.rect is equal to the results of pygame analyzing
    the natural bounds of its graphic.

Creating the World
~~~~~~~~~~~~~~~~~~

Since this is your first game written in Python and pygame, your game
world will be very basic. In fact, your world will mostly be an empty
computer window::
  17. window_width = 960
  18. window_height = 720
  19. window_area = pygame.display.set_mode([window_width,window_height])
  20. pygame.display.set_caption('Feeding Frenzy')

17. The first line establishes a new variable that you will use as the
    width of the window you are going to have pygame create.

18. The second line establishes a new variable that you will use as
    the height of the window you are going to have pygame create. As
    with the colour variables in lines 7 and 8, the name of the
    variable has no affect on how you use it, but naming it something
    sensible helps your code be understood by others, and also
    yourself in a few weeks or months when you come back to look at
    it.

    .. note:: Why use variables at all for straight-forward numbers
	      like the window width and height? Well, if you give
	      yourself one place where important numbers are
	      established, then when you re-programme your game for
	      HD, all you have to do is change two variables rather
	      than go through all of your code and changing every
	      instance of 960 to 1920 and every instance of 720
	      to 1080.

19. The third line of this code block creates a variable called
   `window_area` and sets its value to the pygame function
   `display.set_mode`, which accepts the width and height as
   arguments.  There are other varieties of the `display` function,
   such as

    * `pygame.FULLSCREEN` creates a fullscreen display
    * `pygame.HWSURFACE` creates a hardware-accelerated fullscreen window
    * `pygame.OPENGL` creates an OpenGL renderable display
    * `pygame.RESIZABLE` creates a window that will be re-sizeable
    * `pygame.NOFRAME` creates a window with no border or window controls

20. The final line of the code block uses the Pygame `display`
    function again, this time invoking `set_caption` to create the
    window title. I am calling my game Feeding Frenzy, but you can
    call yours whatever you like.

Spawning
~~~~~~~~

So far you've set some variables, you've created a class that gives
your programme the ability to generate sprites, and you've created a
world in which the gameplay will occur. There's not much more you can
do without creating those sprites and putting them into their world.

The easiest to generate is the player's avatar. If this was a fancy
video game with lots of code in it, you might give the player a chance
to choose their character and maybe even customize the sprite in some
way. 

This, however, is not a fancy video game. 

Even so, you do need some graphic to use as your player sprite, so use
`KolourPaint` or `mtpaint` or any variety of graphical applications to
create a sprite for yourself. Or, if you just want a quick and easy
fix for this example game, go online to `openclipart.org
<http://openclipart.org>`_ and download a Creative Commons (cc0)
image. Download it in the PNG format. 

You may also use a sprite bundled with this book's sample code. 

While you're getting a hero sprite, grab a sprite for your collectible
items as well. I'm using food as the collectible item, but you could
just as well use bags of gold or gems or anything you like. You will
probably need to open the graphic up in some graphic application and
give it a solid background of the same exact shade (use the RGB values
to ensure you are using the *exact* same shade) as your colorkey
variable. 

If you don't know how to perform this function, see the appendix.

.. note:: For now, save the image you will use for your sprite in the
	  same directory that your source code is located. If you were
	  programming in real life, with an eye for distribution, you
	  would want to carefully plan where and how your game files
	  are stored. For instance, keep your images in an `images`
	  folder, sounds in a `sounds` folder, and so on.  

	  For now, however, throw that advice to the wind for the sake
	  of simplicity. 

Once you have your hero image downloaded, type in the code that will
draw your hero onscreen::

  21. all_sprites_list = pygame.sprite.Group()
  22. food_list = pygame.sprite.Group()
  23. '''player sprite below'''
  24. player = avatar("cat1.png")
  25. all_sprites_list.add(player)

21. The first line of this code block creates yet another variable (I
    told you variables were important coding concepts; aren't you glad
    you practised with them?) called `all_sprites_list` and sets its
    value to the pygame function `sprite.Group`. You can read all
    about sprite groups at `http://pygame.org/docs/ref/sprite.html
    <http://pygame.org/docs/ref/sprite.html>`_

    A sprite group is, as the name suggests, a method of grouping
    sprites together so that you can perform some function upon many
    sprites at once. Your `all_sprite_list` will represent all
    sprites that spawn in your game. This will be important eventually,
    because you will tell Pygame to refresh all of the sprites on
    screen, which is why the frame rate (from way back on line 06,
    remember?) and a group containing all sprites are important.

22. The second line creates the variable `food_list`. This group will
    contain all the collectable items in your game but *not* the hero.
    This will be important eventually because as the player collects food
    during the game, you will need to subtract those sprites from the
    world; in order to do that, you'll need a list of what sprites existed
    in the first place. `food_list` will be that list.

23. The next line is a comment. Python ignores comments, and they are
    exclusively for your own reference. Use comments often, so you will
    remember what different parts of code does what.

24. Next, you create a new variable called `player` which invokes your
    `avatar` class along with the filename `cat1.png`; your player avatar.

    .. warning:: Make sure your hero avatar is saved in the same
   directory as your source code, or else your programme won't know
   where to find the image.

   The filename, of course, gets passed into the `avatar` class and is
   processed by the __init__ function, resulting in a graphical hero
   avatar for your player.

25. The last line of the code block immediately adds the newly
    generated sprite to the `all_sprites_list` group, using the `add`
    method of the `sprite.group` function.

    .. note:: If you excluded this line, the game would still work,
	      except that your hero avatar would be invisible for all
	      but 1/15th of a second or so.

	      Adding the hero to this group and then ensuring later in
	      the programme that this group of sprites is constantly
	      re-drawn onscreen makes your avatars persistent
	      throughout the game.

Spawning Food
~~~~~~~~~~~~~

Spawning instances of the collectible items in the game is similar to
spawning your hero, but with one important difference: there are many
collectible items and only one hero. By now you probably will not be
surprised to hear that a repeated action in your programme will be
driven by a for-loop. 

It might be at this stage in your programming that you would realise
that you ought to spawn your collectible items randomly, or else every
time someone plays your game all the items will always appear in the
exact same place. That's not very fun.

You could create some variables that invoke Python's builtin `time`
modules, using the number of seconds as the co-ordinates of your
collectible items. Or you could sample the seconds and perform some
math against that number with the minute value, and invent some new
co-ordinates. You might be able to think of other ways to generate
consistantly different values, but there is an easier way.

You can use the Python module called `random`, but if you want to use
`random`, you need to import it. So before proceeding, move back up to
your imports and add::
  import random
  
Now you have access to a pseudo-random number generator, which you can
use to invent unique values each time the programme is run::
  26. for i in range(10):
  27. ¬ collectible = avatar("fish.png")
  28. ¬ collectible.rect.x = random.randrange(window_width)
  29. ¬ collectible.rect.y = random.randrange(window_height)
  30. ¬ '''list management'''
  31. ¬ food_list.add(collectible)
  32. ¬ all_sprites_list.add(collectible)

26. The first line of this code block starts the for-loop with fairly
    typical for-loop syntax. It uses the Python function `range` to
    define how many times the for-loop will be repeated.

27. The second line of the code block creates a variable `collectible`
    and invokes your `avatar` class to create a graphical sprite. It
    uses the filename of your collectible sprite as its source. Notice
    that the rest of the code is indented, because it is all contained
    within the for-loop.

28. The next line sets the location of the collectible sprite along
    the horizontal [x] axis. It uses the `random` module to generate a
    random number within the range of `window_width`.

29. The next line sets the location of the collectible sprite along
    the vertical [y] axis. It uses the `random` module to generate a
    random number within the range of `window_height`.

30. The next line is a comment.

31. The next to last line of the block adds the sprite that has just
    been generated into the `food_list` group. This will track what
    collectible sprite has been collected and what still remains to be
    gathered up by the player.

32. The final line of the block adds the sprite to the
    `all_sprites_list` so that it is a member of the group that gets
    refreshed 15 times a second (or whatever you set the frame rate
    to).

The Loop
~~~~~~~~

Most graphical programmes have a main loop that kees them going for as
long as the user wants the application to remain open. It makes sense,
if you think about it; if your Python game simply processed each
instruction in order and then quit when it reached the end of your
code, when would the player ever get the chance to actually play the
game? The same goes for most graphical applications. After all, the
user expects the office application or media player, or whatever, to
remain open so that they can use it for as long as they like.

At this point in your code, you have set up important variables and a
class, and you have spawned all the sprites you will need. All of
those things should only be done once per game, so they are the
initialization sequence of your programme. Now that your game is
initialized, you start the loop. 

A common way to implement a main loop is to create a variable, often
called `main` in other programming languages, and set its value to
True. Then tell your programme to run for as long as the variable
continues to be true; your game will continue to run forever. Later in
your code, you can programme in a signal that will change the value of
`main` to False, which will end the game::
  33. main = True
  34. while main==True:

33. The first line of this block establishes the variable `main` and
    sets it to `True`. Note that it could be set to anything, since
    the next command is to perform a loop for as long as that variable
    remains unchanged. However, it's traditional to use the boolean
    `True` and `False` values, or `1` and `0`.

34. The second line starts the loop with a `while-statement`. While
    `main` is equal (note the double equal signs, which are Python's
    way of doing a comparison of values, whereas a single equal sign
    would simply set a variable to a value) to `True`, the actions
    within the loop will be performed over and over again.

This has created an infinite loop, although as yet the loop contains
no code. This is as good a time as any to create a condition that will
detect a QUIT signal, and close the game.

The most pure code-y way of doing this would be to detect a QUIT
signal and then set `main` to `False`. However, pygame has builtin quit
functions that are designed to gracefully exit your game and that will
send appropriate quit signals to whatever operating system pygame is
running upon, so it's recommended to use these::
  35. ¬ for event in pygame.event.get():
  36. ¬¬ if event.type == pygame.QUIT:
  37. ¬¬¬ pygame.quit(); sys.exit();

35. The first line, indented once because it is within the `main`
    while-loop, sets up a for-loop. The for-loop taps into pygame's
    `event.get` function. This is one of the most powerful aspects of
    pygame, on a pragmatic level; the `event` module sits quietly and
   monitors for keyboard, mouse, and joystick events, so getting signals
    into Python is trivial. In this case, `event.get` will monitor for the
    letter **q** to be pressed, or a window close-button to be clicked.

36. The next line, indented one more level because it exists within the
   for-loop, creates an if-statement. If an event is equal to the `QUIT`
   function (which pygame interprets and defines for you), then the next
   line of code will be executed...

37. The final line, indented yet one more level since it's a member of
   the if-statement within the for-loop inside the while-loop, defines
   that if an event is equal to `QUIT`, then it should quit pygame in one
   of the two recommended methods, depending on what operating system it
   is running upon.

Game Plumbing
`````````````

Now that you are inside the while-loop, you need pygame to do three
things to keep your game alive. You need the sprites to be drawn, the
frame counter to advance according to your frame rate, and the sprites
to be re-drawn::
  38. ¬ all_sprites_list.draw(window_area)
  39. ¬ clock.tick(fps)
  40. ¬ pygame.display.flip()

38. The first line, indented because it is still within the while-loop
   but *not* within your `QUIT` trap, draws the sprites into your game
   world.

39. The second line tells the clock to proceed at the frame rate you
   defined earlier in your code.

40. The third line implements the refreshing of the screen with a
   pygame function called `flip`. If you think of your pygame as an
   animation, then you can imagine that `flip` advances everything
   forward by one frame. Or you might think of it as turning a page in
   one of those animation flipbooks you may have made as a child.

Movement Controls
~~~~~~~~~~~~~~~~~

It's been about 40 lines of code, but at this stage your game is
*technically* capable of running. You have not programmed in any
controls yet, so the player won't be able to move the avatar, but the
game will launch, spawn sprites, and continue to run until it receives
a `QUIT` signal. 

Try running your game now, just to see what you have
built so far. Go to the Tools menu of SPE and select Run/Stop , or use
the keyboard shortcut control r . A dialogue box will appear,
prompting for arguments but since your game takes no arguments, just
click the Run button to proceed. 

When your programme runs, you should
see a new window open, with one hero avatar and ten collectible item
sprites. You should be able to press q on your keyboard, or click the
Close Window button in the window decoration, to quit the game. 

Now that you have established that your code works such as it is, you
can programme movement controls. Luckily, pygame handles all of this
deftly.

Two sections of new code will be required. The first is a set
of variables representing directions in which a player can move, plus
the speed at which the player will move.

These variables must be established up front, from the moment the game
is launched. In other words, they are variables that should be created
in the initialization portion of your programme, not within the `main`
loop. Somewhere above your `main` loop, insert this code::
  moveDown = True
  moveUp = True
  moveLeft = True
  moveRight = True
  move_speed = 10

This code block establishes variables that, if changed, will cause the
player's avatar to move.

Just as with the `QUIT` signal that you monitor to close the `main`
loop, you can also monitor for key presses that will cause the
player's avatar to move. First, you must understand that there are two
states of a key on a keyboard or button on a mouse; the key is
*pressed* or the key is *not pressed*. In pygame vernacular, these
states are KEYDOWN and KEYUP, and the function that identifies them is
`event.key`. 

Establishing a control scheme is repetition of the same basic theory:
if `event.key` detects a left- arrow or a , then set the variable
`moveRight` to False and the variable `moveLeft` to True. And so on
for each possible valid keypress.

It looks like a lot of code, but in fact it's mostly just straight
copy and paste from online documentation, and a lot of repetition to
account for each possible direction a player might move.

Notice that the entire code block for movement will begin indented
twice, since it is a part of the while-loop::

  '''movement schema'''	
  ¬¬ if event.type == KEYDOWN:
  ¬¬¬ if event.key == K_LEFT or event.key == ord('a'):
  ¬¬¬¬ moveRight = False
  ¬¬¬¬ moveLeft = True
  ¬¬¬ if event.key == K_RIGHT or event.key == ord('d'):
  ¬¬¬¬ moveLeft = False
  ¬¬¬¬ moveRight = True
  ¬¬¬ if event.key == K_UP or event.key == ord('w'):
  ¬¬¬¬ moveDown = False
  ¬¬¬¬ moveUp = True
  ¬¬¬ if event.key == K_DOWN or event.key == ord('s'):
  ¬¬¬¬ moveUp = False
  ¬¬¬¬ moveDown = True
  ¬¬ if event.type == KEYUP:
  ¬¬¬ if event.key == K_ESCAPE or event.key == ord('q'):
  ¬¬¬¬ pygame.quit()
  ¬¬¬¬ sys.exit()
  ¬¬¬ if event.key == K_LEFT or event.key == ord('a'):
  ¬¬¬¬ moveLeft = False
  ¬¬¬ if event.key == K_RIGHT or event.key == ord('d'):
  ¬¬¬¬ moveRight = False
  ¬¬¬ if event.key == K_UP or event.key == ord('w'):
  ¬¬¬¬ moveUp = False
  ¬¬¬ if event.key == K_DOWN or event.key == ord('s'):
  ¬¬¬¬ moveDown = False
  
As is, that block of code will allow your player to move in any
direction by using either the arrow keys or the quintessential wasd
combination that longtime PC gamers know and love. Unfortunately, it
will let the player move *forever*, right off screen, in fact.

To prevent the player from walking so far off screen that their avatar
becomes hopeless lost, you must prevent the player's position from
exceeding the minimum and maximum values of the width and height of
your game world. This is usually done in one to two ways;
mathematically or collision. You have probably seen both, if you have
played video games. Both are important concepts, but you will use the
mathematical method here, and collision for another task later in the
code. 

Insert this block of edge detection code anywhere within your `main`
loop. Note that it starts indented only once because it's within the
while-loop::

  ¬ if moveDown and player.rect.bottom < window_height:
  ¬¬ player.rect.top += move_speed
  ¬ if moveUp and player.rect.top > 0:
  ¬¬ player.rect.top -= move_speed
  ¬ if moveLeft and player.rect.left > 0:
  ¬¬ player.rect.left -= move_speed
  ¬ if moveRight and player.rect.right < window_width:
  ¬¬ player.rect.right += move_speed

As you can probably see, this code block is a series of if-statements
which invoke pygame's awareness of the bounding box of your sprite's
avatar (which you established way back at line 16) and compares the
leading edge of the bounding box to the value of the window width and
height. If the values of whichever edge is moving is greater than the
maximum value or less than the minimum, then the avatar stops moving
in that direction.

For example, if the player has caused `moveRight` to be True by
pressing the up-arrow key, then pygame compares the value of the
rightmost edge of the avatar to the rightmost value of the world's
width. For as long as the edge is less than the maximum value of the
world, the player moves 10 pixels to the right (because you set the
player's `move_speed` to 10 when creating your movement
variables). 

Much of the movement controls looks a lot more confusing than it
actually is, and fortunately most of it is fairly standard. 

If you type in the code once, you have a block of motion control code
that you can re-use in any game you write. If you were to play the
game now, then you would see that the player's avatar moves, but it
leaves a trail behind it marking everywhere it's been. This is
happening because the sprites are being re-drawn, but never are they
erased. This is a great trick to know if you are designing a paint
programme, but not too useful in a game.

To clear the past positions of a moving sprite, redraw a fresh
background along with the sprites.

The following code block of one line will refresh the background.
Notice that it is indented but once, as it is an independent line
within the while-loop::
  ¬ window_area.fill(blue)

Place this window filler line *above* the line that draws the sprites.
In other words, you should have a block of code that reads a little
something like this::
  ¬ window_area.fill(blue)
  ¬ all_sprites_list.draw(window_area)
  ¬ clock.tick(fps)
  ¬ pygame.display.flip()

Killing off a Sprite
~~~~~~~~~~~~~~~~~~~~

Now your game not only launches and generates sprites, it also allows
your player to move around the world freely. What it does not yet do
is detect that the player has moved over the collectible items. When
you want one sprite to affect another, you want collision detection, a
basic game mechanic for any game framework and a mere four lines in
pygame::
  ¬ food_hit_list = pygame.sprite.spritecollide(player, food_list, True)  
  ¬ for spawn in food_hit_list:
  ¬¬ score +=1
  ¬¬ print( score )

In this code block, you first create a variable and set its value to
the pygame function `sprite.spritecollide`. This function takes three
arguments: the name of one constant sprite (the player, who will never
disappear), the name of another sprite or a sprite group, and the
boolean condition True. Without knowing or needing to know how pygame
manages it, you use this function so that it checks to see if the
player sprite has collided with anything in the `food_list`.

The second line in the code block checks to see if this condition is
True.  It does this each time it cycles through the game loop. If it
is True that two sprites have collided, then it adds the collided
sprite to the variable `food_hit_list` and increments the player's
score.

The final line of code in this blok increments the player's score by 1
and prints the score into whatever console is running the game. If you
run the game from a terminal, then you will see the score dumped into
the console as you play.

That's all there is to collision detection with pygame, and that's all
there is to the game. It is now a fully functional, albeit basic,
game.

Feeding Frenzy
--------------

The complete source
code::
   #!/usr/bin/env python

    import pygame
    from pygame.locals import *
    import sys
    import random
    
    
    pygame.init()
    
    
    clock = pygame.time.Clock()
    fps = 15
    
    
    # colours
    black = (26, 26,  26)
    blue = (57, 103, 189)
    keyer = (0, 0, 0)
    
    
    score = 0
    
    
    # create a sprite
    class avatar(pygame.sprite.Sprite):
    ¬ def __init__(self, filename):
    ¬ ¬ pygame.sprite.Sprite.__init__(self)
    ¬ ¬ self.image = pygame.image.load(filename).convert()
    ¬ ¬ self.image.set_colorkey(keyer)
    ¬ ¬ self.rect = self.image.get_rect()
    
    
    window_width = 960
    window_height = 720
    window_area = pygame.display.set_mode([window_width,window_height])
    pygame.display.set_caption('Feeding Frenzy')
    
    
    all_sprites_list = pygame.sprite.Group()
    food_list = pygame.sprite.Group()
    '''player sprite below'''
    player = avatar("cat1.png")
    all_sprites_list.add(player)
    
    
    for i in range(10):
    ¬ collectible = avatar("food.png")
    ¬ collectible.rect.x = random.randrange(window_width)
    ¬ collectible.rect.y = random.randrange(window_height)
    ¬ '''list management'''
    ¬ food_list.add(collectible)
    ¬ all_sprites_list.add(collectible)
    
    
    moveDown = True
    moveUp = True
    moveLeft = True
    moveRight = True
    move_speed = 10
    
    
    main = True
    while main == True:
    ¬ for event in pygame.event.get():
    ¬ ¬ if event.type == pygame.QUIT:
    ¬ ¬ ¬ pygame.quit(); sys.exit()
    ¬ ¬ ¬ main=False
    
    
    if event.type == KEYDOWN:
    ¬ if event.key == K_LEFT or event.key == ord('a'):
    ¬ ¬ moveRight = False
    ¬ ¬ moveLeft = True
    ¬ if event.key == K_RIGHT or event.key == ord('d'):
    ¬ ¬ moveLeft = False
    ¬ ¬ moveRight = True
    ¬ if event.key == K_UP or event.key == ord('w'):
    ¬ ¬ moveDown = False
    ¬ ¬ moveUp = True
    ¬ if event.key == K_DOWN or event.key == ord('s'):
    ¬ ¬ moveUp = False
    ¬ ¬ moveDown = True
    if event.type == KEYUP:
    ¬ if event.key == ord('q'):
    ¬ ¬ pygame.quit(); sys.exit()
    ¬ if event.key == K_LEFT or event.key == ord('a'):
    ¬ ¬ moveLeft = False
    ¬ if event.key == K_RIGHT or event.key == ord('d'):
    ¬ ¬ moveRight = False
    ¬ if event.key == K_UP or event.key == ord('w'):
    ¬ ¬ moveUp = False
    ¬ if event.key == K_DOWN or event.key == ord('s'):
    ¬ ¬ moveDown = False
    
    
    # move the player sprite
    if moveDown and player.rect.bottom < window_height:
    ¬ player.rect.top += move_speed
    if moveUp and player.rect.top > 0:
    ¬ player.rect.top -= move_speed
    if moveLeft and player.rect.left > 0:
    ¬ player.rect.left -= move_speed
    if moveRight and player.rect.right < window_width:
    ¬ player.rect.right += move_speed
    
    
    # See if the player spawn has collided with anything.
    food_hit_list = pygame.sprite.spritecollide(player, food_list, True)  
    
    
    # Check the list of collisions.
    for spawn in food_hit_list:
    ¬ score +=1
    ¬ print( score )
    
    
    window_area.fill(black)
    
    
    # Draw all the spites
    all_sprites_list.draw(window_area)
    # advance the clock
    clock.tick(fps)
    
    
    # update the window_area with what we've drawn.
    pygame.display.flip()
