Python on the Desktop
=====================

Through BASH, you have learned the basics of programming concepts.
With BASH and Python, you learned the difference between scripting,
procedural programming, and object-oriented programming. With Python
and Pygame, you learned how to wield and perfect your programming
skills, and how to use existing frameworks (also sometimes called an
API) to create graphical applications.

Obviously Pygame is not the only API out there, nor is Python the only
object-oriented programming language. It will behoove you to have some
experience with another API, just so you can see how everything you
have learned about Pygame and how to discover its features translates
to some other framework.

The framework you will use for this section of the book is not only
for Python; it is most frequently used as a framework for C++, an
advanced programming language that builds upon C and is used for
applications ranging from office tasks, graphic manipulation, video
editing, and much much more. The framework is called Qt ("cute"). It
was originally developed by Trolltech and later purchased by Nokia and
then by Digia.  It is an ardently cross-platform framework in use on
Linux, Mac, Windows, and mobile platforms. One of its most famous
cross-platform uses has been on Skype , but there are many more
examples. You can learn more about Qt at `qt-project.org
<http://qt-project.org>`_.

Since `Qt` was built primarily for C++, it is not compatible with Python
out of the box. In order for Python to hook into the Qt framework, you
must install code that binds Python commands to C++ libraries. These
are usually called "bindings", and they are maintained by `Riverbank
Computing <http://www.riverbankcomputing.com/software/pyqt/intro>`_. On Slax,
all of this can be accomplished by going to
`slax.org/en/modules.php?category=develop
<http://slax.org/en/modules.php?category=develop>`_ and downloading
and activating `pyqt`.

Documentation
-------------

Your first stop should be the `documentation
<https://qt-project.org/doc/qt-4.8/classes.html>`_ site for `Qt`. Much
of this will be foreign to you because it's written for C++ but at
least it gives you an overview of all the different classes are
available, what each class does, and what functions those classes
contain.

There are, of course, a lot of classes, and each class has a lot of
different functions. In a word, it's overwhelming. However, when
starting out with a new language or API, it always helps to either
look at example code or a tutorial. This is the latter, but there are
plenty of examples and other tutorials online, so in the future you
can refer to those. It's normal to have a little "down time" in order
to research the tools that you'll be using for a project, so don't
feel like you're the only person in the world who has to pause to
study a little before actually writing code.

A Basic PyQt Application
------------------------

Let's write a basic GUI calculator application with PyQt. To keep it
simple, we will only offer addition and subtraction, and we'll have
the user enter the two numbers manually via the keyboard rather than
onscreen buttons. The important thing is that you will learn how to
look at a new API and use it with a language that you already know.

Looking at the application from a pseudo-code perspective, we could
say that the steps in programming will be:

* Open an application window.
* Get a number.
* Define whether we are adding or subtracting.
* Get a second number.
* Add or subtract the two numbers.
* Present the new number to the user.

Knowing what you know about scripting versus object oriented
programming, you can probably already deduce that the user will not be
prompted for any of this information, but presented with a graphical
interface which patiently awaits their input.

A quick, basic sketch of the graphical interface might be helpful.

.. image:: /images/calculator_mockup.png

Now that we know what we need, we could go investigate how to get
those graphical elements by reading up on Qt documentation. However,
since this is a book dedicated to teaching you new stuff, skip that
for now and just read on::

  01. #!/usr/bin/env python
  02. import sys
  03. from PyQt4.QtCore import *
  04. from PyQt4.QtGui import *

1. The first line declares the type of document.
2. Imports the Python sys module.
3. Imports the QtCore module from PyQt. This module contains the core
   non-GUI classes, including the event loop and something called the
   "signal and slot" mechanism, which you will use a little in this
   programme and a lot later on if you continue doing traditional
   application programming. It also includes platform independent
   abstractions for Unicode, threads, mapped files, shared memory,
   regular expressions, and user and application settings; put simply,
   it's the core of Qt....hence the name of the module.
4. Imports the QtGui module from PyQt. This contains most of the GUI-
   related classes and functions.

For this application, you only need one class. Within the class, you
will have two functions, which will feel pretty familiar to you, as
one will be called `__init__` and the other `update`. These two
function names and the jobs they perform, you have already used in
Pygame; the `__init__` will take care of the initial creation of the
GUI window and the `update` function will post any changes within the
application as it runs, much as the functions of the same names did
with your `Player` sprite, remember?::
  05. class Form(QDialog):
  06. ¬ def __init__(self,parent=None):
  07. ¬¬ super(Form,self).__init__(parent)
  08. ¬¬ self.setWindowTitle("Calculator")

5. Creates a class called `Abacus` by using the builtin class
   `QDialog`. The `QDialog` class of Qt draws a basic window which can
   contain any number of GUI elements; think of it, more or less, as the
   `display` module of Pygame.
6. Creates a new function called `__init__` to handle all the initial
   setup of the class (and in this case, since there will be only one
   class in the entire application, it is handling the initialization of
   the application itself). As arguments, it requires itself, and we
   additionally define `parent` equal to None. That means that it is an
   independent window which is not the child or spawn of another larger
   window; we use this so that our calculator application is not seen as
   a dialogue box of a greater application, but as an application in
   itself.
7. Uses the `super` function of Python to allow the `Abacus` class to
   access variables and functions created in the class itself. Without
   `super`, `Abacus` would attempt to check in on itself for data like
   layout and what text needs to be displayed in the window it has
   spawned, and would not receive valid information.
8. Sets the window title of your application to "Calculate". This is
   the text that will appear in the title bar of the window that PyQt
   opens on the user's computer.

Now you can insert the useful fields. Start with the first number we
need from the user::
  09. ¬¬ self.firstnumber = QLineEdit("00")
  10. ¬¬ self.firstnumber.selectAll()
  11. ¬¬ self.firstnumber.setFocus()

9. Creates a new variable called `firstnumber`, specifying it as a
   variable belonging to the `Abacus` class with the prefix `self`, for
   the benefit of Qt . The variable is set to contain the `QLineEdit`
   function of PyQt, which is, simply, a text entry field. In
   parenthesis, the default text is provided.
10. Modifies the variable that you have just created by sending it
   through the PyQt `selectAll` function, which selects all the text and
   moves the cursor to the end of the default text. If the user starts
   typing, the default text is replaced.
11. Modifies the variable `firstnumber` by sending it through the
   `setFocus` function of PyQt, which, as you might guess, assures that
   when the application is opened, the user's cursor is placed inside the
   edit field for the first number. That way, the user only needs open
   the application and start typing the number they want calculated,
   rather than clicking in the field first and then typing. It's a small
   feature, but it's nice for the user, and it only takes a line of code
   for the programmer, so why not use it?

Now give the user the opportunity to decide whether addition or
subtraction is desired::

  12. ¬¬ self.funcCombo = QComboBox()
  13. ¬¬ self.funcCombo.addItem("+")
  14. ¬¬ self.funcCombo.addItem("-")

12. Creates a variable (within the `self` namespace) and sets it to the
   `QComboBox`. A QCombobox is a dropdown menu.
13. You've added things to lists before, when you added sprites to a
   sprite group in Pygame. This line adds an entry into the dropdown menu
   you have just created. Specifically, this line adds a plus sign to the
   dropdown menu. Obviously, replacing the "+" with "Addition" or "plus"
   would work just as well; use your own preference.
14. Adds a minus sign to the dropdown menu. As before, replacing the
   "-" with "Subtraction" or "minus" would work just as well, so use
   whatever you like.

   The second number is obtained in exactly the same manner as the first::

     15. ¬¬ self.secondnumber = QLineEdit("00")

15. Creates a variable `secondnumber` in the `self` namespace and sets
   it to `QLineEdit` with the default text of 00. Notice that we do not
   use the self.foo.selectAll() line this time, because the first number
   is already the auto-selected field.

Next, we need a place to provide the answer for the user. On a fancier
application, you might work to generate a cool LCD screen with old
calculator characters to emulate a classic Texas Instruments handheld
calculator. For this application, we're just going to render the text
at the top of the window::

  16. ¬¬ self.total = QLabel()
  17. ¬¬ button1 = QPushButton("Calculate")

16. Creates a variable `total` in the `self` namespace and sets it to
   `QLabel`. QLabel allows PyQt to write text into a window. It is
   frequently used to label text fields or dropdown menus, but we will
   use it to display the result of the calculation being done. Note that
   right now, the QLabel is completely empty. We could set it to "00" if
   you wanted to indicate that no calculation has been done yet by
   placing "00" or similar in the parentheses. Later in the code, you'll
   update this QLabel with text that reflects the results of the
   calculation.
17. Creates a button labelled Calculate by creating a variable
   `button1` and setting it to the `QPushButton` function of PyQt.

GUI Layout
----------

So far, you've created variables and set them to take advantage of
various built-in functions and classes of PyQt. You have not yet told
PyQt where to place all of these different GUI elements you are using.

There are a few different ways to design the look of an application
such as this, including a drag-and-drop interface that ships along
with Qt Creator . For this application, however, we'll do it all in
code. PyQt allows for basic layouts through the `QVBoxLayout` and
`QHBoxLayout` classes; the former provides a basic vertical layout
scheme, and the latter a horizontal scheme. In terms of usage, you can
think of them as sprite groups, in a way::

  18. ¬¬ layout = QVBoxLayout()
  19. ¬¬ layout.addWidget(self.total)
  20. ¬¬ layout.addWidget(self.firstnumber)
  21. ¬¬ layout.addWidget(self.funcCombo)
  22. ¬¬ layout.addWidget(self.secondnumber)
  23. ¬¬ layout.addWidget(button1)
  24. ¬¬ self.setLayout(layout)

18. Creates a variable `layout` and sets it to `QVBoxLayout`, the
   vertical layout template for PyQt.
19. Adds an existing widget to the layout; think of this as a sprite
   group from Pygame, to which you add all of the widgets you have
   created with your variables. The first one we add is the total label,
   which will display the results of the calculation. Since it is listed
   first, and this is a vertical layout, this will be at the very top of
   the window.
20. Adds `firstnumber` to the layout.
21. Adds `funcCombo` (our dropdown menu) to the layout.
22. Adds `secondnumber` to the layout.
23. Adds `button1` to the layout. Since this is the last item added, it
   will appear at the very bottom of the window.
24. Calls the `setLayout` class from PyQt upon all items in the `self`
    namespace, applying the rules from the `layout` you have just defined.

Slots and Signals
-----------------

You might remember that in Pygame, there were special Pygame classes
that would detect things like collisions and key presses, and so on.
PyQt is not so game-centric, but it does similarly have special
classes to detect things like button presses, drag-and-drop actions,
mouse clicks, and other interactive signals. The Qt term for this is
"slots and signals".

Slots and signals are how one widget (such as a
button) can communicate with another )such as a label that we want to
update). They're a great feature of Qt and are quite elegant when
compared to some of the other methods that other frameworks provide.
You yourself could probably imagine a way around the idea of slots and
signals; if the user clicks a button, set a variable to `clicked`, and
then in a main loop have some other object monitor for a `clicked`
variable. If the variable `clicked` is found to be true, then do some
other thing. That would be a basic hack, and there are more
sophisticated ways of approaching it, but Qt 's slots and signal
mechanism is more sophisticated than most, and yet very easy for the
programmer to use::
  
  25. ¬¬ self.connect(button1, SIGNAL("clicked()"), self.update)

25. Calls the `connect` function of PyQt to make the `button1` variable
   a signal generator. A signal is emitted when `button1` is clicked. And
   the "slot" listening for the signal is the `update` function of our
   very own `Abacus` (or in this context, `self`) class. 

   In simpler terms, this means that when our Calculate button is
   clicked, a new function called `self.update` is executed. The
   obvious problem here is that you do not yet have a function called
   `update`. But then again, you also have not yet done any math and
   you're writing a calculator application; luckily, the `update`
   function is the exact place for all of the math needing to be done,
   so let's get started.

Updating the GUI
----------------

Just as you updated your sprite's condition and state in an update
function in Pygame, you update the state of your calculator. Since
this application requires the user to click a button to start the
calculations, constant updating is not necessary the way it was in
Pygame. But if the user does click the Calculate button, then we know
that we have all the numbers and information we need, so we can start
the calculations and provide the results::

  26. ¬¬ def update(self):
  27. ¬¬ FIRSTN = int(self.firstnumber.text())
  28. ¬¬ SECOND = int(self.secondnumber.text())
  29. ¬¬ FUNC = self.funcCombo.currentText()
  30. # maths
  31. ¬¬ if FUNC == '+':
  32. ¬¬¬ SUM = FIRSTN+SECOND
  33. ¬¬ else:
  34. ¬¬¬ SUM = FIRSTN-SECOND
  35. # set QLabel
  36. ¬¬ self.total.setText(str(SUM))

26. Creates a new function `update` within our `Abacus` class, pulling
   in all the data already established in the `__init__` function (ie, it
   brings in `self` as an argument).
27. Creates the variable `FIRSTN` and sets it to the results of
   converting the text in `firstnumber` to an integer. This is obviously
   important, given that math can only be done on integers, not text.
28. Creates the variable `SECOND` and sets it to the results of
   converting the text in `secondnumber` to an integer.
29. Creates a variable `FUNC` that contains the current text of the
   dropdown menu.
30. A comment for clarity.
31. An if-statement that looks at the contents of the `FUNC` variable
   we just created. If the variable contains a "+" sign (or whatever you
   put into the dropdown menu to denote addition), then...
32. ...addition is performed by setting a new variable, `SUM`, to the
   sum of `FIRSTN` and `SECOND`.
33. Otherwise...
34. ...subtraction is performed by setting a new variable `SUM` to the
   results of `FIRSTN` minus `SECOND`.
35. A comment for clarity.
36. Sets the text of the `total` label (which we are using to display
   our results) to whatever is contained within `SUM`.

That completes the class that makes up the bulk of our calculator
application. The only thing left to do is to call that class so that
the application actually runs when launched. In other words, we need a
main loop. 

Sort of.

Main Loop
---------

In Pygame, we used a main loop for all of the really important real-
time code. PyQt has a lot of the really important main-loop details
built into a class called `QApplication`. `QApplication` manages a lot
of details that you could, as is often the case, code yourself, but if
you did then you'd be working a lot harder than necessary. So rather
than writing out a main loop with all kinds of stipulations like user
styles, default fonts, keyboard events that should be monitored, and
so on, you can simply::

  37. app = QApplication(sys.argv)
  38. form = Abacus()
  39. form.show()
  40. app.exec_()

37. Creates a variable `app` that contains the actions of the
   `QApplication` class. It's good practise to pass through any system
   arguments that Qt allows a user to add on to any Qt application they
   run; that way, if a user needs your application to be aware of
   anything important, like what display to run on, or a special widget
   set to use when rendering, and so on, then your application will
   conform.
38. Creates a variable `form` and sets it to our very own `Abacus`
   class.
39. Calls upon our `Abacus` class to run, and filters it through the
   `show()` function of PyQt to ensure that it is displayed in the window
   that is spawned.
40. Executes the code in this application. By using the `app` variable,
    we ensure that everything runnin in this application is managed by the
    `QApplication` class. 

    Notice how we don't write even a line of code about listening for
    mouse clicks or keyboard entry, or telling our application how and
    when to quit, and so on. We had to do those things in Pygame, but
    not so in PyQt; we get all of those details done for us, for free,
    in `QApplication`.

Running the Application
-----------------------

Save your code and then run it by typing into a terminal::

  # python ./calculate.py

Your application should launch, and it should be able to do simple
math for you. Your first non-game GUI application is complete!

To run your application on other platforms, those platforms must have
Python and Qt installed. Python is already on Mac OS X and it is
downloadable for Windows, and Qt is available for both (it's a big
download, but worth it). If you're going to distribute your calculator
to friends, it's probably a good idea to hand them a copy of Python
and Qt as well, since you can't count on them figuring out how to
install dependencies such as those.

Homework
--------

This calculator is very basic, but that doesn't mean you can't make it
better.

* Try adding different functions (such as multiplication and division)
  to your code.
* Try modifying the code so that decimal numbers can be processed.
* Try adding a few different modes to your calculator; add a dropdown
  menu or some radio buttons that allow the user to choose between, for
  instance, currency conversion, temperature conversion, and so on.

Whatever you do with your calculator from this point on, you have a
working desktop application that you might actually find useful. And
it's something that you yourself built, so be proud of it, and use it,
and improve it. There's no better way to learn better code than by
using something you programmed, finding its bugs or annoyances, or
just thinking up cool new features that would be helpful to you, and
then adding those features in.

calculator.py
-------------

The source
code::
  #!/usr/bin/env python
  # GNU All-Permissive License
  # Copying and distribution of this file, with or without modification,
  # are permitted in any medium without royalty provided the copyright
  # notice and this notice are preserved.  This file is offered as-is,
  # without any warranty.
  
  import sys
  from PyQt4.QtCore import *
  from PyQt4 import QtGui
  
  class Abacus(QtGui.QDialog):
  ¬ def __init__(self,parent=None):
  ¬ ¬ super(Abacus,self).__init__(parent)
  ¬ ¬ self.setWindowTitle("Calculate")
  
  ¬ ¬ self.firstnumber = QLineEdit("00")
  ¬ ¬ self.firstnumber.selectAll()
  ¬ ¬ self.firstnumber.setFocus()
  
  ¬ ¬ self.funcCombo = QComboBox()
  ¬ ¬ self.funcCombo.addItem("+")
  ¬ ¬ self.funcCombo.addItem("-")
    
  ¬ ¬ self.secondnumber = QLineEdit("00")
  
  ¬ ¬ self.total = QLabel()
  ¬ ¬ button1 = QPushButton("Calculate")
  
  ¬ ¬ layout = QVBoxLayout()
  ¬ ¬ layout.addWidget(self.total)
  
  ¬ ¬ layout.addWidget(self.firstnumber)
  ¬ ¬ layout.addWidget(self.funcCombo)
  ¬ ¬ layout.addWidget(self.secondnumber)
  
  ¬ ¬ layout.addWidget(button1)
  ¬ ¬ self.setLayout(layout)
  ¬ ¬ self.connect(button1, SIGNAL("clicked()"), self.update)
  
  ¬ def update(self):
  ¬ ¬ FIRSTN = int(self.firstnumber.text())
  ¬ ¬ SECOND = int(self.secondnumber.text())
  ¬ ¬ FUNC = self.funcCombo.currentText()
  
  ¬ ¬ if FUNC == '+':
  ¬ ¬ ¬ SUM = FIRSTN+SECOND
  ¬ ¬ else:
  ¬ ¬ ¬ SUM = FIRSTN-SECOND
  
  ¬ ¬ self.total.setText(str(SUM))
  
  app = QApplication(sys.argv)
  form = Abacus()
  
  form.show()
  
  app.exec_()
