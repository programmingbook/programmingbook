A Basic PyQt Text Editor
========================

In the beginning of this book, you used BASH to programme a rolodex-
style application that you could ostensibly use to take notes and to
review them. Now, at the end of the book, you will write your very own
graphical text editor, a little like Kate on Slax Linux, or Notepad
and similar applications on Windows and OS X.

Your text editor will be useful; you will actually be able to use in
real life. It won't be overflowing with features, although after you
code it, you might see some features that you would like to add, and
you'll actually know how to go about adding them. And since we're
using PyQt , the entire source code will only be about 100 lines.

A quick, basic sketch of the graphical interface might be
helpful.

.. image:: /images/textedit_mockup.jpg

Once again, once you know what you want, you can start
researching, in your framework's documentation, how to make it all
happen. Feel free to do just that, and see if you can predict the
logic. When you've finished, start coding along and see how you did::

  01. #!/usr/bin/env python

  02. import sys
  03. import os
  04. from PyQt4.QtGui import *

1. Declares where Python is located.
2. Imports the sys module of Python so that your programme can access
   files.
3. Imports the os module of Python so that your programme knows how to
   talk to whatever computer it happens to be running on.
4. Imports the QtGui set of classes from PyQt so that the programme
   will have access to the common Qt widgets and functions.

This kind of header is probably starting to look pretty familiar to
you. My first iteration of this sample code imported far more modules
than I ended up using, so I trimmed it down; it's commmon to import
too much or too little, so when you're working from scratch just
import whatever you think you will need after reading up on the
framework that you are using. You can add more, or take away excess,
later.

Init
----

Next, you will create a class. It will be the only class of this
application, but don't let that fool you; it has plenty of functions
to keep you occupied::

  05. class TextEdit(QMainWindow):
    
  06. ¬ def __init__(self):
  07. ¬¬ super(TextEdit, self).__init__()
  08. ¬¬ #font = QFont("Courier Prime", 11)
  09. ¬¬ #font.setFixedPitch(True)
  10. ¬¬ #self.setFont(font)
  11. ¬¬ self.filename = False
  12. ¬¬ self.Ui()

5. Creates a class arbitrarily named `TextEdit`, by invoking the
   `QMainWindow`.
6. Creates a new function called `__init__` to handle all the initial
   setup of the class. In this case, since there will be only one
   class in the entire application, it is handling the initialization
   of the application itself). As arguments, it requires only itself.
   
   Notice that we do not, as we did with the calculator application,
   specify that this window has no parent. After all, this is a
   `QMainWindow`, so it's assumed that it has no parent. In fact, it's
   a lot more likely that this will be a parent of another window and
   in fact, it will be, since when we start saving files from our text
   editor we'll call upon `QDialog` to generate a dialog window with
   this window as its parent.

7. Uses the `super` function of Python to allow the `TextEdit` class
   to access variables and functions created in the class itself. Without
   `super`, `TextEdit` would attempt to check in on itself for data like
   the contents of variables and what text needs to be displayed in a
   dialog window it has spawned, and would not receive valid information.
8. Creates a new variable called `font` and sets it to the results of
   the `QFont` function. In this case, `QFont` sets the application's
   font to Courier Prime (a free courier-style font). It is also
   commented out, so the application will inherit the default system
   font. I leave this line in the source code, however, so that you can
   see how to invoke functions from PyQt.
10. Processes the `font` variable through the `setFont` function. Well,
   it would if it were uncommented, anyway.
11. Creates the new variable `filename` and sets it to the string
   `False`. In other words, the default state of the text editor will be
   to spawn a file without a filename.
12. Calls the `Ui` method, which doesn't actually exist yet but you'll
   create it next. The important thing is that the initialization of the
   application is done, and the next method is called.

Menu
----

The next method you will create will handle the layout and basic
functionality of the application. The first thing you will create is a
menubar.

The next block of code looks like a lot more than it actually is. Look
for the pattern and you can see that there's a lot of repetition
involved, since most menu items do essentially the same thing: they
get created, added to a menu, and then call a separate function::

  13. ¬ def Ui(self):
  14. ¬¬ newFile = QAction('New', self)
  15. ¬¬ openFile = QAction('Open', self)
  16. ¬¬ saveFile = QAction('Save', self)
  17. ¬¬ quitApp = QAction('Quit', self)
      
  18. ¬¬ copyText = QAction('Copy', self)        
  19. ¬¬ pasteText = QAction('Yank', self)
  
  20. ¬¬ newFile.setShortcut('Ctrl+N')
  21. ¬¬ newFile.triggered.connect(self.newFile)
  22. ¬¬ openFile.setShortcut('Ctrl+O')
  23. ¬¬ openFile.triggered.connect(self.openFile)
  24. ¬¬ saveFile.setShortcut('Ctrl+S')
  25. ¬¬ saveFile.triggered.connect(self.saveFile)
  26. ¬¬ quitApp.setShortcut('Ctrl+Q')
  27. ¬¬ quitApp.triggered.connect(self.close)
  28. ¬¬ copyText.setShortcut('Ctrl+K')
  29. ¬¬ copyText.triggered.connect(self.copyFunc)
  30. ¬¬ pasteText.setShortcut('Ctrl+Y')
  31. ¬¬ pasteText.triggered.connect(self.pasteFunc)

13. Creates the method `Ui`. Since this application only consists of
   two interface items (a menubar and a text field), there's no need to
   choose between a layout scheme as you did with the calculator
   application, so this, as user interfaces go, is fairly simple. But the
   method itself does draw the UI, so I named it `Ui`.
14. In this first block of code, variables are created to house each
   selectable menu item. The first is `newFile`, which a user will select
   to create a new empty file. We use the `QAction` function of Qt, which
   takes as its first argument the text label, and `self`.
15. Creates the variable `openFile` using `QAction`, providing a text
   label that will appear in the menu, and access to all `self` data.
16. Creates the variable `saveFile` using `QAction`, providing a text
   label that will appear in the menu, and access to all `self` data.
17. Creates the variable `quitApp` using `QAction`, providing a text
   label that will appear in the menu, and access to all `self` data.
18. Creates the variable `copyText` using `QAction`, providing a text
   label that will appear in the menu, and access to all `self` data. In
   the source code, I am using the old traditional Unix term "Yank" as
   the text label, because I'm used to it and find it more comfortable
   than "Paste" but you can feel free to change that if you'd like.
19. Creates the variable `pasteText` using `QAction`, providing a text
   label that will appear in the menu, and access to all `self` data.

20. The next block of code assigns keyboard shortcuts to each menu item
   just created, and then uses Qt's signals and slots mechanism to
   dictate what should happen when the menu item is selected by the user.
   The first shortcut assigned is Ctrl+N , a common keyboard combination
   used to start a new file.
21. The `newFile` menu item, if selected, triggers the as-yet
   nonexistant `self.newFile` function (you'll create it soon, I
   promise).
22. Sets the shortcut for `openFile`.
23. Connects the `openFile` menu selection to the `self.` function, which we
   have to create later in this class.
24. Sets the shortcut for `saveFile`.
25. Connects the `saveFile` menu selection to the `self.saveFile`
   function, which we have to create later in this class.
26. Sets the shortcut for `quitApp`.
27. Connects the `quitApp` menu selection to the `self.close` function,
   which is a built-in function of PyQt, so it's one function you will
   *not* have to create.
28. Sets the shortcut for `copyText`. In my source code, I use the old,
   traditional Unix shortcut of Control+K because I'm used to it and find
   it convenient. You may prefer Control+C
29. Connects the `copyText` menu selection to the `self.copyFunc`
   function, which we have to create later in this class.
30. Sets the shortcut for `pasteText`. In my source code, I use the
   old, traditional Unix shortcut of Control+Y because I'm used to it and
   find it convenient. You may prefer Control+Y
31. Connects the `copyText` menu selection to the `self.copyFunc`
   function, which we have to create later in this class.

That took care of keyboard shortcuts, now we deal with the actual menu
instance itself::

  32. ¬¬ menubar = self.menuBar()
  33. ¬¬ menubar.setNativeMenuBar(True)
    
  34. ¬¬ menuFile = menubar.addMenu('&File')
  35. ¬¬ menuFile.addAction(newFile)
  36. ¬¬ menuFile.addAction(openFile)
  37. ¬¬ menuFile.addAction(saveFile)
  38. ¬¬ menuFile.addAction(quitApp)
    
  39. ¬¬ menuEdit = menubar.addMenu('&Edit')
  40. ¬¬ menuEdit.addAction(copyText)
  41. ¬¬ menuEdit.addAction(pasteText)

32. Creates the actual menubar for which you have been creating all
   these variables for in the first place. You do this by creating a
   variable `menubar` and setting it to the `menuBar` function of PyQt.
33. Processes the `menubar` variable through the `setNativeMenuBar`,
   setting its option to `True`. This ensures that this application's
   menu will conform to any system with particularly unique menubar
   implementations, such as the Mac OS X top menubar.

34. Similar to how you added platform ledges and sprites to sprite
   groups in Pygame, you must add toplevel menu categories to a menubar.
   The first to add is the ubiquitous File menu. The syntax is fairly
   intuitive; create a variable `menuFile` and set it to the result of
   the `addMenu` function of PyQt. The text label of the menu is given as
   '&File' but the ampersand won't actually be visible; it only denotes
   that the letter to its immediate right (in this case, "F") will become
   the alt keyboard shortcut for that menu, which is a conventional
   mouseless accessibility option on Linux and Windows computers.
35. In addition to adding the top-level menu names to the menubar, each
   menu item must be added. This line adds the `newFile` action. We
   reference it by its variable, but of course the user will see it by
   the text label given when you first created the variable with
   `QAction`.
36. Adds the `openFile` menu item to the File menu.
37. Adds the `saveFile` menu item to the File menu.
38. Adds the `quitApp` menu item to the File menu.
39. Starts a new top-level menu, this one called `menuEdit` and
   labelled Edit for the user. The alt keyboard shortcut will be e , as
   denoted by the ampersand.
40. Adds the `copyText` menu item to the Edit menu.
41. Adds the `pasteText` menu item to the Edit menu.

That's all it takes to create the skeletal structure of your
application's menu. Granted, it's not a full-featured text editor
(aving no search, search-and-replace, spell check, and so on) but it
does have the essential features for jotting down quick notes or
viewing simple text README files or similar.

There are a few more items that the `Ui` method must tend to, and then
we can start creating the code that will drive all of those fancy menu
items we have just made::

  42. ¬¬ self.text = QTextEdit(self)
  43. ¬¬ self.setCentralWidget(self.text)
  44. ¬¬ self.setMenuWidget(menubar)
  45. ¬¬ self.setMenuBar(menubar)

42. Amazingly, this one line creates about 98% of your application, at
   least if you think of what one might imagine a programmer would spend
   time and effort on when programming a text editor. In reality, every
   bit of editing text actually occurs magically as a result of the
   `QTextEdit` widget.

   This one liner creates a variable `self.text` and sets it to an
   instance of `QTextEdit`. The prefix `self` is used to ensure that
   this widget is available to all functions of the application;
   that's important when you start doing things like saving data out
   of your application and into files on the user's computer.  Imagine
   telling a programme to save all the text that a user has just typed
   only to have the application ask "Text? what text?". That's exactly
   what would happen if the text edit widget was locked inside of the
   `Ui` method without any way of contacting the rest of the class.
   Prepending `self` before the variable makes it a global variable of
   which the other functions are inherently aware.
43. Sets the central widget of the window, meaning that the application
   will launch with this widget having focus. On a lower level, it also
   means that `QMainWindow` takes ownership of this widget and will
   manage its various states. That's a good thing; it binds the window
   and its main widget together and allow you, the programmer, to more or
   less forget about them. The application will close this widget and
   clean up any temporary data when the quit signal is received so that
   you don't have to.
44. Sets the menubar as the menu widget of this application. Not
   strictly necessary in a simple application like this, but a good habit
   to get into.
45. Sets `menubar` as the menubar of this application. Not strictly
   necessary in a simple application like this, but a good habit to get
   into.

That took care of the menu bar instance, so now you can deal with some
of the details involved in spawning the application window itself::

  46. ¬¬ self.setGeometry(200,200,480,320)
  47. ¬¬ self.setWindowTitle('TextEdit')
  48. ¬¬ self.show()

46. Sets the default location on the user's screen where the
   application will appear when launched, and the size of the application
   window. I have the application opening 200 pixels from the top and 200
   pixels from the left edges of the screen, and opening as a modest 480
   by 320 window.
47. Sets the window title of this application; this is the text that
   will appear at the top of the window that opens when the application
   is launched.
48. Enables the application to be visible. Users tend to get irritated
   when their applications are invisible.

Copy Paste
----------

The copy and paste functions are freebies, courtesy Qt::
  49. ¬ def copyFunc(self):
  50. ¬¬ self.text.copy()
    
  51. ¬ def pasteFunc(self):
  52. ¬¬ self.text.paste()

49. Creates a method `copyFunc` and grants it access to all the data
   contained within the class via `self`.
50. Uses the builtin Qt `copy` function on the `text` widget (in other
   words, we inherit the ability to copy from `QTextEdit`). This action
   copies text to the system clipboard, regardless of operating system.
51. Creates a method `pasteFunc` and grants it access to all the data
   contained within the class via `self`.
52. Uses the builtin Qt `paste` function on the `text` widget (in other
   words, we inherit the ability to paste from `QTextEdit`). This action
   pastes text from the system clipboard, regardless of operating system.
   It even works fluidly with the clipboard history managers that ship
   along with most Linux desktops.

Unsaved Changes
---------------

This application might be but practise for you and me, but if anyone
is going to use your text editor (even if that someone is you) then we
as programmers suddenly inherit the very real and very serious duty of
safeguarding the user's data. The last thing you ever want is for a
user to spend time entering data, whether it's just classroom notes or
something precious like a poem or code, only to lose the code because
they accidentally closed the window without saving or called up an
empty file not realizing that they would not be prompted to save
first.

To prevent losing data, our programme will warn the user if there are
unsaved changes whenever the user tries to open a new empty file, or
to quit the application.

As you might have expected, `QTextEdit` makes it really easy to check
if there have been changes since the last save. It stores the
information as a boolean (ie, either "True" or "False", or "0" or "1")
in a function called `isModified`. 

Figuring out how to use such helpful functions can sometimes be
difficult. When I first learnt of the `isModified` function, I tried
the usual variable-processing methodology, such as::

  self.text.isModified()

But of course that did not work, and if you think about it logically
then you can see why. Simply stating self.text.isModified() is a
little like shouting "True!" in a crowded theater. Everyone can hear
you, but no one will have any idea why you have just shouted.

The way to properly communicate back and forth with the computer is
through variables. So, you assign a variable to the results of the
`isModified` function and then check to see what the variable
contains, doing one thing or another depending on whether the contents
of the variable is True or False. 

It so happens that the correct way to call `isModified` is through the
`document` object, which you will see in the source code. Eventually,
I promise, you'll be able to read documentation and get all of this
stuff on your own, but it does take time, so keep at it::

  53. ¬ def unSaved(self):
  54. ¬¬ destroy = self.text.document().isModified()
      
  55. ¬¬ if destroy == False:
  56. ¬¬¬ return False
  57. ¬¬ else:
  58. ¬¬¬ detour = QMessageBox.question(self,
      ¬¬¬¬¬¬¬ "Hold your horses.",
      ¬¬¬¬¬¬¬ "File has unsaved changes. Save now?",
      ¬¬¬¬¬¬¬ QMessageBox.Yes|QMessageBox.No|
      ¬¬¬¬¬¬¬ QMessageBox.Cancel)
  59. ¬¬¬ if detour == QMessageBox.Cancel:
  60. ¬¬¬¬ return True
  61. ¬¬¬ elif detour == QMessageBox.No:
  62. ¬¬¬¬return False
  63. ¬¬¬ elif detour == QMessageBox.Yes:
  64. ¬¬¬¬ return self.saveFile()
    
  65. ¬¬ return True`

53. Creates a new method within the `TextEdit` class called `unSaved`
   because it will check for unsaved changes. The purpose of this
   function is to check for unsaved changes, and if unsaved changes are
   found then prompt the user to save, discard changes, or cancel.
54. Creates a variable called `destroy` and sets it to the results of
   the `QTextEdit` function `isModified`. The results are either True or
   False. To see these results, you could insert a statement like
   print(destroy) and run the programme from a terminal; when you go to
   save, the value of `destroy` will appear in the terminal output. This
   is a useful debugging trick that is very common.

55. Starts an if-statement that depends upon the value of `destroy`.
   This initial line states that if the value of `destroy` turns out to
   be False, then return a false `False` signal to the function that
   requested `unSaved`. In other words, if `isModified` is `False`, then
   no saving action is required and the the programme can continue as
   planned.
56. Returns false if `destroy` contains the value `False`.

57. If `destroy` does *not* contain `False` then it must be true; so
   the document *has* been modified. So...
58. Creates a variable called `detour` because, well, we're taking a
   little detour from the command that the user has issued. The `detour`
   variable contains the results of `QMessageBox` (a function of
   `QDialog`, which you used as the main application window in the
   calculator app). 

   `QMessageBox` takes as its first argument (aside from `self` data,
   that is) the window title; in this case, "Hold your horses.". Its
   next argument is the text in the message box itself. And finally it
   requires the different types of buttons you want.

59. Starts an if-statement (note that you are still inside the else
   part of your initial if-statement, so this is a sub if-statement) to
   examine the contents of `detour`. The `detour` variable gets set by
   `QMessageBox`, so it contains one of three different possibilities: it
   is one of the three buttons that the user is presented with. The first
   option specifies that if `detour` contains `QMessageBox.Cancel`,
   then...
60. ...return True to the function that called `unSaved`. A response of
   True will tell any function that might otherwise destroy the user's
   data to *stop* or, as the button itself is labelled, to cancel any
   further action.

61. If `detour` does not contain a cancel signal, then perhaps it
   contains a `QMessageBox.No` signal...
62. ...in which case, this function will return a False signal to the
   function that requested `QMessageBox`, causing the function to
   continue in doing whatever the user has told it to do (open a new file
   without saving, or quitting the application) and destroy the unsaved
   data.
63. If `detour` does not contain either a cancel or a negative signal,
   then it must contain a `QMessageBox.Yes` signal...
64. ...and so `unSaved` calls upon a new function that we will create
   soon, called `saveFile`. This will let the user save the text data to
   a file on their computer.
65. The if-statements end, and technically so does the function.
   However, as an extra safeguard, just in case something goes horribly
   wrong and the `QMessageBox` does not intercept the user, we return
   True. Remember that a True signal sent to the function requesting
   `unSaved` will tell any function that might otherwise destroy the
   user's data to *stop*. The application might not let the user quit or
   get a new empty document, but at least their data is in tact. It's
   always a good idea to attempt to crash in the favour of your user.

Saving Files
------------

Saving to files is fairly simple in Python, and of course Qt itself
takes care of the heavy lifting such as calling up a save-file window
that is native to the operating system in use::

  66. ¬ def saveFile(self):
  67. ¬¬ if self.filename is False:
  68. ¬¬ ¬ self.filename = QFileDialog.getSaveFileName(self, 'Save File', os.path.expanduser('~'))
  69. ¬¬ else:
  70. ¬¬¬ f = open(self.filename, 'w')
  71. ¬¬¬ filedata = self.text.toPlainText()
  72. ¬¬¬ f.write(filedata)
  73. ¬¬¬ f.close()

66. Creates the new function `saveFile` and brings in, as you might
   expect, all the data about the surrounding class via `self`, since it
   would be inconvenient for a save-file window to be unaware of the file
   it was supposed to save.
67. Remember way in the beginning of the class when you created a
   variable called `filename` and set its contents to `False`? This line
   checks to see if the `filename` variable contains `False`. If it
   does...
68. ...then the filename is re-set to the result of the
   `getSaveFileName` function, which is a part of the `QFileDialog` class
   of Qt. As arguments, this function receives all of the usual `self`
   data, a string for the window titlebar, and the default location it
   should first go while prompting the user to save. On Linux and other
   Unixy operating systems, the commonly expected location is the user's
   home folder. On Windows it might be a user's Documents folder, but we
   will default to the user's home since that is a safe bet in either
   case. 

   Note that as a part of the arguments, you actually use the Python
   function `os.path.expanduser`. That kind of integration is just
   another bonus to using PyQt.

69. If the `filename` is not `False`, then it must already have been
   assigned a filename by the user, so the function will continue with
   saving...
70. A familiar incantation in Python, the next four lines run through a
   common file-save process. First, we assigned the variable `f` to the
   results of the Python function `open`, taking the filename and the
   code for "write" or "writable" to let Python know to open the file as
   a writable file rather than a read-only one.

71. Creates a new variable `filedata` to the results of the
   `toPlainText` function from the `QTextEdit` class. It is applied, of
   course, to the data contained in the `QTextEdit` widget, which you
   yourself defined as `text` earlier in the class.

72. Uses the Python `os` module's `write` to send the data contained in
   `f` into the variable `filedata`. You can think of it as a sort of
   pipeline from the text edit widget into the file on a harddrive.

73. Uses the Python `os` module's `close` function to close the
   pipeline.

New File
--------

Creating a new empty file is a simple three line function::

  74. ¬ def newFile(self):
  75. ¬¬ if not self.unSaved():
  76. ¬¬¬ self.text.clear()`

74. Creates the new `newFile` function, bringing in all `self` data.
75. This line calls the `unSaved` function...
76. ...unless `unSaved` returns a False signal, in which case `newFile`
   continues into the `QTextEdit` function `clear`, which, as you
   probably can guess, clears the text widget of any data.

If that seems deceptively simple, it is; a lot of the work is being
offloaded onto `unSaved`. But that's why we wrote `unSaved`, so it's a
good thing.

Opening Files
-------------

Opening a file is the exact opposite of saving a file. Rather than
writing data out from the text widget into a file, you simply read
data from a file and put it into the text widget::

  77. ¬ def openFile(self):
  78. ¬¬ filename = QFileDialog.getOpenFileName(self, 'Open File', os.path.expanduser("~"))
  79. ¬¬ f = open(filename, 'r')
  80. ¬¬ filedata = f.read()
  81. ¬¬ self.text.setText(filedata)
  82. ¬¬ f.close()

77. Creates the `openFile` function, bringing in the `self` data about
   the rest of the class.
78. Sets a local variable `filename` to the results of
   `getOpenFileName` from the `QFileDialog` class of PyQt. As with saving
   files, the arguments are the window title and the default location
   from which to start.
79. Assigns the variable `f` to the results of the Python function
   `open`, taking the filename and the code for "read" or "read-only".
80. Creates a new variable `filedata` to the results of Python's `read`
   function from the `os` module.
81. Uses the `setText` function from PyQt's `QTextEdit` class to set
   the contents of the text widget to the contents of `filedata`, which
   of course has just been populated with the contents of the file that
   the user has selected.
82. Uses the Python `os` module's `close` function to close access to
   the file that it has just read into the text widget.

Quit
----

Quitting is also largely handled by Qt. The only thing you must do is
prevent it from being too draconian by first calling the `unSaved`
function to make sure that no unsaved text is destroyed along with the
application window::

  83. ¬ def closeEvent(self, event):
  84. ¬¬ if self.unSaved():
  85. ¬¬¬ event.ignore()
  86. ¬¬ else:
  87. ¬¬¬ exit

83. Creates a new function `closeEvent` which brings in both `self`
   data as well as event data. Event data is something that Qt mostly
   handles, so this function is not arbitrarily named; it ties into Qt's
   event structure and lets you insert some extra code into the close-
   event process.
84. Starts an if-statement that calls the `unSaved` function. If
   `unSaved` returns True, then the quit process is intercepted by the
   `unSaved` warning dialog box.
86. Otherwise, the quit process continues as usual.

That finishes up the application. There are no more features we need
to take care of, so you can proceed to the main loop.

Main
----

The main loop, such as it is, starts with a `main` function. This
function is not within the `TextEdit`, but a function of its own::
    
  88. def main():
  89. ¬ app = QApplication(sys.argv)
  90. ¬ editor = TextEdit()
  91. ¬ sys.exit(app.exec_())

  92. if __name__ == '__main__':
  93. ¬ main()

88. Creates a new function called `main`. The only purpose of this
    function will be to launch and kill the application, so it needs no
    awareness of what the application contains. Everything inside the
    application is managed by `QMainWindow`; this function manages the
    window itself.
    89. Creates a new variable `app` and sets it to the results of
    `QApplication`, with any commandline arguments passed to it. The
    `QApplication` class does things like initializing the application
    with the user's default settings (fonts, mouse settings, and so on),
    colour settings, and so on.
90. Creates a variable called `editor` and places the `TextEdit` class
    into it.
91. Exits the application.

92. Creates a necessarily true condition...
93. ...and runs the function which runs the class which runs the
   application.

Save the source code and run the application, and you will find that
you now have a nice little text editor written in Python and Qt.

pyqtxt.py
---------

The source code for the text
editor::

  #!/usr/bin/env python
    
  # GNU All-Permissive License
  # Copying and distribution of this file, with or without modification,
  # are permitted in any medium without royalty provided the copyright
  # notice and this notice are preserved.  This file is offered as-is,
  # without any warranty.
  
  import sys
  import os
  from PyQt4.QtGui import *
  
  class TextEdit(QMainWindow):
  
  ¬ def __init__(self):
  ¬ ¬ super(TextEdit, self).__init__()
  ¬ ¬ #font = QFont("Courier", 11)
  ¬ ¬ #self.setFont(font)
  ¬ ¬ self.filename = False
  ¬ ¬ self.Ui()
  
  ¬ def Ui(self):
  ¬ ¬ newFile = QAction('New', self)
  ¬ ¬ openFile = QAction('Open', self)
  ¬ ¬ saveFile = QAction('Save', self)
  ¬ ¬ quitApp = QAction('Quit', self)
  
  ¬ ¬ copyText = QAction('Copy', self)
  ¬ ¬ pasteText = QAction('Yank', self)
  
  ¬ ¬ newFile.setShortcut('Ctrl+N')
  ¬ ¬ newFile.triggered.connect(self.newFile)
  ¬ ¬ openFile.setShortcut('Ctrl+O')
  ¬ ¬ openFile.triggered.connect(self.openFile)
  ¬ ¬ saveFile.setShortcut('Ctrl+S')
  ¬ ¬ saveFile.triggered.connect(self.saveFile)
  ¬ ¬ quitApp.setShortcut('Ctrl+Q')
  ¬ ¬ quitApp.triggered.connect(self.close)
  ¬ ¬ copyText.setShortcut('Ctrl+K')
  ¬ ¬ copyText.triggered.connect(self.copyFunc)
  ¬ ¬ pasteText.setShortcut('Ctrl+Y')
  ¬ ¬ pasteText.triggered.connect(self.pasteFunc)
  
  ¬ ¬ menubar = self.menuBar()
  ¬ ¬ menubar.setNativeMenuBar(True)
  
  ¬ ¬ menuFile = menubar.addMenu('&File')
  ¬ ¬ menuFile.addAction(newFile)
  ¬ ¬ menuFile.addAction(openFile)
  ¬ ¬ menuFile.addAction(saveFile)
  ¬ ¬ menuFile.addAction(quitApp)
  
  ¬ ¬ menuEdit = menubar.addMenu('&Edit')
  ¬ ¬ menuEdit.addAction(copyText)
  ¬ ¬ menuEdit.addAction(pasteText)
  
  ¬ ¬ self.text = QTextEdit(self)
  ¬ ¬ self.setCentralWidget(self.text)
  ¬ ¬ self.setMenuWidget(menubar)
  ¬ ¬ self.setMenuBar(menubar)
  ¬ ¬ self.setGeometry(200,200,480,320)
  ¬ ¬ self.setWindowTitle('TextEdit')
  ¬ ¬ self.show()
    
  ¬ def copyFunc(self):
  ¬ ¬ self.text.copy()
  
  ¬ def pasteFunc(self):
  ¬ ¬ self.text.paste()
  
  ¬ def unSaved(self):
  ¬ ¬ destroy = self.text.document().isModified()
  
  ¬ ¬ if destroy == False:
  ¬ ¬ ¬ return False
  ¬ ¬ else:
  ¬ ¬ ¬ detour = QMessageBox.question(self,
  ¬ ¬ ¬ ¬ ¬ ¬ ¬ "Hold your horses.",
  ¬ ¬ ¬ ¬ ¬ ¬ ¬ "File has unsaved changes. Save now?",
  ¬ ¬ ¬ ¬ ¬ ¬ ¬ QMessageBox.Yes|QMessageBox.No|
  ¬ ¬ ¬ ¬ ¬ ¬ ¬ QMessageBox.Cancel)
  ¬ ¬ ¬ if detour == QMessageBox.Cancel:
  ¬ ¬ ¬ ¬ return True
  ¬ ¬ ¬ elif detour == QMessageBox.No:
  ¬ ¬ ¬ ¬ return False
  ¬ ¬ ¬ elif detour == QMessageBox.Yes:
  ¬ ¬ ¬ ¬ return self.saveFile()
  
  ¬ ¬ return True
  
  ¬ def saveFile(self):
  ¬ ¬ if self.filename is False:
  ¬ ¬ ¬ self.filename = QFileDialog.getSaveFileName(self, 'Save File', os.path.expanduser('~'))
  ¬ ¬ else:
  ¬ ¬ ¬ f = open(self.filename, 'w')
  ¬ ¬ ¬ filedata = self.text.toPlainText()
  ¬ ¬ ¬ f.write(filedata)
  ¬ ¬ ¬ f.close()
  
  ¬ def newFile(self):
  ¬ ¬ if not self.unSaved():
  ¬ ¬ ¬ self.text.clear()
  
  ¬ def openFile(self):
  ¬ ¬ filename = QFileDialog.getOpenFileName(self, 'Open File', os.path.expanduser("~"))
  ¬ ¬ f = open(filename, 'r')
  ¬ ¬ filedata = f.read()
  ¬ ¬ self.text.setText(filedata)
  ¬ ¬ f.close()
  
  ¬ def closeEvent(self, event):
  ¬ ¬ if self.unSaved():
  ¬ ¬ ¬ event.ignore()
  ¬ ¬ else:
  ¬ ¬ ¬ exit
  
  def main():
  ¬ app = QApplication(sys.argv)
  ¬ editor = TextEdit()
  ¬ sys.exit(app.exec_())
  
  if __name__ == '__main__':
  ¬ main()
