Python
======

Now that you are familiar with the basic constructs of programming,
it's time to try out a programming language with modern conveniences,
like `Python`. 

Because BASH is not an object-oriented language, its ability to
produce asynchronous applications is limited. It is a scripting
language meant to enable people to script common commands. There are
few frameworks for it; people don't write libraries to help you play
sounds and display graphics within BASH . There isn't much emphasis
upon concurrent events and output and input in BASH , so even if you
were satisfied with building your own game framework for BASH it would
be very difficult to achieve any sense of real-time game play. Python
, on the other hand, has libraries and modules available to it so that
you can use it for everything from games to server monitoring.

.. note:: On the scale of "real" programming languages, Python sits
	  more or less in the middle between scripting languages like
	  BASH and Perl and very advanced languages like `C` and
	  `C++`. Don't think that just because you're not learning `C`,
	  however, that you're not learning to programme. All of these
	  languages are useful and important to a programmer, and in
	  fact even the most advanced triple-A game titles are known
	  to use basic scripting languages like `Lua`. And all of them
	  use game frameworks and engines, so learning to work with a
	  Python game library will get you ready to interface with
	  more advanced ones later on.

Getting Started with Python
---------------------------

Python is a very popular language because it is flexible, relatively
simple to learn, and yet very powerful. Due to its rampant popularity,
there are tutorials and tips and code samples all over the internet,
so don't limit yourself to this book while you're getting to know the
language.

As with everything in this book, Python is cross-platform.  Even
moreso than BASH , it is trivial to install Python on Windows, and as
with BASH , Python is included by default on both Mac OS and
Linux.

There are two ways to interface with Python , the language.

* A text editor, into which you type Python statements, just as you
  did with BASH. You can use something like Kate to type your Python
  programme, but there is a specialized kind of application called an
  IDE (Integrated Development Environment), which helps cut down on
  repetitive tasks in programming and usually have some convenient
  multi-window layouts that help you multi-task and stay organized. At
  this stage in your career, an IDE is not strictly necessary but we'll
  learn a little about `SPE`, a popular Python IDE, later in this book.
* Through an interactive Python shell, called `IDLE`, which looks
  deceptively like a Linux terminal but is dedicated to Python commands.
  The advantage of dipping down into a Python shell is that it provides
  immediate feedback, making it easy to debug Python statements without
  having to run your entire programme.

Starting simple, let's launch a Python shell and see how the language
works at its most basic.

#. Launch a terminal by clicking on the K-menu in the bottom left
   corner of the Slax desktop and selecting Konsole .
#. In Konsole , type in `python`. This launches an instance of IDLE,
   the interactive Python shell, within your Konsole window.

   .. tip:: It's important to understand that IDLE is not a part of
	    your terminal; it is its own application that just
	    happens, in this case, to run inside of a terminal. IDLE
	    can run inside of other windows, and in fact does, so
	    don't get the IDLE confused with a BASH shell.

#. You are now at a python prompt. It looks like this: `>>>`

Python Variables
----------------

You know how to create and use variables in BASH , so now let's try
out some variables in Python. In IDLE, type this.

.. code-block:: python

  >>> DESSERT = 'cupcake'

Just as you did in BASH, you have now instantiated the variable
"DESSERT" and placed the string "cupcake" into it. To get a value back
from a variable, you simply type it in again.

.. code-block:: python
    
  >>> DESSERT
  'cupcake'

Or you can use it within python functions. The most basic python
function is `print()`.

.. code-block:: python

  >>> print(DESSERT)
  cupcake

Of course you can also create variables that are numbers.

.. code-block:: python

  >>> HEALTH = 10
  >>> print(HEALTH)
  10
  >>> print(HEALTH + 2)
  12

You're an old variables pro by now; you get the idea.

Modules, Classes, and Methods
-----------------------------

In BASH , we used pipes to filter the contents of our variables
through other programmes::
  `# echo $TITLE | tr [:upper:] [:lower:]`

That process is somewhat unique to BASH because BASH generally works
on data *streams*. BASH , generally, expects a constant flow of input
and output, and excels at processing, filtering, and re-directing that
data. There are exceptions, but even in the most skilled hands, BASH
is still "just" a scripting language. 

Python and other object oriented programming languages are able to
see data as chunks of useful information which can be placed into RAM
for processing and re- use whenever needed. So in Python you won't run
variables through filters in quite the same way, although the
principle is similar.

In Python and many other languages, you don't call upon external
applications through which you filter a variable, you instead add on
modules that provide boosts to Python's capabilities. A "module" is
like an application that works only within the programming language;
it's like a plugin of sorts. Or, if you're really *really* into
gaming, you could think of it as a skill that you're activating in
your character's skill tree.

Modules contain different functions, each of which can perform a
different task. They are frequently used to perform some task upon a
variable.

The syntax is often *the name of the module* followed by a dot,
followed by *some function*, followed by the variable (in parentheses)
that you wish to process.

For instance, the `str` class in Python operates on strings (the fancy
technical word for a series of letters and/numbers in some
circumstances), and features a few "methods" such as `capitalize` to
capitalize the first letter of a string, `upper` to capitalize all
letters in a string, and so on. Try typing something like this into
your Python shell.

.. code-block:: python

  >>> ANIMAL = str.upper("llama")
  >>> print(ANIMAL)
  LLAMA

To see all of the functions in a module, use the `help` class within
your Python shell.

.. code-block:: python

  >>> help(str)

This will give you a brief, probably cryptic, description of what the
class does and how to use it, plus all of the methods contained in
that class. Try a few of the methods now to get the hang of it. If you
don't understand how to use everything that the help screen mentions,
don't worry about it. Help screens are frequently puzzling if you're
not familiar with the topic because help screens are generally
intended as quick-references rather than as tutorials for beginners.
But try a few tricks anyway just to see what happens.

Of course, to use a help screen at all requires that you know what
modules exist in the first place. The more you use Python, the more
you'll learn about available modules, but you can also look at
`docs.python.org <http://docs.python.org>`_ for a list of builtin
modules and what they do. In practise, however, you'll probably learn
about modules as the need for them arises; for instance, if you find
yourself in need of a date or timestamp then you'd come across the
`time` module; particularly `time.localtime()`.  How will you come
across modules? Well, the same way you find anything; you'll research
online, you'll ask friends, you'll read about one in a tech blog or
magazine, or you'll stumble across one on accident. Trust me, once you
start programming, you just start learning stuff because you start to
know what to look for.

There are many standard Python functions that you can use on data, not
just `print` and `str` and `help`. For example, concatenation the
contents of the variables `ANIMAL` and `DESSERT`.

.. code-block:: python

  >>> ANIMAL + " " + DESSERT
  'LLAMA cupcake'

Or print just a slice (pun intended) of `DESSERT`.

.. code-block:: python

  >>> DESSERT[3:7]
  'cake'

Or calculate a hash from the contents of `ANIMAL`.

.. code-block:: python

  >>> hash(ANIMAL)
  7937225788042750390

Look through the Python documentation and see if you can experiment
with a few inbuilt functions.


Importing Modules
-----------------

There are three types of modules in Python.

* Some modules are built into Python. You get them all when you
  install (or more likely happen to have on your system) Python. As you
  might expect, they are the most basic functions of the Python
  language.
* Others are not considered built-in modules but are quietly installed
  along with Python on most systems. They are not pre-loaded into Python
  unless explicitly imported into the code by the programmer (that would
  be *you*).
* Additional modules can be found online, downloaded and installed by
  you on your system, and used in your programming projects.

There are many modules available for Python, including one that we
will use, called PyGame . But first let's practise with Python.

Your First Python Programme
---------------------------

Your first python programme will demonstrate how to start using a
programming language even when you have no experience with that
language. The steps are the same, no matter what language you're
using.

#. Identify what you want to accomplish with your programme.
#. Determine how you will accomplish it in pseudo code.
#. Research available resources within the programming language you
   will be using.
#. Programme!

In this example, we will do something simple; we will reimplement the
UNIX ls command, that is, something to list the contents of a
directory on our system. Now that we know what we want to do, try
thinking about how to implement it.

* Get the location of the directory to list.
* List the contents of the directory for the user.

The first step, then, is to determine what built-in function or
additional module would do these two tasks. The most efficient way to
do that is to search the internet, or look at Python documentation.
Eventually, you would come across the `os` module. 

The `os` module provides an interface between Python and the operating
system it is running upon. Since this is our first time out with
Python, we'll try everything in the Python shell first, and then type
it into a text editor and run in outside of IDLE.

Try reading the help page for `os`::
  >>> help(os)
        Traceback (most recent call last):
        File "<stdin>", line 1, in
        <module>
        NameError: name 'os' is not defined

This error message, a `NameError`, is telling you that the Python
shell believes that the module you have tried to invoke does not
exist. And in fact, it doesn't. At least, not in the currently running
instance of IDLE. Only the basic components of Python are available
until you import them, so import the `os` module now::
  >>> import os
  >>> help(os)

The help pages for `os` are quite long and a little cryptic, but you
can search by pressing the `/` key on your keyboard and typing in your
search term. In this case, search for "dir", as in "directory"; use `n`
to proceed to the next match if the first is not helpful.

.. tip:: Whenever you are trying to use a new module and are unsure
	 how to implement it, either search for online help or find
	 some example code that uses the module.

Eventually, if you do enough searching and reading, you would find
that there are two useful functions in the `os` module: `getcwd()` and
`listdir()`. Let's try `getcwd()` first::

  >>> os.getcwd()
  '/home/root'

Notice that when invoking `getcwd`, you prefaced it with `os.`; this
is because Python was never told explicitly where to find `getcwd`.
You can avoid having to specify what module a function is located in
by importing that function explicitly, instead of just importing the
entire module. In other words, if you already know what function or
functions you want to use from a module, use `from os import getcwd`
instead of `import os`. Otherwise, just import the entire module with
`import os` but provide the module name before each function. 

The function `getcwd` did work as we'd hoped; it returned the
underlying current directory, so that's good. Now let's try listing
the directory::
  >>> os.listdir()
    	Traceback (most recent call last):
    	  File "<stdin>", line 1, in <module>
    	  TypeError: listdir() takes exactly 1 argument (0 given)

This error message is teling you, quite clearly, that `listdir`takes
exactly 1 argument, meaning that Python doesn't know which directory
you want listed. In other words, we need to get the results of
`getcwd` into `listdir`.

As with most languages, passing information to and from the programme
is done with a variable::
  >>> LOCATE = os.getcwd()
  >>> os.listdir(LOCATE)

And our programme is, more or less, complete. All we need to do now is
type it into a file that we can save and make executable so that we
can run it like a real application.

Executable Python
-----------------

Open up Kate or another text editor of your choice and save an empty
file as `ls.py`. Once saved, Kate will use the `.py` extension as a
guide for syntax highlighting, making your programme easier to read
and debug. Enter our Python programme::
  #!/usr/bin/env python
    
  from os import getcwd
  from os import listdir

  LOCATE = getcwd()
  LS = listdir(LOCATE)
  print(LS)

Make this new programme executable and then you can run it from any
Linux terminal as if though it was just another BASH command::
    
  # chmod +x ls.py

As with any application that has not been properly installed, you will
need to give your Linux system the full path to ls.py::
    
  # /home/root/ls.py

Our first Python programme isn't terribly good, especially compared to
the actual Linux ls command, but you now have a little experience with
the process of fleshing out a programe in Python. We could spend
another chapter on improving the humble little ls.py application, but
there are far more exciting things that Python can do, so let's just
jump right into `PyGame`.

ls.py
-----

The source
code::
   #!/usr/bin/env python

   from os import getcwd
   from os import listdir

   LOCATE = getcwd()
   LS = listdir(LOCATE)
   print(LS)
