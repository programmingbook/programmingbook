Object Oriented Dice via Python
===============================

Pygame has the potential of producing very complex games, and object
oriented programming is especially relevant to game programming since
so much in a video games are just instances of a few master
classes. For instance, you only have to play a video game for a few
levels before you catch on that there are only so many villain
objects, which are then spawned over and over again in order to
populate the bad guy's hideout with dangerous henchman. Sometimes the
villains will have a different colour shirt, or a different weapon, or
a slightly different build, but you can still see that they are all
instances of just one or two sources.

Object oriented programming brings that, and more, into your code; you
can write an "object" which you can use later to produce useful tasks
or actions in your programme.

So that you really get a feel for why object oriented code is unique,
let's step away from Pygame for a chapter and do one more
implementation of our dice game, this time as a true object oriented
programme.

First Iteration
---------------

By now, we know the logic and flow of the game; assign two variables
one random number each, compare the numbers, and pronounce the
winner. Simple enough.

In Python, you could do a first draft like this::
  1.  #!/usr/bin/env python

  2.  import random

  3.  class dice():
  4.  ¬ def __init__(self):
  5.  ¬¬ self.player=random.randrange(6)
  6.  ¬¬ self.computer=random.randrange(6)

1. The initial line telling the interpreter what programme to use to
   execute the code in this file.
2. Python has a built in (pseudo) random number generator. To see this
   in action, open a Python shell and type this::
     >>> import random
     >>> random.randrange(100)
     
   You will get a random number back, somewhere in the range of 0
   to 100.

3. Creates a "class" called `dice`, which will serve as the model for
   any instance of a dice we need to call later in the application.

4. Creates an `__init__` function within `dice`. This function will
   run any time `dice()` is used later in the application.

   As an argument, `__init__` takes its own data; that is, its
   `self`. This will allow it to gives itself attributes that we can
   use as parts of each dice we spawn during the game.

5. Creates a variable called `self.player` and assigns to it a random number
   between 0 and 6.

6. Creates a variable called `self.computer` and assigns to it a
   random number between 0 and 6.

That establishes a `dice` object, meaning that when we as the
programmer tell the application to roll the dice, Python uses the
`dice` class to pick two random numbers and assign them to two
variables. That sums up the work we need our virtual "dice" to do, but
since this is all virtual we may as well also let it do a little bit
of logic for us::
  7.  ¬ def compare(self):
  8.  ¬¬ if self.player == self.computer:
  9.  ¬¬¬ print("you win")
  10. ¬¬¬ sys.exit()
  11. ¬¬ else:
  12. ¬¬¬ print ("you lose")
  13. ¬¬¬ sys.exit()

7. This line creates a new function called `compare` inside of the
   `dice` class (notice that it is indented once, so as to be within
   the `dice` class but *not* within the `__init__` function. It
   brings in all `self` data, because it will use the self.player and
   self.computer variables from the `__init__` function.

8. Sets up an if/then statement comparing `self.player` to
   `self.computer`.

9. If they are equal, then Python prints "you win" and...

10. ...quits.

11. Otherwise, Python skips to the `else` statement.

12. We print "you lose" if the values are not equal.

13. And then we quit.

This being Python, we do not close the if-statement, we simply
continue coding, but one indentation level back (in this case, that
would be no indendation)::
  14. mydice=dice()
  15. mydice.compare()

14. Just as in the BASH version of the procedural version of
    `dice.sh`, the `dice` class is not actually executed when the
    programme is run. In fact, you could save the programme now and
    run it; it will not fail, but then again, neither will actually
    *do* anything.
    
    To get the application to do something useful, we create a
    variable called `mydice` and assign it an instance of `dice`. Upon
    doing so, the dice has, essentially, been rolled and the `player`
    and `computer` variables get a random number each.

15. Having obtained numbers for our two key variables, we must now
    compare those numbers. To do that, all we need do is run the
    `compare` function we wrote into the `dice` object. This function
    does the comparison, and announces the winner.

Breaking it Down
----------------

In order to understand the flow of data within this simple object
oriented programme, let's try deconstructing it a little.

First, try removing the `self` argument from the functions::

  #!/usr/bin/env python

  import random

  class dice():
  ¬ def __init__():
  ¬¬ self.player=random.randrange(6)
  ¬¬ self.computer=random.randrange(6)
  ¬ def compare():
  ¬¬ if self.player == self.computer:
  ¬¬¬ print("you win")
  ¬¬¬ sys.exit()
  ¬¬ else:
  ¬¬¬ print ("you lose")
  ¬¬¬ sys.exit()

  mydice=dice()
  mydice.compare()

If you run that code, you should get this error::
  Traceback (most recent call last):
     File "./dice.py", line 19, in <module>
     mydice=dice()
  TypeError: __init__() takes no arguments (1 given)

As you can see, when a class is invoked in a programme, it inherently
receives data about itself. If we do not allow for this, then a
function receives `self`-ful information but does not know what to do
with it.

The idea of `self` is important in object oriented programming because
a function needs to know what object it is operating upon.

The fact that we use `self` is really just a convention. Sometimes you
will see the term `this` used, but in Python it is most often
`self`. Fact is, however, you could theoretically use anything. Try a
find/replace in your text editor::
  #!/usr/bin/env python

  import random

  class dice():
  ¬ def __init__(penguin):
  ¬¬ penguin.player=random.randrange(6)
  ¬¬ penguin.computer=random.randrange(6)
  ¬ def compare():
  ¬¬ if penguin.player == penguin.computer:
  ¬¬¬ print("you win")
  ¬¬¬ sys.exit()
  ¬¬ else:
  ¬¬¬ print ("you lose")
  ¬¬¬ sys.exit()

  mydice=dice()
  mydice.compare()

If you run that code, with `self` replaced with, for instance,
`penguin`, then you will see that it does indeed run as
expected. There is nothing magickal about `self`, it's just a
conventional word to serve as a marker to indicate what object it is a
function is operating upon. If you exclude it, your programme will not
know where to direct its functional code.

Now try adding an additional function to the `dice` object. Make it do
something simple, and then call it at the end of your programme::
  #!/usr/bin/env python

  import random

  class dice():
  ¬ def __init__(self):
  ¬¬ self.player=random.randrange(6)
  ¬¬ self.computer=random.randrange(6)
  ¬ def compare():
  ¬¬ if self.player == self.computer:
  ¬¬¬ print("you win")
  ¬¬¬ sys.exit()
  ¬¬ else:
  ¬¬¬ print ("you lose")
  ¬¬¬ sys.exit()
  ¬ def source(self):
  ¬¬ print("May the source be with you.")

  mydice=dice()
  mydice.compare()
  mydice.source()

Run that code, and notice that everything works as expected. But now
let's pass some data into our new `source` function::
  [...]
  ¬ def source(self,message):
  ¬¬ print(message)

  mydice=dice()
  mydice.compare()
  message="Programmers are tools for converting caffeine into code."
  mydice.source(message)

Now you have discovered how the flow of data works in an object
oriented environment, so you can probably see to further abstract, for
instance, who defines the contents of the `message` variable so that
the *user* can define what message is printed at the end.

Even more useful, however, would be to allow a user to define how many
sides are on the dice that they are about to roll. Currently, your
application has that value hardcoded in as 6, but why not let the user
decide?

Second Iteration
----------------

You might recall from the BASH chapters that code such 
as::
  #!/bin/bash
  echo "$1"

would echo any argument that a user types in after the command to run
the application; for instance::
  # ./echo.sh foo
  foo

In Python, it's slightly more complex, but still similar. In order to
read in an argument from the command line executing the Python
application, you use the `sys` module::
  #!/usr/bin/env python
  import sys

  print(sys.argv[1])
  ARG=sys.argv[1]
  print(ARG)

As you can probably tell, `sys.argv` is the function that looks at
what was typed on the command line. Like BASH, it uses spaces as a
delimiter and assigns a numerical value to each string given. 0 is the
command itself, 1 is the first argument, 2 the second, and so on.

Now let's leverage this new trick so we can really take advantage of
the object oriented nature of our new dice programme::
  #!/usr/bin/env python

  import random
  import sys

  class dice():
  ¬ def __init__(self,sides):
  ¬¬ self.player=random.randrange(sides)
  ¬¬ self.computer=random.randrange(sides)
  ¬¬ print(self.player)
  ¬¬ print(self.computer)
  ¬ def compare(self):
  ¬¬ if self.player == self.computer:
  ¬¬¬ print("you win")
  ¬¬¬ sys.exit()
  ¬¬ else:
  ¬¬¬ print ("you lose")
  ¬¬¬ sys.exit()
  
  sides=sys.argv[1]

  mydice=dice(sides)
  mydice.compare()

In theory, this change should make it so that running `dice.py` along
with some number will define how many sides the dice has; in other
words, it determines the upward boundary for the range of random
numbers provided by Python. 

However, if you actually run this code, it will fail with this
message::
  # ./dice.py 21
  Traceback (most recent call last):
  File "./dice.py", line 22, in <module>
    mydice=dice(sides)
  File "./dice.py", line 8, in __init__
    self.player=random.randrange(sides)
  File "/usr/lib64/python2.7/random.py", line 186, in randrange
    raise ValueError, "non-integer arg 1 for randrange()"
    ValueError: non-integer arg 1 for randrange()

The final line of this error message is the most useful: it states
that the argument given is a non-integer. In other words, `random`
wants to see an integer (a number) but Python is reading the argument
n the command line as a *string*.

Converting the string into a proper number is easy, but first let's
see proof of what's happening. Modify your code temporarily to this::
  [...]
  sides=sys.argv[1]
  print(type(sides))
  #mydice=dice(sides)
  #mydice.compare()

Running this new debugging version of your code will reveal what
Python is seeing::
  # ./dice.py 21
  <type 'str'>

The `type` function identifies your "number" as a string rather than
an integer. Let's fix it::
  [...]
  sides=int(sys.argv[1])
  mydice=dice(sides)
  mydice.compare()

Now we are passing the string `21` (or whatever number you provide on
the command line) through the `int` function, converting it into a
proper number rather than a character.

Run the programme, and you will see that it runs as expected.

Exceptions
----------

This application is not very user friendly. If a user issues the
command to run it and does not know to include a number for how many
sides the dice should have, it will fail. Instead of just letting it
crash and burn, let's add a helpful message.

In BASH, we used if/then statements to validate data; for example, if a user
entered `y` then we repeated the game, and if the user entered `n`
then we ended. To some degree, we can use if-statements in Python for
that, and in fact in `dice.py` we do use an if-statement in the
`compare` function.

However, there is a function in Python built especially for catching
errors, or *exceptions* as they are called in programming. The
*try* statement in Python allows for failure, and instead of failing
publically and ungracefully, stifles the error and tries something
else. It's a lot like an if/then statement that we might use in BASH,
but especially well suited for error signals.

Change the `sides=int(sys.argv[1])` line to this block of 
code::
  try:
  ¬ sides=int(sys.argv[1])
  except:
  ¬ print("Please provide how many sides you want your dice to be.")
  ¬ print("For example: ./dice.py 21")
  ¬ sys.exit()

Now try executing the code, first without a number for sides, and then
with::
  # ./dice.py
  Please provide how many sides you want your dice to be.
  For example: ./dice.py 21
  # ./dice.py 42
  15
  36
  you lose

Now our application is user friendly!

Homework
--------

The programme runs only once. Can you figure out a way to make it
offer to repeat rather than just quitting?

dice.py
-------

The complete source 
code::
  #!/usr/bin/env python

  import random
  import sys

  class dice():
  ¬ def __init__(self,sides):
  ¬¬ self.player=random.randrange(sides)
  ¬¬ self.computer=random.randrange(sides)
  ¬¬ print(self.player)
  ¬¬ print(self.computer)
  ¬ def compare(self):
  ¬¬ if self.player == self.computer:
  ¬¬¬ print("you win")
  ¬¬¬ sys.exit()
  ¬¬ else:
  ¬¬¬ print ("you lose")
  ¬¬¬ sys.exit()

  try:
  ¬ sides=int(sys.argv[1])
  except:
  ¬ print("Please provide how many sides you want your dice to be.")
  ¬ print("For example: ./dice.py 21")
  ¬ sys.exit()
  
  mydice=dice(sides)
  mydice.compare()
