Reflection
==========

Long ago, fledgling web designers used to go to any page on the
internet they fancied, looked at the HTML4 code, and literally learnt
valuable skills from simple reverse engineering.

And then HTML5 came along, with its separated CSS stylesheets, new tag
set, and its content-over-style (or is that style-over-content?)
philosophy. In many cases, it was nearly impossible to just reverse
engineer because it was impossible to understand what all the CSS
rules were doing wthout actually *understanding* how the flow and
hierarchy of CSS worked.

Now, go read over the main loop of the PyQt Text Editor you
built. Let's face it: a non-programmer would hardly believe *that*
could possibly be the "main" executable portion of any useful
software. Just look at it; it creates two variables to represent the
application as a running process, and then it exits. 

How can three lines of code be the main loop of an application?

And that, dear reader, is why programming is so danged confusing to
the uninitiated. To the casual observer, it appears illogical and
cryptic. It takes real knowledge to unravel what the code is saying,
much less how it all works together. 

But don't let that fool you. You are not the uninitiated! You have
gone through lessons in BASH, Python, and PyQt. You have created
functions, you have used third party methods and libraries, you have
experienced Linux, IDE's, and frameworks. You know that programmes are
not written from inside the main loop out.

Someone might ask you some time what programming language you
know. 

Tell them you don't know any programming language. 

Tell them that you know the *language of programming*.
