Rolodex.sh
==========

BASH is many things, one of which is a basic and powerful interface
between the human at the keyboard and the computer behind it. Let's
try a really basic program that, in theory, could even be helpful in
every day life. After all, that's why we're programming, right? to
make life easier.

In this chapter, you will programme a note-taking application which
will allow a user to type in notes and save them as files. When the
application is launched, the user will have the option of either
viewing existing notes, or creating a new one.

I'm calling my application Rolodex because it sounds retro, and what's
more retro than a note-taking application with only a text-based
interface?

#!/bin/sh
---------

First, let's get organized. Make yourself a code directory and change
into that directory::
  # mkdir ~/code
  # cd ~/code

You've just created and then "opened" a folder, except you've done it
in the terminal, in plain text. So we're now "in" the code folder.
Don't believe me? Type `pwd` and see what it says.

.. note:: For the new terminal user, the commands `pwd`, `cd`, and
          `ls` are the most important commands in the world.

          * `pwd` tells you where you are. If you ever get lost in
            your terminal, first type `pwd` to find out *where* you are.
          * `ls` lists all the files in your current location.
          * `cd` changes directory; it's how you move around in your
            terminal. So if somehow you got out of your code
            directory, then you can always type `cd ~/code` and you'll
            be placed promptly back in the code directory. Or you can
            just type `cd` and you'll be moved back home, and then you
            can use `ls` and `cd` to get back into your code
            directory.

#. Now let's start our first code project. We will use echo statements
   and redirection to build our first code project; later in the book we
   will use a text editor or Integrated Development Environment
   (IDE). 

   We first need to declare that the source code we are building is
   written in the BASH scripting language::
     # echo "#!/bin/bash" >> rolodex.sh

   There's probably a fancy programmer's term for that, but I don't
   know what it is. What this initial line does, however, is declare
   what kind of document we have. The computer looks at that initial
   line and sees that we'd like to use a UNIX *sh*ell to interpret the
   contents of the file as opposed to, say, Python or Java or Ruby or
   any number of other things that could try to interpret and run a
   file full of source code.

   So the `#!/bin/sh` establishes that the
   default shell ( sh ), located in the `/bin` directory, should be
   used to run this (currently empty) program.

.. note:: Are you curious how Linux manages to just look at the first
	  line of a file for this important information? Well, it
	  could use the GNU application `cat` (it doesn't, exactly,
	  but it's analogous), which has the job of
	  spitting the contents of a file into the standard
	  output. You can try this::
            # cat rolodex.sh
    	    #!/bin/sh

	  You could also use the `head` command, which gives you the option of
	  how many lines of text you want to see. In this case, it would be
	  redundant since there is only one line of text so far, but it's still
	  a handy command to know about::
            # head -n1 rolodex.sh
    	    #!/bin/sh

Pseudo Code
-----------

The thought process of programming is sometimes a little backward,
because you have to account for things that are entirely hidden from
the user. However, the first step usually is to imagine your
application from a user's perspective so that you have an ideal target
to aim for.

Defining how your programme will work is often done in "pseudo code",
meaning that you should write out the steps of your programme in
code-like phrases using code-like logic, and then use that as a guide
when you're writing the actual code.

This practise is invaluable because code logic (lots of if/then
decisions, testing for certain values, filtering data, and so on) is
universal. If you can pseudo code something, then it's really only a
matter of syntax in order to get it into actual code that something
like BASH or Python or Ruby or Java (and so on) can actually run.

For this note-taking application, I envision something a little like
this:

* Rolodex is launched.
* View existing note? or Create New Note?
* If view existing note, then GET input. Show the $INPUT note.
* If create new note, then GET input.
* Save $INPUT to a file.
* Quit.

That is, more or less, the application. There will probably be
additions and changes once we figure out the requirements of the
programming language, but over-all that encompasses the flow of the
code.

So now let's start hacking.

Main Menu
---------

I figure the first thing our users should sees are some options on
what they want to do. In a graphical application, this would probably
take the form of some welcome screen or workspace window, but in this
case we haven't exactly got those at our disposal. Instead, we'll ask
the user for responses to pre-set prompts.

#. First, we need to know whether the user wishes to view an existing
   note or create a new one, so let's just ask them::
     # echo 'echo "Welcome to Rolodex." >> rolodex.sh
     # echo 'echo "Select a note to view or enter NEW to create a new one." >> rolodex.sh

   You recognize the `echo` command, of course. In this step, you are
   echoing lines of code into `rolodex.sh`, telling the shell to echo
   questions back to the user.

#. Test the application, such as it is, by executing this command::
     # chmod +x ./rolodex.sh
     # ./rolodex.sh
     Welcome to Rolodex
     Select a note to view or enter NEW to create a new one.

   In the first command, you are *changing mode* of the `rolodex.sh`
   programme in order to allow it to be executed (+x) as an application.
   This is a security measure, to make sure that random data files are
   not accidentally run as an application instead of seen as just data
   files. 
   
   The second command runs the file `rolodex.sh` as an
   application. Normally the only files run as applications are the ones
   found in your application path (which you can view by echoing the
   value of the $PATH variable. Try it!) but by preceeding it with a dot-
   slash, you are telling the shell to execute the file `rolodex.sh` in
   the current directory as an application.

#. Now that we've asked the user a question, we should obviously
   capture their response. You already know how to do this with the read
   command::
     # echo 'read SHOW' >> rolodex.sh

   Here we've ceated a variable called SHOW, where we will store the
   user's response. It wouldn't be a bad idea to test the application
   again, even though it still isn't doing much::
     # ./rolodex.sh

If / Then
---------

At this point, we have a decision to make; either the user has entered
a note to view, or the user has requested to create a new one. Our
application must determine which. 

The easiest and most traditional
method of deciding between two options is an if/then statement. You
already know how to *test* for certain conditions, so all we need to
do is wrap a test in an if/then statement. 

Before we dive in, let's
take a moment to figure out what exactly we are testing.

* If $SHOW == [some existing note], then show the user the note.
* If $SHOW == "NEW", then start creating a new note.

That seems simple enough, but if you think it through then you'll see
a problem. The problem is that it's nearly impossible to determine if
the user has entered an existing note. How will BASH know whether or
not a note exists?

Therefore, the easier way to determine which path
our application should take is by testing whether or not the keyword
"NEW" has been entered. In BASH , two equal signs (==) means "equal
to" and an exclamation mark preceding a single equal sign (!=) means "
*not* equal to".

Knowing this, the only further information is the
syntax of a BASH if/then statment.

#. This tests to see if the value of $SHOW is *not* equal to NEW::
     # echo "if [ X$SHOW != XNEW ]" >> rolodex.sh

#. Following the logic, we know that if $SHOW is *not* NEW, then it
   must be some existing file. Therefore, we can tell BASH to::
     # echo 'then echo $SHOW || echo "File not found."'

   This enables the shell to do what comes naturally to it; it will
   search the current directory for a file with the name of whatever is
   contained in the $SHOW variable. Ideally, the shell will find a file
   with the name that the user provides, and cat its contents out into
   the terminal for them.

   Realistically, there's a possibility that the user will mistype or
   will type in nonsense or type in a non-existant filename. To smooth
   this over a little, I snuck in an "or" clause, represented by the
   double-pipe, which tells the shell that if it's unable to perform
   the cat command then it will echo "File not found" to the
   user. This is just a little more user-friendly than just exiting.

#. At this point, if the user just wants to see an existing note, the
   application is complete. We ask for a file name, we get the file name,
   determine if the file name is *not* NEW, and then show the user the
   contents of the file they have specified. 

   This leaves only the NEW note possibility. To get out of the *if*
   side of the if/then statement and into the *then* side, we echo::
     # echo 'else' >> rolodex.sh

   As you might expect, "else" is the keyword that tells BASH that we are
   finished with the possibility that $SHOW is not equal to NEW. Now we
   enter the possible universe in which $SHOW *is* equal to NEW.

#. In order to establish a new note and save that note to a file, we
   need to capture input from the user that represents the file into
   which the note will be saved, and the contents of the note itself. 

   You already know how to capture input, so let's just ask the user
   for some more information::
     # echo 'echo "Enter a name for your new note. No Spaces!"' >> rolodex.sh
     # echo 'read TITLE'

   We now have the desired file name of a new note in $TITLE.

#. Now let's capture the contents of the note::
     # echo 'echo "Enter your note. Press RETURN to end text entry and save the note."' >> rolodex.sh
     # echo 'read CONTENT'

   We now have the contents of the note in $CONTENT.

#. Now that we have all the data we need from the user, we can dump it
   all into a file and end our programme. Once again, you already know
   how to dump data from a variable into a file, and this is no
   exception::
     # echo 'echo $TITLE > $TITLE.dex

   This command will tell the shell to echo the data in $TITLE into a new
   file of the same name, along with the file extension *.dex*, which is
   a file extension I made up just to differentiate our note files from
   other files.

#. Now we will dump the data in $CONTENT into the file we have just
   created. The one new concept here is the usage of curly braces, which
   "protects" the data from being misinterpreted (which can happen when
   there are spaces in the data, since we use spaces to separate normal
   things like commands from the file names they are supposed to work
   upon, and so on)::
     # echo 'echo ${CONTENT} >> $TITLE.dex

#. All tasks are complete, so all that is left is to close the
   application. Normally, the application would just end on its own,
   except that in this case we still have an open if/then statement. So
   we will close the if/then statement and bid the user farewell just to
   confirm that all tasks are complete::
     # echo 'fi' >> rolodex.sh
     # echo 'echo "Good bye!"'

Homework
--------

Your first programme is complete. You can now test it by running it::
  # ./rolodex.sh

It is not a terribly robust application, but it is functional and does
exactly what we set out to create, all in under just 20 lines of code.
And hopefully, you actually understand *why* it works. 

Since you have finished it, take time to really test it out. It might
be fun to try the application and watch it actually, especially since
you programmed it almost all by yourself, but a good bug-tester feels
happiest when an application crashes. So try to confuse the
application (it won't be hard to do) and try improving it.

Rolodex.sh
----------

The source 
code::
  #!/bin/bash
  # GNU All-Permissive License
  # Copying and distribution of this file, with or without modification,
  # are permitted in any medium without royalty provided the copyright
  # notice and this notice are preserved.  This file is offered as-is,
  # without any warranty.
  
  echo "Welcome to Rolodex"
  echo "------------------"
  echo "Select a note to view"
  echo " or enter NEW to create a new one."
  
  
  ###### wildcards
  for i in *.dex
  ## for loop
  ## variables
  do echo $(basename $i .dex) 
  done
  echo "NEW"
  ###########
  
  
  ## input
  echo "What note do you wish to view?"
  read SHOW
  
  
  ## tr processing
  CHOICE=$(echo $SHOW | tr [:lower:] [:upper:])
  ## test
  if [ X$CHOICE != XNEW ]
  then cat $(basename $SHOW .dex).dex || echo "File Not Found." && exit 1
  else
  echo "Enter a name for your new note. No Spaces!"
  read TITLE
  echo "Enter content of note."
  read CONTENT
  
  
  ## re-direction
  echo $(basename $TITLE .dex) > $TITLE.dex
  echo $(date) >> $TITLE.dex
  echo "-------------------" >> $TITLE.dex
  echo $[CONTENT] >> $TITLE.dex
  echo "-------------------" >> $TITLE.dex
  fi
  
  
  echo "Good bye!"
  
  # EOF
