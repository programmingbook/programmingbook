Rolodex Improved
================

In the previous chapter, you programmed a basic note-taking
application entirely in BASH . This was no small task, but we managed
to get it done in under 20 lines of code and learned the essentials of
programming, including variables, if/then loops, testing values,
output redirection, and more. 

Now it is time to improve the programme,
and also time to open up a proper text editor so that you can start
editing source code like a normal human being.

Text Editors
------------

Linux, being so popular among so many programmers, has an amazing
assortment of amazing text editors available for its users. Obviously
they all do basically the same thing but many have nuances or
conventions that set them apart from the rest. For this chapter, you
will use the kate editor, a traditional-style text editor with the
added advantages of sytax highlighting, code collapsing, and more.

`Kate` can be launched from the K-menu in the lower left of your Slax
desktop. Its interface is intuitive and at first it might come across
as a normal text editor, but do explore the `Settings` > `Configure Kate`
options to get an idea of how customizable it is.

.. image:: /images/kate.png
   :scale: 50%
   :alt: An image of the Kate text editor.

When you're ready to revise code, open up `rolodex.sh` in Kate. Notice
how Kate provides different colours for text depending on what role
the text plays in your BASH script; it detects the kind of code you're
editnig and adapts its environment for you, so use that when typing in
code to catch typos or hanging quotation marks and so on. It also has
contextual auto-completion, so use that when available so that you
avoid mistyping a variable name or command.

Menu List
---------

Currently, when our application launches it just asks the user for the
name of the note to view, without actually listing the notes
available. It's not exactly a bug, but it does lead to buggy
behaviour, since it relies on the user to successfully remember the
exact names of all the notes ever created. It would be much better if
rolodex.sh would give the user a list of notes available to view. 

I've already made it easy for us to tell a rolodex.sh note file from
other files by ensuring any file we create from within rolodex.sh is
given a `.dex` suffix. That's good, because the `.dex` suffix is a
built-in unique identifier that we can leverage.

To see only the `.dex` files available in your home directory, launch
Konsole from the K-Menu in the left corner of your Slax desktop, and
type:: 
  # ls *.dex

In the BASH language, an asterisk (*) represents a wildcard, so
assuming there are `.dex` files in your home folder, you will see
those, and only those, listed. Other files are excluded from the
output.

To use this built-in filtering method in our programme, we could just
add that same line to the beginning of `rolodex.sh`; the application
would launch, greet the user with a brief intro, list all the
available `.dex` files, and then ask the user to either enter a
filename or the word NEW.

That would work, but we can do better. The nice thing about using
`.dex` as our own custom file extension is that we can select the
files that belong to us easily and fairly effortlessly. The problem
with the `.dex` extension is that the user will likely never remember
whether rolodex.sh will automatically add the `.dex` suffix itself or
whether rolodex.sh expects the user to add it. In other words, the
likelihood of ending up with files with names like `groceries.dex.dex`
and `todo.dex.dex`, and so on, is very high.

There are many ways to protect our users from this, from the very
simplistic echoed warning that the user should not include the `.dex`
suffix, to the more complex option of writing an entire if/then test
statement to determine if `.dex` was included in what the user typed
and adding it if necessary. We will do something squarely in the
middle of those two options. 

With Kate, add the following block of text (the first line shown is
the line that already exists in your code, after which you should add
the new lines)::
  echo "Select a note to view or enter NEW to create a new one."

  for i in *.dex
  do echo $i
  done
  echo "NEW"

Welcome to the *for loop*.

The For-Loop
------------

The "for loop" is another universal building block of programming. It
appears in nearly every language, albeit with different syntax here
and there, but once you get the principle down it's just matter of
getting used to how one language implements the idea. 

In `BASH` , the for-loop that we just added to `rolodex.sh` looks
something like this, in pseudo code:

#. Look for all files ending in `.dex` and assign them, one by one, to
   $i
#. For each instance of $i, echo back to user $i
#. Once each instance of $i has been echoed, the loop is finished;
   move along.

And that's all a for loop does; it establishes what batch of files (or
characters or data sets or whatever) to work on, and then takes them
*one by one* and performs some action upon each, and then closes the
loop and moves on with the program.

The "problem" with for loops is a lot like the "problem" with
programming in general. If you look at the code, it sounds backwards
when compared to the pseudo code. The code phrase "for $i in *.dex" is
a lot like saying something like "for 'name' in students" to ask for
the names of each person in a classroom. It's just not how the English
language has trained us to phrase things. So don't get too hung up in
the implied logic of the syntax of the code; it doesn't matter how it
looks or sounds, it's just syntax.

Now that you've added the for loop, test your application and see how
it works.

Normalizing Data
----------------

The application is already much more user-friendly, now that it gives
the user some indication of what notes they have to choose from.
However, you might have noticed during testing that it is case-
sensitive to a fault. That is, if the user enters "new" or "New" or
even "NeW" instead of exactly "NEW", then the application does not
understand that the user is attempting to create a new note. That'll
get really annoying really fast, so we should fix it. 

There are a few ways around unexpected results in a programme.

We could use a loop or an array to account for all the different
possible inputs; for instance, we could accept new, New, nEw, neW,
NEw, NeW, NEW, n, and N as valid responses. This is a perfectly valid
way of dealing with input and there isn't necessarily a good argument
against using it, but it would take a little bit of extra work plus
it's a little beyond your knowledge at this point. Sometimes, the best
answer is whatever comes easiest to you while you're drumming your
fingers trying to figure something out.

The alternative is data normalization. You know how to do this already
(although you may not know that you know, so give me a moment) and
it's another good lesson that will help you later on in your
programming career.

Our problem now is that our test condition compares the contents of
$SHOW with the string "NEW".  Wouldn't it be nice if we could take the
user's input, convert it to uppercase, and compare *that* to "NEW"?
Well, of course we can do that with tr , as you surely recall.

Try adding this to your document in 
Kate::
  echo "What note do you wish to view?"
  read SHOW
  
  CHOICE=$(echo $SHOW | tr [:lower:] [:upper:])
  
  if [ X$CHOICE != XNEW ]

As you can see, here we have added a new variable (get used to it;
variables are *huge* in programming) that holds *the result of*
(represented by wrapping the statement in $(), but more on that later)
running $SHOW though a tr filter, such that anything that the user has
entered as lowercase becomes uppercase.

This ensures that no matter what the user enters, as long as the
characters *n*, *e*, and *w* are entered (in that order), our
application recognizes it as the word "new". 

You should probably test your application now.

Subprocesses
------------

In BASH , you can great subprocesses by wrapping statements in
parentheses preceded by a dollar sign. This ensures that BASH performs
a command on the *results* of the statement in parens. Open a terminal
and try this easy example::
  # echo date
  date
  # echo $(date)
  Mon Jul 30 22:08:37 EDT 2035

This is very useful, as you have already seen when we made CHOICE a
variable containing the result of echoing $SHOW through tr . We could
use it again to continue in our quest for a more user-friendly
application; as it is now, our application is a little confusing,
since the user must type in filenames without any indication of
whether the application will automatically add the `.dex` extension
(it does). This will almost certainly lead to notes with filenames
like `todo.dex.dex` while others will become just `groceries.dex` and
that just gets to be annoying. 

Once again, there are many ways to deal with this; we could have the
application check to see if the last four characters are ".dex" or
not, and act accordingly. That would work, so if it was the only trick
you knew then you could drum up a for loop plus rev and a test
condition, and no one would think less of you for it. 

`BASH` has a built-in command that would make the file extension check
and normalization much easier. The command is `basename` and its
function, more or less, is to echo back to you the final string of
characters in a long file name or path. `Basename` assumes that you are
requesting the filename at the end of a path unless you tell it
otherwise. 

Try this in a terminal to understand its behaviour::
  # basename /home/root
  root
  # basename example.dex .dex
  example

It seems strangely simplistic, but when variables are involved it
becomes very useful. Add this to your for-loop in Kate (extra lines of
code are provided for context)::
  for i in *.dex
  do echo $(basename $i .dex)
  done

Perhaps now you see why basename is so useful. In this for loop, we
know the file extension but the filename is unknown, so basename lets
us drop the file extension while the for loop provides the filename.

Now if you test the application, you'll see that we don't even bother
telling the user about the `.dex` extension; we just provide the
filenames, and we can safely assume that the user will mimic this
behaviour when creating new notes.

If the user doesn't mimic that
behaviour, thenwe can force it upon them, secretly, with basename
again. In Kate , change the::
  echo $TITLE > $TITLE.dex

so that it reads::
  echo $(basename $TITLE .dex) > $TITLE.dex

This obviously drops any `.dex` extension from the variable's echo so
that just the intended filename is provided when we create the file.
And then it manually adds `.dex` to the filename when the file is
created. In other words, we ensure that no matter what the user has
given us, we know that we have a filename without the file extension;
this gives us the freedom to standardize our application and provide
consistent behaviour for the user.

The final place we could use basename is when we attempt to display an
existing note, if that is what the user has chosen. Since we're
showing the user only the filenames without the `.dex` extension, it
it would be unlikely that the user will type in the full file
name...but hardly impossible. The same technique, then, can be used
here; strip any `.dex` extension and then re-append it, since we do
know that the *actual* file does contain the extension (because our
application put it there).

Change the line that will `cat` the note for the user::
  then cat $(basename $SHOW .dex).dex || echo "File Not Found." && exit 1

Notice what basename is doing, and how we append `.dex` to the end of
the parenthetical statement. This leaves us with the stripped file
name plus the `.dex` extension.

.. note:: You might also notice that I added the phrase *&& exit 1* to
	  the end of the "or" statement. This is not strictly
	  necessary, but since we're improving things, I decided it
	  would be cleaner to tell BASH explicitly to exit the program
	  if we reach the point of "File Not Found". It's a good
	  practise to get into, because otherwise BASH might attempt to
	  continue through the program which, at best, could throw
	  some ugly errors out at your users. 

You might wonder if it would be beneficial to run the user's title
through tr in order to ensure all file names are, for instance, lower
case, or have no spaces in them.  You could do that if you wanted, but
I would rather give the user the freedom to use fancy capitalization,
even at the risk of the user mistyping when trying to open a note. Our
main menu provides all the names of the notes created so if the user
types it in all lower case when in fact there is a capital letter,
it's the user's fault.

Homework
--------

Our simple note-taking application, rolodex.sh , is now complete. We
have had two revisions, and have improved its usability with just a
few extra lines of code.

The program still isn't terribly robust or flexible. For instance:

* It does still require the user to type in the names that the user
  wishes to view rather than just selecting from a numbered list.
* It has no QUIT menu selection, and in fact it necessarily quits
  after just one note is viewed or created.
* It still doesn't deal well with spaces in the filenames.

Even so, you have learned a lot of basic programming skills and you
have used them in a fairly advanced way. Try to further improve this
little note-taking application, and then continue on with the next
lesson.
