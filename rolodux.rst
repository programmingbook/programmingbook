A More Unixy Approach to Rolodex.sh
===================================

We wrote rolodex.sh to mimic a GUI application, with lots of user
prompts and menus and interactivity. This is what most people expect
when they think of software; it's something that people expect.
However, this kind of application design is partly why so much
software out there is so inefficient; programmers write applications
and train people so that the computer has to come to a grinding halt
every few seconds while it waits for the human operator to respond to
some dialogue box, or to initiate the next step in a process. In other
words, most popular applications being sold today are based on a model
that keeps computers from doing what they do best: process information
*quickly*. 

When you design your applications, it's important to keep
in mind that not everyone wants to use your software in the same way.
Just because a new user needs lots of prompts and interactivity
doesn't mean that an experienced user who is already familiar with how
your software works wants the computer to act as if it needs its hand
held while it pokes around at processing data. 

The alternative is to write a rolodex application that can also be
controlled entirely in one command, such as::

  # rolodex --title filename.dex --message 'this is a sample note'

This would actually be a valid, very traditional UNIXy way of writing
the application. It's fairly trivial to add the functionality, and yet
it makes your software something that can be scripted or automated,
friendly to "power users", and just all-around more flexible in user
experience.

Learning Syntax
---------------

One practise of programming is learning how to do something that you
don't know how to do, in the syntax of the language you are
programming in. Sometimes this is learning about and understanding how
to use basic programming concepts, as you have done with for-loops,
if/then statements, test conditions, and more. Other times, it's
learning about specific functionality of a specific programming
language. 

In this case, you must parse options given as part of a
shell command. There are, as usual, many ways to accomplish this, and
the one I favour is the more flexible solution: the "case" statement.

.. note:: The concept of a `getopt` in one form or another is common
	  to many text-based interfaces and has been standardized by
	  POSIX, a set of specifications used to ensure that there is
	  consistency across many computing platforms, languages, and
	  behaviour. For this exercise, you'll learn about the `case`
	  statement specific to BASH but the general concept will be
	  useful to you in many other shells and languages like Perl ,
	  Python , Ruby , and others.

Options, often called "switches" or "flags" modify how a command
works. For instance, the ls command lists the contents of a directory,
but if `ls -l` is entered then it lists the contents plus the sizes
and owners of the file. So if we want to give rolodex.sh the ability
to understand these options, we must learn `getopts`.

Opts and Args
-------------

Within a command, there are three components;

* The command itself, such as `ls`, `pwd`, `cd` and so on.
* Options, sometimes in a short form like `-x` or `-v` and other times
  in a long form such as `--verbose` or `--exclude`. Sometimes
  (depending on how the programmer implements the `getopts` concept)
  they can be grouped together, as in `tar -xvf archive.tar`, which is
  the same as `tar -x -v -f archive.tar`.
* Arguments, sometimes associated to options, as in `--vcodec
  libvorbis`, and other times representing data that the command is
  supposed to operate on, such as `tar -xvf archive.tar`.

Let's look at our proposed `rolodex` command::

  # ./rolodex.sh --title sample.dex --message 'some text in a note file'

That looks simple to understand to you and I, but to a computer it's
just a jumble of letters. The way a computer identifies words is
similar to how we do; it looks for a space between characters. The
computer can thereby determine which of those jumbles of letters is
the Command element, because the command always comes first. So in our
sample command, "rolodex" is the executable programme. That's the easy
part for the computer, and it makes our lives easier because we don't
have to define or differentiate it in any way.

The problem we're left with is that the computer can't really tell an
option from an argument, although you probably can. Obviously, the
options are preceded by a dash. Then again, what if a user entered
something like this::

  # ./rolodex.sh --title -sample.dex --message some more notes to file

Now it becomes more difficult to tell the options from the arguments.

A computer parses every string of characters after the command by
assigning each string a number. The command, ostensibly, would be 0,
and each element afterwards is incremented by one. For example, in the
statement::

  # xmlto txt slackermedia.xml slackermedia.txt

Each string is mapped to the variables:

+---------+--------+------------------+------------------+
| command | option | argument         | argument2        |
+=========+========+==================+==================+
| xmlto   | txt    | slackermedia.xml | slackermedia.txt |
+---------+--------+------------------+------------------+
| 0       | 1      | 2                | 3                |
+---------+--------+------------------+------------------+

Therefore, in a BASH script you can always refer to anything that the
user has entered as part of the initial command by referring to its
positional variable. If we built echo statements into a script, we
could access each option or argument by the number in which they
occurred (where $0 is always the command itself)::
  # echo $1
  txt
  # echo $3
  slackermedia.txt

What does all of this mean? Well, quite simply it means that a
computer will look at $1 when trying to determine what to do after the
initial command, so if we are trying to catch any options preceded by
a single or double dash, then the position we are looking at is $1. We
can tell the BASH to look at $1 and then do certain actions depending
on what it finds there with a `case` statement. For example::
  case "$1" in
  -t | --title)
    TITLE="$2"
    shift 2
    ;;

This is just the beginning of case-statement, but notice that we
define $1 as being -t *or* --title, following this with a parenthesis.
Under that line, we define what BASH should do in case that is true;
specifically, we tell BASH to set the variable TITLE to whatever is in
the $2 place. That makes sense, of course, sense the expected and
supported usage of `rolodex.sh` should contain::

  # ./rolodex.sh --title myTitle.dex

The quandry here is how to parse additional options and arguments. We
can't safely assume that the user will always place `--message` after
they give the `--title` switch, so if we assume that $3 and $4 contain
the contents of the user's note, our application will parse one but
not the other.

The answer to this problem is solved with the `shift`
function, which you see at the end of the code sample. `Shift` tells
BASH to, well, *shift* where $1 is located. In this case, since we
know we want to parse whatever is in the $1 and $2 slots, we then
shift *two* positions over such that what would be $3 becomes the new
$1.

The case-statement we can construct so far, therefore, would be::
  case "$1" in
  -t | --title)
    TITLE="${2}"
    shift 2
    ;;
  -m | --message)
    CONTENT="${2}"
    shift 2
    ;;
  esac

Notice the quotes *and* brackets surrounding `$2` when it appears as
the value for `TITLE` and `CONTENT`. Using these protective symbols
ensures that whatever the user types, spaces are not seen as
BASH delimiters, but as space characters. The user still must surround
their text in quotes while typing so that BASH can parse it, but these
characters protect those spaces from being re-interpreted as
delimiters as our Rolodux programme processes the data.

But now we have one final problem to fix: as we have this programmed,
BASH will only parse the options and arguments once. It will pick up
whatever option and argument set is provided *first* and then it
reaches the end of the case-statement and continues on with the
programme. We obviously want it to examine every string after the
initial command until it runs out of options and arguments.

Any time you want a program to perform an action repeatedly, you need
a loop.  You've already used a loop in rolodex.sh in the form of a
for-loop.  For this task, you will use a new kind of loop: the while
loop.

The While Loop
--------------

While-loops are one of the most common loops in programming. While
loops can be BIG. I mean, really big. Many programmes are wrapped in a
for- or while-loop because, if you think about it, most "normal"
applications (like the ones you and I are used to using on a daily
basis) are launched and they stay open until we close them. The way
many programmes achieve this is that the application is made to launch
with a variable, maybe called $QUIT, equal to false. The programme is
told that while QUIT is equal to false, the application should remain
open, endlessly looping through its main set of code. But the moment
that the QUIT variable is set to true, the application closes. You can
probably guess how the QUIT variable would be changed to true; usually
it's through a menu or button labelled quit or exit or something
similar. 

Our while-loop does not have to be that big. We only need a way for
our application to loop through every string that follows the initial
command and check to see if it's a recognized option. When it hits an
unrecognized option, we need the loop to be broken so that the rest of
the application can be run. 

A while-loop is intrinsically true (unless
told otherwise), so a very simple example of a while loop can be done
in a terminal, like so::
  # i=1
  # while true ; do echo $i && let i++ ; done

First you have set the variable i to 1, and then in a loop that will
last for as long as while is true, you echo $i plus 1. Since nothing
in your code ever sets while to false, this will never end. You can
hit ctrl-c to force it to cease. You could also change your code,
although it will be a little less than exciting::
  # i=8739
  # while true ; do echo $i && break ; done

The `break` command sends an error signal to a loop. Since this while-
loop is set to continue forever while "true", sending an error to it
kicks it into "false" mode, thus ending the perpetual loop.

In the context of our option parsing case-statement, here is the
complete (and I do mean complete) loop::
  while :
    do
        case "$1" in
          -t | --title)
          TITLE="${2}"
    	  shift 2
    	  ;;
          -m | --message)
          CONTENT="${2}"
          shift 2
    	  ;;
          *)
    	  break
    	  ;;
        esac
    done

As you can see, we add a catch-all possibility at the very end, using
an * (asterisk) as a wildcard. This means that if BASH encounters
anything other than what we have defined as valid responses, then it
should perform a `break` and move on.

Determining the Flow of the Application
---------------------------------------

If you have added that while-loop and case-statement to your
programme, you probably have noticed that it doesn't seem to improve
anything. The reason, of course, is that the programme is completely
linear. Even after the while-loop is performed, the application
proceeds to provide a menu and interactive prompts. 

What you really
want is for rolodex.sh to detect whether it is being launched as an
interactive application or whether the user has already provided
everything it needs.

Two possible paths of action? sounds like a job
for another if/then statement to me.

The logic here would be:

* If rolodex.sh is launched with options on the command line, then do
  not be interactive.
* Otherwise, be interactive.

We are already using an if/then statement you are already using to
determine whether your user wants to create a new note or just view
one that already exists. The concept is exactly the same here::
    
  #/bin/bash
  if [ X$1 != "X" ]: then 
      echo "Welcome to Rolodex"
      ... [ all the rest of the code here] ...
      read CONTENT
      fi
  fi
    
  else
  while :
    do
    case "$1" in
    -f | --file)
    ... [ the while loop here ] ...
    break
    ;;
    esac
    done
    
  echo $(basename $TITLE .dex) > $TITLE.dex
  ... [ the rest of the code until the end ]

.. note:: Notice, again, that when testing for the contents of $1, we
	  normalize the data by testing for X$1 being *not* equal
	  to X. In other words, we are looking to see if X and the
	  contents of our variable leaves us with just X. If so, the
	  variable is empty.

This sounds over-complex, but it's more foolproof than just testing to
see if $1 is equal to "".  This successfully produces an application
that will auto-detect whether it is being run as a non-interactive
script, or as an interactive menu-driven application. And frankly this
bonus task has taught you a heck of a lot about programming, so good
job for making it this far. This one was tough! This is me, patting
you proudly on the back.

Debugging
---------

As you can probably see, and possibly even experienced first-hand, all
of those embedded loops can get really really confusing. Not only can
you forget which loop you're in at any given point during the
application, but if you happen to close a loop at the wrong time then
you may cut off entire blocks of code from being run under certain
conditions. Or all conditions.

A common technique to follow the flow of an application is to add a
plethora of `echo` statements and `read` statements. I usually make my
echoes pretty clear; I'll write in things like::

  echo "you are now in the CHOICE if then loop"

Messages like that are useful because you can see exactly where in the
application BASH is going. If you see an echo statement telling you
that you've reached the a part of code that you know you should never
hit given the input you provided, then you know where to concentrate
your debugging efforts. 

Sometimes, I also insert `read DEBUG` statements, which causes the
programme to pause until it accepts input. Since I never do anything
with `$DEBUG`, hitting any key counts as input and resumes the
programme. I generally only use `read` statements for programmes that
provide so much output that the echo statement confirming where I am
in the application would scroll off screen too quickly for me to
notice. In the case of rolodex , a `read DEBUG` statement is probably
not necessary, but remember it as a principle.

Homework
--------

Given the simplicity of our application, we could probably declare it
feature-complete at this point. It performs its task and even provides
the user two different ways to interface with it. Nevertheless, here
are some ideas on things to explore in case you want to add to it:

* Add a `--show` option so that a user can view a file without having to
  go through the menu system (in reality, this is a little silly; any
  experience UNIX user would just use `cat` to view the contents of a
  simple text file).
* Wrap the whole interactive portion of the application in yet another
  while-loop, such that the application stays open until the user types
  in `QUIT`.
* Have the application place all of the files that it generates into a
  specific folder, so the user won't have to look at three dozen `*.dex`
  files every time they look at their home directory.

Rolodux.sh
----------

The source
code::
   #!/bin/bash
    # GNU All-Permissive License
    # Copying and distribution of this file, with or without modification,
    # are permitted in any medium without royalty provided the copyright
    # notice and this notice are preserved.  This file is offered as-is,
    # without any warranty.


    if [ X$1 != "X" ]; then
    while :
     do
       case "$1" in
        -f | --file)
	TITLE="$2"
	shift 2
	;;
	-m | --message)
	CONTENT="${2}"
	shift 2 
	;;
	*)
	break
	;;
       esac
    done
    else
      echo "Welcome to Rolodex"
      echo "------------------"
      echo "Select a note to view"
      echo " or enter NEW to create a new one."
    
    
    ###### wildcards
    for i in *.dex
    ## for loop
    ## variables
    do echo $(basename $i .dex) 
    done
    echo "NEW"
    ###########
    
    
    ## input
    echo "What note do you wish to view?"
    read SHOW
    
    
    ## tr processing
    CHOICE=$(echo $SHOW | tr [:lower:] [:upper:])
    
    
    ## test
    if [ X$CHOICE != XNEW ]
    then cat $(basename $SHOW .dex).dex || echo "File Not Found." && exit 1
    else
    echo "Enter a name for your new note. No Spaces!"
    read TITLE
    echo "Enter content of note."
    read CONTENT
    fi
    fi
    ## re-direction
    echo $(basename $TITLE .dex) > $TITLE.dex
    echo $(date) >> $TITLE.dex
    echo "-------------------" >> $TITLE.dex
    echo "${CONTENT}" >> $TITLE.dex
    echo "-------------------" >> $TITLE.dex
    
    
    echo "Good bye!"
    
    # EOF
