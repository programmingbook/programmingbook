Tiles
=====

Right now your game is a lot more functional than it is pretty, so
it's time to add some nice graphics. As with so many other graphical
programmes, the actual look of the product is mostly completely
separate from the code itself; in many cases, you could write an
entire game without knowing what it was actually going to look like.
Applying art to an existing game engine is sometimes known as
"skinning" a game. 

Skinning a game is fairly easy. The player sprite, in the case of the
game you are writing now, will adopt whatever shape and size of any
series of images named `cat{1-8}.png` that are in its current
directory. A background image can be provided with only a few minor
changes. Tiled images, such as textured walls or platforms, can be a
little more complex but once you understand how they work, it becomes
trivial.

Background Images
-----------------

Setting a background image for your game is achieved with the `blit`
(Bit Level block Transfer) function of Pygame. Referring to the Pygame
documentation on `blit <http://www.pygame.org/docs/ref/surface.html#pygame.Surface.blit>`_ reveals that this function's purpose is to
draw one image onto another. It requires as arguments the source image
and the destination of where the image is to be drawn.

Currenly all the relevant code to the background is located just under
the `pygame.init()` statement. Locate the block of code::

  pygame.init()
  screen = pygame.display.set_mode([screenX, screenY])
  ledge_list = createWorld()

In order to blit an image in the background of the game, you must
create a source entity and a destination entity. Both will be, as you
might have guessed, variables. The source is created with the familiar
pygame `image.load` function::

  backdrop = pygame.image.load("livingroom.png").convert()

This lines creates a variable and sets it to an image, in this case
`livingroom.png`. This image is stored in the same directory, but if
you store it elsewhere then simply provide the full path to the image.

Next, the destination is created::

  backdropRect = screen.get_rect()

Here, the `get_rect` function of Pygame is invoked in order to create
the variable `backdropRect` which is, essentially, an empty canvas the
size of `screen`, which you define just a few lines earlier.

All that is left to do now is to implement the blit within the `main`
loop. You already have a line of code that paints the background a
solid colour::

  screen.fill(white)

That is no longer what we want, so replace the line with::

  screen.blit(backdrop, backdropRect)

This line invokes `blit` on the `screen` instance, using the
`backdrop` variable as the source image and the `backdropRect` (which
is, for all practical purposes, the `screen`) as the destination.

Launch your game now and you will see that you have a background
image.

Tiling
------

Tiling images across collide-able objects such as walls, buildings,
trees, grass, mountains, oceans, and so on, can be complex partly
because of the way that these tiles are usually distributed, and
partly because of the many ways you can use each tile. 

The first issue is that game artists very frequently create
tilesheets: one file with all of the tile objects associated with that
tile theme. So you might download a castle tilesheet or a forest
tilesheet, and so on, and you would then need to "slice" the tilesheet
into individual tiles. Or, more accurately said, Pygame slices the
tilesheet. 

The second issue is that each tile is going to be used multiple
times. For instance, if you have a ledge that you want to draw on
screen, then you want a left corner consisting of one left endcap
tile, a middle section consisting of, probably, the same middle tile
over and over again in a row, and then a right edge consisting of a
right endcap tile. That alone would be fairly simple to generate, but
unless all of your ledges are always the exact same length, the
position and number of the middle tiles and the position of the right
edge will need to be dynamically calculated each time you generate a
ledge on screen.

That means math. Lots and lots of math.

.. note:: If math scares you, don't panic too much. The math does get
	  complex, but when you sit down and look at what your game
	  needs and hack through the logic of determining where each
	  tile type goes, you will probably find that the math will
	  start to make sense, even if only for the few moments you
	  are muddling your way through it. The good news is that once
	  you have it set up, you're finished with it and you can
	  spawn as many ledges as you want without ever thinking about
	  the complex math that went into making them look pretty.

There are simple tilemaps for the example game bundled with this
book's source code, and there are tilemaps online for free as well.

We won't use it in this game, but a good generic tilemap was recently
released over at
`http://www.kenney.nl/downloads/platformerArtAssetsDeluxe.zip
<http://www.kenney.nl/downloads/platformerArtAssetsDeluxe.zip>`_, and
it has been licensed as a Creative Commons asset bundle. If you unzip
the archive, you will find the `tiles_spritesheet.png` file. This
contains all of the tiles in one big file. 

Some sets of tiles ship a text file that explains the tile set. The
`Kenney` set is one of them, so if you look at the
`tiles_spritesheet.txt` file, you will notice, for instance::

  stone = 284 781 70 70
  stoneCenter = 426 710 70 70
  stoneCenter_rounded = 355 781 70 70
  stoneCliffLeft = 639 497 70 70
  stoneCliffLeftAlt = 568 568 70 70
  stoneCliffRight = 497 639 70 70
  stoneCliffRightAlt = 710 497 70 70
  stoneHalf = 639 568 70 70
  stoneHalfLeft = 568 639 70 70
  stoneHalfMid = 497 710 70 70
  stoneHalfRight = 426 781 70 70
  stoneHillLeft2 = 497 781 70 70
  stoneHillRight2 = 568 710 70 70
  stoneLeft = 710 568 70 70
  stoneMid = 639 639 70 70
  stoneRight = 781 497 70 70
  stoneWall = 781 568 70 70

These numbers are important. The first tells you the pixels on the
horizontal axis where the named tile begins, the second lists the same
for the vertical axis, and the final two define the width and height
of the tiles. 

If a sprite sheet does not come with such a friendly explanation of
its assets, then you can use a graphic programme to analyze the
tilemap yourself. You might also be able to use `cutter.py`, a custom
Python application that cuts up tilemaps. 

In a blog post, programmer Radomir Dopieralski wrote a programme that,
for the programmer's reference only, slices up a tilemap. The authors
of this book have made some changes to his programme and include
it with the source code examples of this book. To use it, just launch
the application from a terminal, providing the filename of the image
you want to slice, and the size of tile. 

.. image:: /images/cutter.png

The output of `cutter.py` is a pygame window with each tile separated
cleanly to make it easier for you to count how many blocks over and
down a specific tile is. 

Armed with this knowledge, you must now modify the `Platform` class in
your game so that Pygame will, essentially, do the job that `cutter.py`
does; create an array that contains the tilemap and the knowledge of
where each tile is located. 

The `createBlock` method in the `Platform` class creates the platform
instances, so it is there that you would define what "skin" they will
"wear". 

Locate the `createBlock` method::

  class Platform(pygame.sprite.Sprite):
    ¬ def createBlock(self,x,y,width,height):
    ¬¬ self.image = pygame.Surface([width, height])
    ¬¬ self.rect = self.image.get_rect()
    ¬¬ self.rect.y = y
    ¬¬ self.rect.x = x
    ¬ def __init__(self,x,y,width,height):
    ¬¬ pygame.sprite.Sprite.__init__(self)
    ¬¬ self.createBlock(x,y,width,height)

And change it to::

  01. class Platform(pygame.sprite.Sprite):
  02. ¬ def createBlock(self,gridX,tileY,x,y,width,height):
  03. ¬¬ tilepic = pygame.image.load("tilemap_furniture.png").convert))
  04. ¬¬ self.image = pygame.Surface([width, height])
  05. ¬¬ self.rect = self.image.get_rect()
  06. ¬¬ self.rect.y = y
  07. ¬¬ self.rect.x = x
  08. ¬¬ self.image.set_colorkey(white)

1. The class line does not change.
2. The `createBlock` method will need to know which tile in the array
   of tiles to load for each platform it is called upon to create. You
   pass this information in the same way as you did for collision
   detection; just add arguments to the initial declaration of the method
   so that the values of `gridX` and `tileY` (neither of which exist yet)
   will be imported into the method when it is called.
3. Creates a new variable which contains the tilemap file that you
   wish to use.
4. No change.
5. No change.
6. No change.
7. No change.
8. This particular tilemap interprets transparency as white, so adding
   a colour key and setting it to white (defined as `255,255,255` in your
   variable block at the top of your document) ensures that white is
   rendered invisible.

Now the hard math begins. Continue editing the same block of code,
adding this to it::

  09. ¬¬ for column in range(width//tilesize):
  10. ¬¬¬
  self.image.blit(tilepic,((tilesize*column+tilesize,0),(tileX,tileY,tilesize,tilesize))

9. Creates a new for-loop that will analyze the length of the platform
   you are creating when you call the `Platform` class, and divides the
   total length by the `tilesize` (which you yourself set earlier in the
   document) in order to determine how many tiles it needs to fully
   populate your "solid" object. Since length implies columns rather than
   rows, we use the word `column` as our for-loop variable (although you
   could use anything, whether it's `foo` or `fuzzypeaches`; the for-loop
   doesn't care, but using something that helps explain its own purpose
   is always a good idea).

10. This line blits the tile defined by `tileX` and `tileY` over the
   pixel position determined by the column number *plus* one tilesize.

   The second set of parentheses determines what tile is used for the
   blit by providing the values `tileX` and `tileY`. However, you might
   notice that you have not yet defined the numbers that these variables
   contain; it's asynchronous, so we will define it later, I promise).
   For now, just know that the default value of `tileX` and `tileY` will
   be the top *middle* of the ledge entity in the tilemap. In other
   words, the top left tile for this graphic will be located at `tileX`
   minus one tile, and the top right tile will be located at `tileX` plus
   one tile. If you're having a difficult time visualizing that, just run
   cutter.py on the `tilemap_furniture.png` file::

     cutter.py tilemap_furniture.png 70

   And take a look at the result. 

   .. image:: /images/cutter.png

   You can see that this particular tilemap is really only three tiles
   wide. So whatever tile you choose as the starting point, you will
   need to offset the other tiles in relation to your first tile. I
   use the top-middle tile as the origin because it made sense to
   me. If you yourself happen to think in terms of left-to-right or
   right-to-left rather than out from the center, then by all means
   adjust all the math and use a different point of origin. It doesn't
   matter, as long as you understand and use an offset correctly.

That takes care of all the middle tiles on the topmost row of any
ledge that you spawn by invoking the `Platform` class.

Now you need to define what tiles will be used for the right and left
edges. The trick here is to narrow down for Pygame which position (or
column) in the ledge that you will eventually be spawning the tile
should be placed.

You can define the rightmost position in the ledge with algebra::

  11. ¬¬ for row in range(height//tilesize):
  12. ¬¬¬ for column in range(width//tilesize);
  13. ¬¬¬¬ self.image.blit(tilepic,(tilesize*column+tilesize,0),(tileX,tileY,tilesize,tilesize))

11. Creates a for-loop that limits our actions to the first row by
   taking the height (in this case, 70) and divides it by the tilesize
   (also 70 in this example) for a total of 1.
12. Takes the width of the ledge and divides it by the tilesize,
   thereby cutting the span of the ledge into columns. For example, let's
   say that you were spawning a ledge that is 700 pixels wide; if each
   tile is 70 pixels wide, then you have 10 columns needing to be filled,
   which this for-loop would tackle one at a time.
13. Blits the tile image defined by `tileX` and `tileY` to the pixel
   position determined by the width of the ledge *minus* one tilesize (70
   in this example). In other words, if your ledge is 700 pixels wide,
   then this line blits the tile image starting at 700 minus 70 (630)
   until the end of the ledge (pixel 700). It makes perfect sense if you
   think about it.

For the left endcap tile, the math is similar in concept::

  14. ¬¬¬¬ #left edge
  15. ¬¬¬¬ self.image.blit(tilepic,(width-width,height-height),(tileX-tilesize,0,tilesize,tilesize))

14. A comment to identify where in the ledge we are.
15. Blits the image defined by `tileX` *minus* one tilesize (since our
    origin tile is the middle tile of the ledge tilemap, we need to
    move over one to get the left endcap) at the pixel position
    determined by the total width of the ledge *minus* the total width
    of the ledge (zero) and the total height of the ledge minus the
    height of the ledge (0).
   
    This is obviously one of the simplest tiles to map correctly,
    since it will always start at the origin point of any new
    `Platform` ledge. Rather than actually making Pygame do math, you
    could just as easily substitute 0 for nearly all of the values. But
    the same is true for most of these variables; if you want to just
    do the math from the output of cutter.py and determine where in the
    source tilemap everything is, you can use numbers like 0, -70, 140,
    70, and so on.

That correctly skins one row (that is, one layer of tiles) upon every
ledge that you spawn using the `Platform` class. You may not want your
ledges to float in mid air, so if you require more than one row, you
would do basically the same thing except constrain the process to the
second row (or, in other words, row multiplied by two and multiplied
by the tilesize). 

In the example game for this book, we are spawning coffee tables
across which Wednesday the Cat will run. The first row represents the
tabletop, so the second row needs to be the legs of the table::

  16. #second row
  17. ¬¬ for row in range(height*2//tilesize):
  18. ¬¬¬ #left edge
  19. ¬¬¬ self.image.blit(tilepic,(0,(row+1)*tilesize),(0,tileY+tilesize,tilesize,tilesize))
  20. ¬¬¬ # middle
  21. ¬¬¬ for column in range(width//tilesize):
  22. ¬¬¬¬ self.image.blit(tilepic,((column*tilesize+tilesize),(row+tilesize)),(tileX,tileY+tilesize,tilesize,tilesize))
  23. ¬¬¬ for column in range(width//tileW-(width//tilesize-1)):
  24. ¬¬¬¬ self.image.blit(tilepic,(column+(width//tilesize-1)*tileW,row+1*tileH),(tileX*2,tileY+tilesize,tileW,tileH))

16. Comment for clarity.
17. Creates a for-loop and limits our actions to the second row by
   taking the height of the ledge (70 in this example) and multiplying it
   by two (because we want the second row), and then dividing that by the
   tilesize. Could you just specify the integer 2 here? Well, yes you
   could although that might make some debugging more difficult if you
   were changing tilesizes and had ledges not divisible perfectly by that
   tilesize.
18. Comment for clarity.
19. Blits the image defined by `tileX` and `tileY` at the pixel point
   determined by taking the current row and adding 1. This actually
   leaves us at 1, because the current row gets the default number of 0
   (because computers start counting at 0, even though we are on row 2;
   it's still the zeroth row to the computer), but we then multiply that
   by the tilesize (70 in this example), so the tile will blit starting
   at 70 pixels down from the Y axis, or in other words, the second row.
   The tile graphic being used is defined as 0 in this sample code, but
   as you know, we could also define it as tileX-tilesize.
19. Comment for clarity; below begins the middle tiles.
20. Does the same trick as the first row code and divides the total
   width of the ledge by the tilesize.
21. Blits a tile over the tiles determined by the column number
   multiplied by the tilesize, plus one tilesize (in other words,
   skipping the first 70 pixels).
22. Now we do the rightmost endcap. Again, we limit our actions to
   acting upon only one column.
23. Blits the tile image defined by `tileX` and `tileY` to the pixel
   position determined by the width of the ledge *minus* one tilesize (70
   in this example). In other words, if your ledge is 700 pixels wide,
   then this line blits the tile image starting at 700 minus 70 (630)
   until the end of the ledge (pixel 700). 

   Notice that the row is defined as the current row (row zero) plus
   1, times `tileH` for a total of 70 pixels. In other words, the tile
   graphic starts 70 pixels down from 0 on the Y-axis.

   The X-location of the tile graphic (within the tilemap graphic) is
   `tileX` times 2, or in other words, the default position in the
   tilemap (which, remember, we will define later, I promise!)
   times 2. We could have expressed the same function with, for
   instance, `tileX`+ `tilesize`; anything that will move Pygame's
   target one tile to the right of our current middle tile. Refer
   cutter.py output if you're still not clear on why. 

   Similarly, the Y-location of the tile graphic is given as `tileY`
   plus the tilesize. Again, we could have just as easily done
   `tileY * 2`, as long as the end result is to move Pygame down one
   tile.

The mechanism to skin spawned ledges is now in place, but the
variables `tileX` and `tileY` still have no default values. To define
defaults that will remain constant through the `createBlock`, place
the variables in the `__init__` function, since it is only run once
when the `Platform` class is called upon. 

Locate this block of code::
  def __init__(self,x,y,width,height):
        pygame.sprite.Sprite.__init__(self)
        self.createBlock(x,y,width,height)

And change it to::
   25. ¬ def __init__(self,x,y,tileW,tileH,width,height):
   26. ¬¬ pygame.sprite.Sprite.__init__(self)
   27. ¬¬ tileX = 70
   28. ¬¬ tileY = 0
   29. ¬¬ self.createBlock(tileX,tileY,tileW,tileH,x,y,width,height)

25. Adds `tileW` and `tileH` to the list of arguments required by this
   function. These arguments allow the `Platform` class to know what size
   is being used for a tile graphic. In this example, this has been 70
   pixels.
26. No change.
27. Defines, as promised, which tile in your tilemap you want to use.
   Since a tile is 70 pixels, setting `tileX` to 70 means we are starting
   70 pixels from the left. If you look at the output of cutter.py , you
   see that this means we are skipping the first tile and setting the
   origin point to the middle tile.
28. Defines, as promised, which tile in th tilemap you want to use on
   the Y-axis. `tileY` is set to zero, so we start at the top row.
29. Creates a new ledge but pulls in the values for `tileX` and `tileY`
   values, plus the `tileW` and `tileH` values so that it now utilizes
   the correct tile graphics.

That skins any ledge you spawn. If you have more than just one table
shape that you want to tile across regions in your game, you must
create a new class and map the graphic tiles over the ledges you spawn
with that class. In other words, you might have a `chairPlatform`
class that is basically the same as the `Platform` class, except that
in its functions it will use a `tileX` value of, for instance, 210
(assuming there was a chair graphic right next to the table graphic in
the tilemap file. The offsets would remain basically the same,
depending on the size of the chair graphic, and you could then spawn
chair-skinned ledges by calling your `chairPlatform` class. 

The only other thing to keep in mind when skinning your game is that
tile sizes will not always be the same. For instance, your first
iteration of this game used block sizes of 32 but this sample tilemap
has tiles that are 70 pixels by 70 pixels. Be sure to set your
`tilesize` to 70.

Launch your game. It is fully skinned!
